package com.epam.datagenerator;

import com.epam.model.Book;
import com.epam.model.enums.RolesEnum;
import com.epam.model.User;
import com.epam.web.converter.NameConverter;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.UserDto;

public class UserDataGenerator {

    public static User generateReader(String surname, String firstName, String email, String phone) {
        User user = new User();
        user.setId(user.getId());
        user.setSurname(surname);
        user.setFirstName(firstName);
        user.setPatronymic("Іванівна");
        user.setPhone(phone);
        user.setEmail(email);
        user.setRole(RolesEnum.READER);
        return user;
    }

    public static User generateLibrarian(String surname, String firstName, String email, String phone) {
        User user = new User();
        user.setId(user.getId());
        user.setSurname(surname);
        user.setFirstName(firstName);
        user.setPatronymic("Іванівна");
        user.setPhone(phone);
        user.setEmail(email);
        user.setRole(RolesEnum.LIBRARIAN);
        return user;
    }

    public static UserDto generateReaderDto(int id, String surname, String firstName, String email, String phone) {
        UserDto userDto = new UserDto();
        userDto.setId(id);
        userDto.setSurname(surname);
        userDto.setFirstName(firstName);
        userDto.setPatronymic("Іванівна");
        userDto.setPhone(phone);
        userDto.setEmail(email);
        userDto.setRole(RolesEnum.READER);
        return userDto;
    }

    public static UserDto generateUserDtoFromUser(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFullName(NameConverter.getFullName(user.getSurname(),user.getFirstName(),user.getPatronymic()));
        userDto.setSurname(user.getSurname());
        userDto.setFirstName(user.getFirstName());
        userDto.setPatronymic(user.getPatronymic());
        userDto.setPhone(user.getPhone());
        userDto.setEmail(user.getEmail());
        userDto.setRole(user.getRole());
        return userDto;
    }

    public static BookDto generateBookDtoFromBook(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());
        bookDto.setCount(bookDto.getCount());
        return bookDto;
    }

}
