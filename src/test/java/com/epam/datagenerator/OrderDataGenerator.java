package com.epam.datagenerator;

import com.epam.web.dto.BookDto;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.UserDto;

import java.sql.Date;
import java.time.LocalDate;

public class OrderDataGenerator {

    public static OrderDto generateOrder(int id, UserDto readerDto, BookDto bookDto) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(id);
        orderDto.setBook(bookDto);
        orderDto.setReader(readerDto);
        orderDto.setDateOfIssue(Date.valueOf(LocalDate.of(2022, 1, 12)));
        orderDto.setExpectedReturnDate(Date.valueOf(LocalDate.of(2022, 3, 12)));
        return orderDto;
    }

}
