package com.epam.datagenerator;

import com.epam.model.Author;
import com.epam.model.Book;
import com.epam.web.dto.AuthorDto;
import com.epam.web.dto.BookDto;

import java.util.List;

public class AuthorDataGenerator {

    public static Author generateAuthor(int id, String firstName, String surname, String patronymic, List<Book> books) {
        Author author = new Author();
        author.setId(id);
        author.setFirstName(firstName);
        author.setSurname(surname);
        author.setPatronymic(patronymic);
        author.setBooks(books);
        return author;
    }

    public static AuthorDto generateAuthorDto(int id, String fullName, String firstName, String surname, String patronymic, List<BookDto> booksDto) {
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(id);
        authorDto.setFullName(fullName);
        authorDto.setFirstName(firstName);
        authorDto.setSurname(surname);
        authorDto.setPatronymic(patronymic);
        authorDto.setBooks(booksDto);
        return authorDto;
    }
}
