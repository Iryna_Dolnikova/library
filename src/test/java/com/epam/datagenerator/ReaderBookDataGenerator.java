package com.epam.datagenerator;

import com.epam.model.Book;
import com.epam.model.ReaderCard;
import com.epam.model.User;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.UserDto;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.datagenerator.UserDataGenerator.generateBookDtoFromBook;
import static com.epam.datagenerator.UserDataGenerator.generateUserDtoFromUser;

public class ReaderBookDataGenerator {

    public static ReaderCard generateReaderBook(int id, User reader, User librarian, Book book) {
        return ReaderCard.Builder.builder()
                .id(id)
                .reader(reader)
                .librarian(librarian)
                .book(book)
                .dateOfIssue(Date.valueOf(LocalDate.now().minusMonths(3)))
                .expectedReturnDate(Date.valueOf(LocalDate.now()))
                .actualReturnDate(Date.valueOf(LocalDate.now().minusMonths(1)))
                .build();
    }

    public static ReadersCardDto generateReaderBookDto(LocalDate localDateOfIssue, LocalDate localDateExpected, LocalDate localDateActual,
                                                       BookDto bookDto, UserDto readerDto, UserDto librarianDto) {
        return ReadersCardDto.Builder.builder()
                .id(5)
                .book(bookDto)
                .librarian(librarianDto)
                .reader(readerDto)
                .dateOfIssue(Date.valueOf(localDateOfIssue))
                .expectedReturnDate(Date.valueOf(localDateExpected))
                .actualReturnDate(Date.valueOf(localDateActual))
                .build();
    }

    public static ReadersCardDto generateReaderBookDtoFromReaderBook(ReaderCard readerCard) {
        return ReadersCardDto.Builder.builder()
                .id(readerCard.getId())
                .reader(generateUserDtoFromUser(readerCard.getReader()))
                .librarian(generateUserDtoFromUser(readerCard.getLibrarian()))
                .book(generateBookDtoFromBook(readerCard.getBook()))
                .dateOfIssue(readerCard.getDateOfIssue())
                .expectedReturnDate(readerCard.getExpectedReturnDate())
                .actualReturnDate(readerCard.getActualReturnDate())
                .build();
    }

    public static List<ReadersCardDto> generateReaderBookDtoListFromReaderBooks(List<ReaderCard> readerCards) {
        return readerCards.stream()
                .map(ReaderBookDataGenerator::generateReaderBookDtoFromReaderBook)
                .collect(Collectors.toList());
    }

}
