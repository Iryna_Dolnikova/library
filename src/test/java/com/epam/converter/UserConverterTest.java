package com.epam.converter;

import com.epam.datagenerator.UserDataGenerator;
import com.epam.model.User;
import com.epam.web.converter.UserConverter;
import com.epam.web.dto.UserDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class UserConverterTest {

    private UserConverter userConverter = UserConverter.getInstance();

    @Test
    public void toUserDtoListShouldReturnUserDtoList() {
        //GIVEN
        List<User> users = new ArrayList<>();
        User reader1 = UserDataGenerator.generateReader("Ivashenko", "Serhey", "ivashenko@gmail.com", "295-41-38");
        User reader2 = UserDataGenerator.generateReader("Kozachenko", "Igor", "kozachenko@gmail.com", "285-32-15");
        User librarian = UserDataGenerator.generateLibrarian("Kuzmenko", "Irina", "kuzmenko@gmail.com", "279-541-17");
        users.add(reader1);
        users.add(reader2);
        users.add(librarian);

        List<UserDto> expectedUserDtoList = new ArrayList<>();
        UserDto readerDto1 = UserDataGenerator.generateUserDtoFromUser(reader1);
        UserDto readerDto2 = UserDataGenerator.generateUserDtoFromUser(reader2);
        UserDto librarianDto = UserDataGenerator.generateUserDtoFromUser(librarian);
        expectedUserDtoList.add(readerDto1);
        expectedUserDtoList.add(readerDto2);
        expectedUserDtoList.add(librarianDto);

        //WHEN
        List<UserDto> actualUserDtoList = userConverter.toUserDtoList(users);
        //THEN
        assertEquals(expectedUserDtoList,actualUserDtoList);
        assertSame(expectedUserDtoList.size(),actualUserDtoList.size());
    }
}
