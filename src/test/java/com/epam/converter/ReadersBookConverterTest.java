package com.epam.converter;

import com.epam.model.Book;
import com.epam.model.ReaderCard;
import com.epam.model.User;
import com.epam.web.converter.ReadersBookConverter;
import com.epam.web.dto.ReadersCardDto;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.epam.datagenerator.ReaderBookDataGenerator.generateReaderBook;
import static com.epam.datagenerator.ReaderBookDataGenerator.generateReaderBookDtoFromReaderBook;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class ReadersBookConverterTest {
    private final ReadersBookConverter readersBookConverter = new ReadersBookConverter();

    @Test
    public void toReaderBookDtoShouldReturnReadersCardDto() {
        //GIVEN
        ReaderCard readerCard = generateReaderBook(1, new User(5), new User(7), new Book(3));
        ReadersCardDto expectedReadersCardDto = generateReaderBookDtoFromReaderBook(readerCard);
        expectedReadersCardDto.setId(readerCard.getId());

        //WHEN
        ReadersCardDto actualReadersCardDto = readersBookConverter.toReaderBookDto(readerCard);

        //THEN
        assertEquals(expectedReadersCardDto, actualReadersCardDto);
    }

    @Test
    public void toListReaderBookDtoShouldReturnListReaderBookDto(){
        //GIVEN
        ReaderCard readerCard1 = generateReaderBook(1, new User(5), new User(11), new Book(8));
        ReaderCard readerCard2 = generateReaderBook(2, new User(6), new User(12), new Book(9));
        ReaderCard readerCard3 = generateReaderBook(3, new User(7), new User(13), new Book(12));
        List <ReaderCard> readerCards = Arrays.asList(readerCard1,readerCard2,readerCard3);

        List<ReadersCardDto> expectedReadersCardDto  = new ArrayList<>();
        expectedReadersCardDto.add(generateReaderBookDtoFromReaderBook(readerCard1));
        expectedReadersCardDto.add(generateReaderBookDtoFromReaderBook(readerCard2));
        expectedReadersCardDto.add(generateReaderBookDtoFromReaderBook(readerCard3));

        //WHEN
        List<ReadersCardDto> actualReadersCardDto = readersBookConverter.toListReaderBookDto(readerCards);

        //THEN
        assertSame(expectedReadersCardDto.size(),actualReadersCardDto.size());
        assertEquals(expectedReadersCardDto,actualReadersCardDto);
    }
}