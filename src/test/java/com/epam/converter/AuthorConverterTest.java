package com.epam.converter;

import com.epam.datagenerator.AuthorDataGenerator;
import com.epam.model.Author;
import com.epam.model.Book;
import com.epam.web.converter.AuthorConverter;
import com.epam.web.dto.AuthorDto;
import com.epam.web.dto.BookDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class AuthorConverterTest {

    private final AuthorConverter authorConverter = AuthorConverter.getInstance();

    @Test
    public void toAuthorDtoListShouldReturnAuthorDtoList() {
        //GIVEN
        List<Author> authors = new ArrayList<>();
        List<Book> bookList = new ArrayList<>();
        authors.add(AuthorDataGenerator.generateAuthor(1, "Irina", "Valueva", null, bookList));
        authors.add(AuthorDataGenerator.generateAuthor(2, "Olga", "Petrenko", null, bookList));
        authors.add(AuthorDataGenerator.generateAuthor(3, "Ivan", "Kamkov", "Igorevich", bookList));

        List<AuthorDto> expectedAuthorDto = new ArrayList<>();
        List<BookDto> bookDtoList = new ArrayList<>();
        expectedAuthorDto.add(AuthorDataGenerator.generateAuthorDto(1, "Valueva I.", "Irina", "Valueva", null, bookDtoList));
        expectedAuthorDto.add(AuthorDataGenerator.generateAuthorDto(2, "Petrenko O.", "Olga", "Petrenko", null, bookDtoList));
        expectedAuthorDto.add(AuthorDataGenerator.generateAuthorDto(3, "Kamkov I.I.", "Ivan", "Kamkov", "Igorevich", bookDtoList));

        //WHEN
        List<AuthorDto> actualAuthorDtoList = authorConverter.toAuthorDtoList(authors);

        //THEN
        assertEquals(expectedAuthorDto, actualAuthorDtoList);
        assertSame(expectedAuthorDto.size(), actualAuthorDtoList.size());
    }

}
