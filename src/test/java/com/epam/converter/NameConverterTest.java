package com.epam.converter;

import com.epam.web.converter.NameConverter;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NameConverterTest {

    public static Stream<Arguments> nameParameters() {
        return Stream.of(
                Arguments.of("Shevchenko", "Taras", "Hruhorovych", "Shevchenko T.H."),
                Arguments.of("Bdgilka", "Olena", "Petrivna", "Bdgilka O.P."),
                Arguments.of("Chernenko", "Hanna", "Olelksandrivna", "Chernenko H.O."),
                Arguments.of("Kyzmenko", "Mykola", null, "Kyzmenko M."),
                Arguments.of("Shulga", null, null, "Shulga"),
                Arguments.of("Kosach", "", "", "Kosach")
        );
    }

    @ParameterizedTest
    @MethodSource("nameParameters")
    public void getFullNameShouldReturnFullName(String firstname, String name, String patronymic, String expectedFullName) {
        //WHEN
        String actualFullName = NameConverter.getFullName(firstname, name, patronymic);

        //THEN
        assertEquals(expectedFullName, actualFullName);
    }
}

