package com.epam.converter;

import com.epam.model.Book;
import com.epam.web.converter.BookConverter;
import com.epam.web.dto.BookDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class BookConverterTest {
    private final BookConverter bookConverter = BookConverter.getInstance();

    @Test
    public void toBookDtoListShouldReturnBookDtoList() {
        //GIVEN
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book(1, "Вечірнє сонце", 15));
        bookList.add(new Book(2, "Зелена трава", 7));
        bookList.add(new Book(3, "Місяць", 9));

        List<BookDto> expectedBookDtoList = new ArrayList<>();
        expectedBookDtoList.add(new BookDto(1, "Вечірнє сонце", 15));
        expectedBookDtoList.add(new BookDto(2, "Зелена трава", 7));
        expectedBookDtoList.add(new BookDto(3, "Місяць", 9));

        //WHEN
        List<BookDto> actualBookDtoList = bookConverter.toBookDtoList(bookList);

        //THEN
        assertEquals(expectedBookDtoList, actualBookDtoList);
        assertSame(expectedBookDtoList.size(), actualBookDtoList.size());
    }

}
