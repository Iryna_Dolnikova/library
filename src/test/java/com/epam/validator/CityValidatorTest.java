package com.epam.validator;

import com.epam.model.City;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CityValidatorTest {

    @Mock
    private HttpServletRequest httpServletRequest;
    private Validator<City> cityValidator = new CityValidator();

    @Test
    public void validateShouldReturnInvalidFieldsList() {
        //GIVEN
        City city = new City("Dnipro*");
        List<String> expectedInvalidName = Arrays.asList("name");

        //WHEN
        List<String> actualInvalidName = cityValidator.validate(city, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidName, actualInvalidName);
        verify(httpServletRequest).setAttribute("name", city.getName());
    }

    @Test
    public void validateShouldReturnEmptyListWhenFieldsAreValid() {
        //GIVEN
        City city = new City("Dnipro");
        List<String> expectedInvalidName = new ArrayList<>();

        //WHEN
        List<String> actualInvalidName = cityValidator.validate(city, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidName, actualInvalidName);
        verify(httpServletRequest).setAttribute("name", city.getName());
    }
}
