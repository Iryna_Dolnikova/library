package com.epam.validator;

import com.epam.model.City;
import com.epam.model.PublishingHouse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PublishingHouseValidatorTest {
    @Mock
    private HttpServletRequest httpServletRequest;
    private Validator<PublishingHouse> publishingHouseValidator = new PublishingHouseValidator();

    @Test
    public void validateShouldReturnEmptyListWhenFieldsValid() {
        //GIVEN
        PublishingHouse publishingHouse = new PublishingHouse(1, "Folio", new City(5, "Dnipro"));
        List<String> expectedInvalidFields = new ArrayList<>();

        //WHEN
        List<String> actualInvalidFields = publishingHouseValidator.validate(publishingHouse, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("name", publishingHouse.getName());
        verify(httpServletRequest).setAttribute("cityId", publishingHouse.getCity().getId());
    }

    @Test
    public void validateShouldReturnInvalidFieldsListWhenNameIsInvalid() {
        //GIVEN
        PublishingHouse publishingHouse = new PublishingHouse(1, "Folio/-", new City(5, "Dnipro"));
        List<String> expectedInvalidFields = Arrays.asList("name");

        //WHEN
        List<String> actualInvalidFields = publishingHouseValidator.validate(publishingHouse, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("name", publishingHouse.getName());
    }

    @Test
    public void validateShouldReturnListWhenTwoFieldsValidWhenNameAndCityIdIsInvalid() {
        //GIVEN
        PublishingHouse publishingHouse = new PublishingHouse(1, "Folio'3", new City(-1, "Dnipro"));
        List<String> expectedInvalidFields = new ArrayList<>();
        expectedInvalidFields.add("name");
        expectedInvalidFields.add("cityId");

        //WHEN
        List<String> actualInvalidFields = publishingHouseValidator.validate(publishingHouse, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        assertSame(expectedInvalidFields.size(), actualInvalidFields.size());
        verify(httpServletRequest).setAttribute("name", publishingHouse.getName());
        verify(httpServletRequest).setAttribute("cityId", publishingHouse.getCity().getId());
    }

}
