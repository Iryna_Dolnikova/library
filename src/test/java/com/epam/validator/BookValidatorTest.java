package com.epam.validator;

import com.epam.model.PublishingHouse;
import com.epam.web.dto.BookDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class BookValidatorTest {

    @Mock
    private HttpServletRequest httpServletRequest;
    private Validator<BookDto> validator = new BookValidator();

    @Test
    public void validateShouldReturnInvalidFieldsListWhenBookNameIsInvalidAndCountLessThenZero() {
        //GIVEN
        BookDto bookDto = new BookDto(1, "Night#sub", null, new PublishingHouse(2, "Morning"),
                2019, -2);
        List<String> expectedInvalidFields = Arrays.asList("name", "count");

        //WHEN
        List<String> actualInvalidFields = validator.validate(bookDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("name", bookDto.getName());
        verify(httpServletRequest).setAttribute("count", bookDto.getCount());
        verify(httpServletRequest).setAttribute("publishingHouseId", bookDto.getPublishingHouse().getId());
        verify(httpServletRequest).setAttribute("issueYear", bookDto.getIssueYear());
    }

    @ParameterizedTest
    @ValueSource(ints = {-99, 9999})
    public void validateShouldReturnInvalidFieldsListWhenPublishingHouseIdLessThenZeroAndIssueYearIsInvalid(int issueYear) {
        //GIVEN
        BookDto bookDto = new BookDto(1, "Night sun", null, new PublishingHouse(-2, "Folio"),
                issueYear, 2);
        List<String> expectedInvalidFields = Arrays.asList("publishingHouseId", "issueYear");

        //WHEN
        List<String> actualInvalidFields = validator.validate(bookDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("publishingHouseId", bookDto.getPublishingHouse().getId());
        verify(httpServletRequest).setAttribute("issueYear", bookDto.getIssueYear());
        verify(httpServletRequest).setAttribute("name", bookDto.getName());
        verify(httpServletRequest).setAttribute("count", bookDto.getCount());
    }

    @Test
    public void validateShouldReturnEmptyListWhenFieldsAreValid() {
        //GIVEN
        BookDto bookDto = new BookDto(1, "Night Sun", null, new PublishingHouse(5, "Folio"),
                1998, 5);
        List<String> expectedInvalidFields = new ArrayList<>();

        //WHEN
        List<String> actualInvalidFields = validator.validate(bookDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("publishingHouseId", bookDto.getPublishingHouse().getId());
        verify(httpServletRequest).setAttribute("issueYear", bookDto.getIssueYear());
        verify(httpServletRequest).setAttribute("name", bookDto.getName());
        verify(httpServletRequest).setAttribute("count", bookDto.getCount());
    }

}
