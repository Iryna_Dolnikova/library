package com.epam.validator;

import com.epam.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class LoginValidatorTest {
    @Mock
    private HttpServletRequest httpServletRequest;

    private Validator<User> userValidator = new LoginValidator();

    @Test
    public void validateShouldReturnInvalidFieldsListWhenEmailAndPasswordAreInvalid() {
        //GIVEN
        User user = new User();
        user.setEmail("taras@gmailcom");
        user.setPassword("Password123");

        List<String> expectedInvalidNames = Arrays.asList("email", "password");

        //WHEN
        List<String> actualInvalidNames = userValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidNames);
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"taras@gmailcom", "tarasgmail.com"})
    public void validateShouldReturnInvalidFieldsListWhenEmailIsInvalid(String email) {
        //GIVEN
        User user = new User();
        user.setEmail(email);
        user.setPassword("Password@123");

        List<String> expectedInvalidNames = Arrays.asList("email");

        //WHEN
        List<String> actualInvalidNames = userValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidNames);
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"Password123", "pa", "Password@@@@", "password@123"})
    public void validateShouldReturnInvalidFieldsListWhenPasswordIsInvalid(String password) {
        //GIVEN
        User user = new User();
        user.setEmail("taras@gmail.com");
        user.setPassword(password);

        List<String> expectedInvalidNames = Arrays.asList("password");

        //WHEN
        List<String> actualInvalidNames = userValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidNames);
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
    }

    @Test
    public void validateShouldReturnEmptyListWhenFieldsAreValid() {
        //GIVEN
        User user = new User();
        user.setEmail("taras@gmail.com");
        user.setPassword("Password@123");

        List<String> expectedInvalidNames = new ArrayList<>();

        //WHEN
        List<String> actualInvalidNames = userValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidNames);
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
    }
}
