package com.epam.validator;

import com.epam.model.City;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidatorTest {

    private Validator<City> validator = new CityValidator();

    public static Stream<Arguments> personNameParameters() {
        return Stream.of(
                Arguments.of("В'ячеславович", true),
                Arguments.of("Iryna", true),
                Arguments.of("Ivano-Frankivsk", true),
                Arguments.of("New York", true),
                Arguments.of("Mr.Smitt", true),
                Arguments.of("Odesa", true),
                Arguments.of("Borispol", true),
                Arguments.of("Verkhnodniprovsk", true),
                Arguments.of("Iryna$", false),
                Arguments.of("Mr", true),
                Arguments.of("Mr44", false),
                Arguments.of("Odesa/", false),
                Arguments.of(null, false),
                Arguments.of("", false),
                Arguments.of("  ", false)
        );
    }

    @ParameterizedTest
    @MethodSource("personNameParameters")
    public void isPersonNameValidShouldValidateName(String name, boolean expectedResult) {
        //WHEN
        boolean actualResult = validator.isPersonNameValid(name);

        //THEN
        assertEquals(expectedResult, actualResult);
    }

    public static Stream<Arguments> personPatronymicNameParameters() {
        return Stream.of(
                Arguments.of("Iryna", true),
                Arguments.of("Ivano-Frankivsk", true),
                Arguments.of("New York", true),
                Arguments.of("Mr.Smitt", true),
                Arguments.of("Odesa", true),
                Arguments.of("Borispol", true),
                Arguments.of("Verkhnodniprovsk", true),
                Arguments.of("Iryna$", false),
                Arguments.of("Mr", true),
                Arguments.of("Mr44", false),
                Arguments.of("Odesa/", false),
                Arguments.of(null, true),
                Arguments.of("", true),
                Arguments.of("  ", true)
        );
    }

    @ParameterizedTest
    @MethodSource("personPatronymicNameParameters")
    public void isPersonPatronymicNameValidShouldValidateName(String name, boolean expectedResult) {
        //WHEN
        boolean actualResult = validator.isPersonNameValidOrNull(name);

        //THEN
        assertEquals(expectedResult, actualResult);
    }

    public static Stream<Arguments> nameParameters() {
        return Stream.of(
                Arguments.of("Ivano-Frankivsk", true),
                Arguments.of("New York", true),
                Arguments.of("Mr.Smitt", true),
                Arguments.of("Odesa", true),
                Arguments.of("1984", true),
                Arguments.of("Iryna$", false),
                Arguments.of("Mr", true),
                Arguments.of("Odesa/", false),
                Arguments.of(null, false),
                Arguments.of("", false),
                Arguments.of("  ", false)
        );
    }

    @ParameterizedTest
    @MethodSource("nameParameters")
    public void isNameValid(String name, boolean expectedResult) {
        //WHEN
        boolean actualResult = validator.isNameValid(name);

        //THEN
        assertEquals(expectedResult, actualResult);
    }

}
