package com.epam.validator;

import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserForUpdateValidatorTest {

    @Mock
    private HttpServletRequest httpServletRequest;

    private Validator<User> updateValidator = new UserForUpdateValidator();

    @Test
    public void validateShouldReturnInvalidFieldsListWhenFirstnameAndPatronymicAreInvalid() {
        //GIVEN
        User user = new User(1, "Shevchenko", "Taras#", "Mykola&", "taras@gmail.com",
                "066 123 45 67", "Password@123", RolesEnum.READER, false);
        List<String> expectedInvalidNames = Arrays.asList("firstName", "patronymic");

        //WHEN
        List<String> actualInvalidName = updateValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"taras@gmailcom", "tarasgmail.com"})
    public void validateShouldReturnInvalidFieldsListWhenSurnameAndEmailAreInvalid(String email) {
        //GIVEN
        User user = new User(1, "Shevche2nko", "Taras", "Mykol", email,
                "066 123 45 67", "Password@123", RolesEnum.READER, false);
        List<String> expectedInvalidNames = Arrays.asList("surname", "email");

        //WHEN
        List<String> actualInvalidName = updateValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"+38 (050) 294-51-08", "14-52 78"})
    public void validateShouldReturnInvalidFieldsListWhenPhoneIsInvalid(String phone) {
        //GIVEN
        User user = new User(1, "Shevchenko", "Taras", "Mykol", "taras@gmail.com",
                phone, "Password@123", RolesEnum.READER, true);
        List<String> expectedInvalidNames = Arrays.asList("phone");

        //WHEN
        List<String> actualInvalidName = updateValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
    }

    @Test
    public void validateShouldReturnEmptyListWhenFieldsValid() {
        //GIVEN
        User user = new User(1, "Shevchenko", "Taras", "Mykol", "taras@gmail.com",
                "066 123 45 67", "Password@123", RolesEnum.READER, true);
        List<String> expectedInvalidNames = new ArrayList<>();

        //WHEN
        List<String> actualInvalidName = updateValidator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
    }
}



