package com.epam.validator;

import com.epam.web.dto.BookDto;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ReadersCardValidatorTest {

    @Mock
    private HttpServletRequest httpServletRequest;

    private Validator<ReadersCardDto> validator = new ReadersCardValidator();

    @Test
    public void validateShouldReturnInvalidFieldsListWhenBookIdLessThenZeroAndDateOfIssueIsAfterCurrentDate() {
        //GIVEN
        ReadersCardDto readersCardDto = new ReadersCardDto();
        readersCardDto.setBook(new BookDto(-1));
        readersCardDto.setReader(new UserDto(2));
        readersCardDto.setLibrarian(new UserDto(3));
        readersCardDto.setDateOfIssue(Date.valueOf(LocalDate.now().plusDays(2)));
        readersCardDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().plusDays(3)));
        readersCardDto.setActualReturnDate(null);
        List<String> expectedInvalidNames = Arrays.asList("bookId", "dateOfIssue");

        //WHEN
        List<String> actualInvalidName = validator.validate(readersCardDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("bookId", readersCardDto.getBook().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", readersCardDto.getDateOfIssue());
        verify(httpServletRequest).setAttribute("readerId", readersCardDto.getReader().getId());
        verify(httpServletRequest).setAttribute("librarianId", readersCardDto.getLibrarian().getId());
        verify(httpServletRequest).setAttribute("expectedReturnDate", readersCardDto.getExpectedReturnDate());
        verify(httpServletRequest).setAttribute("actualReturnDate", readersCardDto.getActualReturnDate());
    }

    @Test
    public void validateShouldReturnInvalidFieldsListWhenReaderIdLessThenZeroAndExpectedReturnDateIsLessThenDateOfIssue() {
        //GIVEN
        ReadersCardDto readersCardDto = new ReadersCardDto();
        readersCardDto.setBook(new BookDto(1));
        readersCardDto.setReader(new UserDto(-2));
        readersCardDto.setLibrarian(new UserDto(3));
        readersCardDto.setDateOfIssue(Date.valueOf(LocalDate.now().minusDays(5)));
        readersCardDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().minusDays(7)));
        readersCardDto.setActualReturnDate(null);
        List<String> expectedInvalidNames = Arrays.asList("readerId", "expectedReturnDate");

        //WHEN
        List<String> actualInvalidName = validator.validate(readersCardDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("readerId", readersCardDto.getReader().getId());
        verify(httpServletRequest).setAttribute("expectedReturnDate", readersCardDto.getExpectedReturnDate());
        verify(httpServletRequest).setAttribute("bookId", readersCardDto.getBook().getId());
        verify(httpServletRequest).setAttribute("librarianId", readersCardDto.getLibrarian().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", readersCardDto.getDateOfIssue());
        verify(httpServletRequest).setAttribute("actualReturnDate", readersCardDto.getActualReturnDate());
    }

    @Test
    public void validateShouldReturnInvalidFieldsListWhenReaderIdLessThenZeroAndExpectedReturnDateIsLessThenCurrentDate() {
        //GIVEN
        ReadersCardDto readersCardDto = new ReadersCardDto();
        readersCardDto.setBook(new BookDto(1));
        readersCardDto.setReader(new UserDto(-2));
        readersCardDto.setLibrarian(new UserDto(3));
        readersCardDto.setDateOfIssue(Date.valueOf(LocalDate.now()));
        readersCardDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().minusDays(3)));
        readersCardDto.setActualReturnDate(null);
        List<String> expectedInvalidNames = Arrays.asList("readerId", "expectedReturnDate");

        //WHEN
        List<String> actualInvalidName = validator.validate(readersCardDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("readerId", readersCardDto.getReader().getId());
        verify(httpServletRequest).setAttribute("expectedReturnDate", readersCardDto.getExpectedReturnDate());
        verify(httpServletRequest).setAttribute("bookId", readersCardDto.getBook().getId());
        verify(httpServletRequest).setAttribute("librarianId", readersCardDto.getLibrarian().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", readersCardDto.getDateOfIssue());
        verify(httpServletRequest).setAttribute("actualReturnDate", readersCardDto.getActualReturnDate());
    }

    @Test
    public void validateShouldReturnInvalidFieldsListWhenLibrarianIdLessThenZeroAndActualReturnDateIsAfterCurrentDate() {
        //GIVEN
        ReadersCardDto readersCardDto = new ReadersCardDto();
        readersCardDto.setBook(new BookDto(1));
        readersCardDto.setReader(new UserDto(2));
        readersCardDto.setLibrarian(new UserDto(-3));
        readersCardDto.setDateOfIssue(Date.valueOf(LocalDate.now()));
        readersCardDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().plusDays(3)));
        readersCardDto.setActualReturnDate(Date.valueOf(LocalDate.now().plusDays(4)));
        List<String> expectedInvalidNames = Arrays.asList("librarianId", "actualReturnDate");

        //WHEN
        List<String> actualInvalidName = validator.validate(readersCardDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("librarianId", readersCardDto.getLibrarian().getId());
        verify(httpServletRequest).setAttribute("actualReturnDate", readersCardDto.getActualReturnDate());
        verify(httpServletRequest).setAttribute("bookId", readersCardDto.getBook().getId());
        verify(httpServletRequest).setAttribute("readerId", readersCardDto.getReader().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", readersCardDto.getDateOfIssue());
        verify(httpServletRequest).setAttribute("expectedReturnDate", readersCardDto.getExpectedReturnDate());
    }

    @Test
    public void validateShouldReturnEmptyListWhenFieldsAreValid() {
        //GIVEN
        ReadersCardDto readersCardDto = new ReadersCardDto();
        readersCardDto.setBook(new BookDto(1));
        readersCardDto.setReader(new UserDto(2));
        readersCardDto.setLibrarian(new UserDto(3));
        readersCardDto.setDateOfIssue(Date.valueOf(LocalDate.now().minusDays(7)));
        readersCardDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().plusDays(4)));
        readersCardDto.setActualReturnDate(Date.valueOf(LocalDate.now().minusDays(1)));
        List<String> expectedInvalidNames = new ArrayList<>();

        //WHEN
        List<String> actualInvalidName = validator.validate(readersCardDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("bookId", readersCardDto.getBook().getId());
        verify(httpServletRequest).setAttribute("readerId", readersCardDto.getReader().getId());
        verify(httpServletRequest).setAttribute("librarianId", readersCardDto.getLibrarian().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", readersCardDto.getDateOfIssue());
        verify(httpServletRequest).setAttribute("expectedReturnDate", readersCardDto.getExpectedReturnDate());
        verify(httpServletRequest).setAttribute("actualReturnDate", readersCardDto.getActualReturnDate());
    }
}
