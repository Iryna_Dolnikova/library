package com.epam.validator;

import com.epam.model.Author;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AuthorValidatorTest {
    @Mock
    private HttpServletRequest httpServletRequest;

    private Validator<Author> authorValidator = new AuthorValidator();

    @ParameterizedTest
    @ValueSource(strings = {"Mykola&", "M"})
    public void validateShouldReturnInvalidFieldsListWhenFirstnameAndPatronymicAreInvalid(String name) {
        //GIVEN
        Author author = new Author(1, "Taras#", "Shevchenko", name);
        List<String> expectedInvalidNames = Arrays.asList("firstName", "patronymic");

        //WHEN
        List<String> actualInvalidName = authorValidator.validate(author, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("surname", author.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", author.getPatronymic());
        verify(httpServletRequest).setAttribute("firstName", author.getFirstName());
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    public void validateShouldReturnInvalidFieldsListWhenFirstnameIsInvalidAndPatronymicIsValid(String name) {
        //GIVEN
        Author author = new Author(1, "Taras#", "Shevchenko", name);
        List<String> expectedInvalidNames = Arrays.asList("firstName");

        //WHEN
        List<String> actualInvalidName = authorValidator.validate(author, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("surname", author.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", author.getPatronymic());
        verify(httpServletRequest).setAttribute("firstName", author.getFirstName());
    }

    @Test
    public void validateShouldNotReturnPatronymicInInvalidFieldsListWhenPatronymicIsNull() {
        //GIVEN
        Author author = new Author(1, "Taras", "Shevchenko№", null);
        List<String> expectedInvalidNames = Arrays.asList("surname");

        //WHEN
        List<String> actualInvalidName = authorValidator.validate(author, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("surname", author.getSurname());
        verify(httpServletRequest).setAttribute("firstName", author.getFirstName());
        verify(httpServletRequest).setAttribute("patronymic", author.getPatronymic());
    }

    @Test
    public void validateShouldReturnEmptyListWhenFieldsAreValid() {
        //GIVEN
        Author author = new Author(1, "Taras", "Shevchenko", "Hruhorovych");
        List<String> expectedInvalidNames = new ArrayList<>();

        //WHEN
        List<String> actualInvalidName = authorValidator.validate(author, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("firstName", author.getFirstName());
        verify(httpServletRequest).setAttribute("surname", author.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", author.getPatronymic());
    }

}
