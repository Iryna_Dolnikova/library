package com.epam.validator;

import com.epam.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserValidatorTest {

    @Mock
    private HttpServletRequest httpServletRequest;

    private Validator<User> validator = new UserValidator();

    private User user = new User();

    @Test
    public void validateShouldReturnEmptyListWhenFieldsValid() {
        //GIVEN
        user.setFirstName("Olha");
        user.setSurname("Petrenko");
        user.setPatronymic("Serhiivna");
        user.setEmail("Petrenko@gmail.com");
        user.setPhone("+38 050 294-51-08");
        user.setPassword("Opass159qaz$");
        List<String> expectedInvalidFields = new ArrayList<>();

        //WHEN
        List<String> actualInvalidFields = validator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"+38 (050) 294-51-08", "14-52 78"})
    public void validateShouldReturnListWithInvalidFieldsWhenSurnameAndPhoneAndPasswordAreInvalid(String phone) {
        //GIVEN
        user.setFirstName("Olha");
        user.setSurname("Petr#enko");
        user.setPatronymic("Serhiivna");
        user.setEmail("Petrenko@gmail.com");
        user.setPhone(phone);
        user.setPassword("pass159qaz$");
        List<String> expectedInvalidFields = Arrays.asList("surname", "phone", "password");

        //WHEN
        List<String> actualInvalidFields = validator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());

    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"taras@gmailcom", "tarasgmail.com"})
    public void validateShouldReturnListWithInvalidFieldsWhenNameAndPatronymicAndEmailAreInvalid(String email) {
        //GIVEN
        user.setFirstName("Olha%");
        user.setSurname("Petrenko");
        user.setPatronymic("S");
        user.setEmail(email);
        user.setPhone("+38 050 294-51-08");
        user.setPassword("Pass159qaz$");
        List<String> expectedInvalidFields = Arrays.asList("firstName", "patronymic", "email");

        //WHEN
        List<String> actualInvalidFields = validator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"Password123", "pa", "Password@@@@", "password@123"})
    public void validateShouldReturnListWithInvalidFieldsWhenSurnameAndPasswordIsInvalid(String password) {
        //GIVEN
        user.setFirstName("Olha");
        user.setSurname("Petrenko");
        user.setPatronymic("S");
        user.setEmail("petrenko@gmail.com");
        user.setPhone("+38 050 294-51-08");
        user.setPassword(password);
        List<String> expectedInvalidFields = Arrays.asList("patronymic", "password");

        //WHEN
        List<String> actualInvalidFields = validator.validate(user, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidFields, actualInvalidFields);
        verify(httpServletRequest).setAttribute("firstName", user.getFirstName());
        verify(httpServletRequest).setAttribute("patronymic", user.getPatronymic());
        verify(httpServletRequest).setAttribute("email", user.getEmail());
        verify(httpServletRequest).setAttribute("surname", user.getSurname());
        verify(httpServletRequest).setAttribute("password", user.getPassword());
        verify(httpServletRequest).setAttribute("phone", user.getPhone());
    }

}
