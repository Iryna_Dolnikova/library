package com.epam.validator;

import com.epam.web.dto.BookDto;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class OrderDtoValidatorTest {

    @Mock
    private HttpServletRequest httpServletRequest;

    private Validator<OrderDto> validator = new OrderDtoValidator();

    @Test
    public void validateShouldReturnInvalidFieldsListWhenBookIdLessThenZeroAndDateOfIssueIsBeforeCurrentDate() {
        //GIVEN
        OrderDto orderDto = new OrderDto();
        orderDto.setBook(new BookDto(-1));
        orderDto.setReader(new UserDto(2));
        orderDto.setDateOfIssue(Date.valueOf(LocalDate.now().minusDays(2)));
        orderDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().plusDays(3)));
        List<String> expectedInvalidNames = Arrays.asList("bookId", "dateOfIssue");

        //WHEN
        List<String> actualInvalidName = validator.validate(orderDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("bookId", orderDto.getBook().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", orderDto.getDateOfIssue());
        verify(httpServletRequest).setAttribute("readerId", orderDto.getReader().getId());
        verify(httpServletRequest).setAttribute("expectedReturnDate", orderDto.getExpectedReturnDate());
    }

    @Test
    public void validateShouldReturnInvalidFieldsListWhenReaderIdLessThenZeroAndExpectedReturnDateIsLessThenDateOfIssue() {
        //GIVEN
        OrderDto orderDto = new OrderDto();
        orderDto.setBook(new BookDto(1));
        orderDto.setReader(new UserDto(-2));
        orderDto.setDateOfIssue(Date.valueOf(LocalDate.now()));
        orderDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().minusDays(7)));
        List<String> expectedInvalidNames = Arrays.asList("readerId", "expectedReturnDate");

        //WHEN
        List<String> actualInvalidName = validator.validate(orderDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("readerId", orderDto.getReader().getId());
        verify(httpServletRequest).setAttribute("expectedReturnDate", orderDto.getExpectedReturnDate());
        verify(httpServletRequest).setAttribute("bookId", orderDto.getBook().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", orderDto.getDateOfIssue());
    }

    @Test
    public void validateShouldReturnEmptyListWhenFieldsAreValid() {
        //GIVEN
        OrderDto orderDto = new OrderDto();
        orderDto.setBook(new BookDto(1));
        orderDto.setReader(new UserDto(2));
        orderDto.setDateOfIssue(Date.valueOf(LocalDate.now()));
        orderDto.setExpectedReturnDate(Date.valueOf(LocalDate.now().plusDays(4)));
        List<String> expectedInvalidNames = new ArrayList<>();

        //WHEN
        List<String> actualInvalidName = validator.validate(orderDto, httpServletRequest);

        //THEN
        assertEquals(expectedInvalidNames, actualInvalidName);
        verify(httpServletRequest).setAttribute("bookId", orderDto.getBook().getId());
        verify(httpServletRequest).setAttribute("readerId", orderDto.getReader().getId());
        verify(httpServletRequest).setAttribute("dateOfIssue", orderDto.getDateOfIssue());
        verify(httpServletRequest).setAttribute("expectedReturnDate", orderDto.getExpectedReturnDate());
    }
}
