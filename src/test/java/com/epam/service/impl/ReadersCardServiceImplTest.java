package com.epam.service.impl;

import com.epam.dao.ReadersCardDao;
import com.epam.exception.BookAbsentException;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.exception.ReaderBlockedException;
import com.epam.model.Book;
import com.epam.model.ReaderCard;
import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import com.epam.service.BookService;
import com.epam.service.UserService;
import com.epam.web.converter.ReadersBookConverter;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.ReadersCardSearchDto;
import com.epam.web.dto.UserDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.epam.datagenerator.ReaderBookDataGenerator.generateReaderBook;
import static com.epam.datagenerator.ReaderBookDataGenerator.generateReaderBookDto;
import static com.epam.datagenerator.ReaderBookDataGenerator.generateReaderBookDtoFromReaderBook;
import static com.epam.datagenerator.ReaderBookDataGenerator.generateReaderBookDtoListFromReaderBooks;
import static com.epam.datagenerator.UserDataGenerator.generateLibrarian;
import static com.epam.datagenerator.UserDataGenerator.generateReader;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class ReadersCardServiceImplTest {

    @Mock
    private ReadersCardDao readersCardDao;
    @Mock
    private ReadersBookConverter readersBookConverter;
    @Mock
    private UserService userService;
    @Mock
    private BookService bookService;

    @InjectMocks
    private ReadersCardServiceImpl readersBooksService;

    @Test
    public void findReadersBooksByIdThrowObjectNotFoundByIDExceptionWhenReadersBookNotExist() {
        //GIVEN
        String expectedException = "Readers Book does not exist";
        int id = 3;
        when(readersCardDao.findReaderBookById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class, () -> readersBooksService.findReadersBooksById(id));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
    }

    @Test
    public void findReadersBooksByIdShouldReturnReadersBooksDto() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        ReaderCard readerCard = new ReaderCard(id, expectedReturnDate, actualReturnDate);
        ReadersCardDto readersCardDto = ReadersCardDto.Builder.builder().id(id)
                .expectedReturnDate(expectedReturnDate)
                .actualReturnDate(actualReturnDate)
                .build();

        when(readersCardDao.findReaderBookById(id)).thenReturn(Optional.of(readerCard));
        when(readersBookConverter.toReaderBookDto(readerCard)).thenReturn(readersCardDto);

        //WHEN
        ReadersCardDto actualReadersCardDto = readersBooksService.findReadersBooksById(id);

        //THEN
        assertNotNull(actualReadersCardDto);
        assertEquals(readersCardDto, actualReadersCardDto);
    }

    @Test
    @DisplayName("Find all readers cards when all their count = 0")
    public void findAllReadersCardsShouldReturnEmptyListAndPageCountIs0() {
        //GIVEN
        int currentPage = 0;
        List<ReadersCardDto> readerCards = new ArrayList<>();
        ReadersCardSearchDto expectedReadersCardSearchDto = new ReadersCardSearchDto(readerCards, 0);
        when(readersCardDao.findReadersBooksBySearchParameter(null)).thenReturn(new ArrayList<>());

        //WHEN
        ReadersCardSearchDto actualReadersCardSearchDto = readersBooksService.findAllReadersBooks(null, null, null, currentPage);

        //THEN
        assertEquals(expectedReadersCardSearchDto, actualReadersCardSearchDto);
    }

    @Test
    @DisplayName("Find all ReadersCards when all search parameters are null")
    public void findAllReadersBooksShouldReturnListReadersBooksDto() {
        //GIVEN
        int id = 5;
        int id2 = 7;
        int currentPage = 1;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        LocalDate localDate2 = LocalDate.of(2022, 1, 12);
        LocalDate localDate3 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate1 = Date.valueOf(localDate2);
        Date actualReturnDate2 = Date.valueOf(localDate3);

        User user = new User();
        user.setSurname("User");

        List<ReaderCard> readerCardList = new ArrayList<>();
        readerCardList.add(new ReaderCard(id, expectedReturnDate, actualReturnDate));
        readerCardList.add(new ReaderCard(id2, expectedReturnDate1, actualReturnDate2));
        readerCardList.forEach(readerBook -> {
            readerBook.setReader(user);
            readerBook.setLibrarian(user);
        });

        UserDto userDto = new UserDto();
        userDto.setSurname("User");

        List<ReadersCardDto> readersCardDtoList = new ArrayList<>();
        readersCardDtoList.add(new ReadersCardDto(id, expectedReturnDate, actualReturnDate));
        readersCardDtoList.add(new ReadersCardDto(id2, expectedReturnDate1, actualReturnDate2));
        readersCardDtoList.forEach(readerBook -> {
            readerBook.setReader(userDto);
            readerBook.setLibrarian(userDto);
        });
        ReadersCardSearchDto readersCardSearchDto = new ReadersCardSearchDto(readersCardDtoList, currentPage);

        when(readersCardDao.findReadersBooksBySearchParameter(null)).thenReturn(readerCardList);
        when(readersBookConverter.toListReaderBookDto(readerCardList)).thenReturn(readersCardDtoList);

        //WHEN
        ReadersCardSearchDto actualReadersCardDto = readersBooksService.findAllReadersBooks(null, null, null, currentPage);

        //THEN
        assertEquals(readersCardSearchDto, actualReadersCardDto);
    }

    @Test
    @DisplayName("Find all ReadersCards by readerName")
    public void findAllReadersBooksShouldReturnFilterListReadersBooksDtoByReaderName() {
        //GIVEN
        int currentPage = 1;
        User reader1 = generateReader("Tarasova", "Anna", "tarasova@gmail.com", "12-15-78");
        User reader2 = generateReader("Tarasenko", "Hanna", "tarasenko@gmail.com", "12-18-75");
        User reader3 = generateReader("Kuzmenko", "Serhiy", "kuzmenko@gmail.com", "13-20-75");

        User librarian1 = generateLibrarian("Ivanova", "Vasulyna", "vasylyna@gmail.com", "12-18-75");
        User librarian2 = generateLibrarian("Petrenko", "Igor", "petrenko@gmail.com", "13-20-75");

        List<ReaderCard> readerCardList = new ArrayList<>();
        readerCardList.add(generateReaderBook(5, reader1, librarian1, new Book(1, "Palyanitsa", 3)));
        readerCardList.add(generateReaderBook(7, reader2, librarian1, new Book(2, "Magic", 5)));
        readerCardList.add(generateReaderBook(8, reader3, librarian2, new Book(3, "Sky", 12)));

        List<ReaderCard> filteredReaderCardList = Arrays.asList(readerCardList.get(0), readerCardList.get(1));
        List<ReadersCardDto> ReadersCardDtoList = generateReaderBookDtoListFromReaderBooks(filteredReaderCardList);

        ReadersCardSearchDto readersCardSearchDto = new ReadersCardSearchDto(ReadersCardDtoList, currentPage);
        when(readersCardDao.findReadersBooksBySearchParameter(null)).thenReturn(readerCardList);
        when(readersBookConverter.toListReaderBookDto(filteredReaderCardList)).thenReturn(ReadersCardDtoList);

        //WHEN
        ReadersCardSearchDto actualReadersCardDto = readersBooksService.findAllReadersBooks(null, "Tar", null, currentPage);

        //THEN
        assertEquals(readersCardSearchDto, actualReadersCardDto);
    }

    @Test
    @DisplayName("Find all ReadersCards by readerName & librarianName")
    public void findAllReadersBooksShouldReturnFilteredListReadersBooksDtoByReaderAndLibrarianName() {
        //GIVEN
        int currentPage = 0;
        User reader1 = generateReader("Tarasova", "Anna", "tarasova@gmail.com", "12-15-78");
        User reader2 = generateReader("Tarasenko", "Hanna", "tarasenko@gmail.com", "12-18-75");
        User reader3 = generateReader("Kuzmenko", "Serhiy", "kuzmenko@gmail.com", "13-20-75");

        User librarian1 = generateLibrarian("Ivanova", "Vasulyna", "vasylyna@gmail.com", "12-18-75");
        User librarian2 = generateLibrarian("Petrenko", "Igor", "petrenko@gmail.com", "13-20-75");

        List<ReaderCard> readerCardList = new ArrayList<>();
        readerCardList.add(generateReaderBook(5, reader1, librarian2, new Book(1, "Ptashka", 12)));
        readerCardList.add(generateReaderBook(7, reader2, librarian1, new Book(2, "Magic", 5)));
        readerCardList.add(generateReaderBook(7, reader3, librarian1, new Book(2, "Morninig", 5)));
        readerCardList.add(generateReaderBook(8, reader2, librarian2, new Book(1, "Sky", 11)));
        readerCardList.add(generateReaderBook(9, reader3, librarian1, new Book(4, "Summer", 12)));
        readerCardList.add(generateReaderBook(10, reader1, librarian2, new Book(5, "Winter", 12)));
        readerCardList.add(generateReaderBook(11, reader2, librarian2, new Book(6, "Holiday", 12)));
        readerCardList.add(generateReaderBook(12, reader1, librarian2, new Book(4, "Summer", 12)));
        readerCardList.add(generateReaderBook(13, reader1, librarian2, new Book(5, "Magic", 12)));
        readerCardList.add(generateReaderBook(14, reader2, librarian2, new Book(6, "Ptashka", 12)));

        List<ReaderCard> filteredReaderCardList = Arrays.asList(readerCardList.get(0), readerCardList.get(3),
                readerCardList.get(5), readerCardList.get(6), readerCardList.get(7));
        List<ReadersCardDto> readerCardListDto = generateReaderBookDtoListFromReaderBooks(filteredReaderCardList);
        ReadersCardSearchDto readersCardSearchDto = new ReadersCardSearchDto(readerCardListDto, 2);

        when(readersCardDao.findReadersBooksBySearchParameter(null)).thenReturn(readerCardList);
        when(readersBookConverter.toListReaderBookDto(filteredReaderCardList)).thenReturn(readerCardListDto);

        //WHEN
        ReadersCardSearchDto actualReadersCardDto = readersBooksService.findAllReadersBooks(null,
                "Tar", "Pet", currentPage);

        //THEN
        assertEquals(readersCardSearchDto, actualReadersCardDto);
    }

    @Test
    public void deleteReaderBookShouldDeleteCardWhenItWasReturn() {
        //GIVEN
        int count = 0;
        ReaderCard readerCard = new ReaderCard();
        readerCard.setId(1);
        readerCard.setActualReturnDate(Date.valueOf(LocalDate.now()));

        when(readersCardDao.findReaderBookById(readerCard.getId())).thenReturn(Optional.of(readerCard));
        when(readersCardDao.deleteReaderBook(readerCard.getId())).thenReturn(count);

        //WHEN
        int actualCount = readersBooksService.deleteReaderBook(readerCard.getId());

        //THEN
        assertEquals(count, actualCount);
        verify(readersCardDao).deleteReaderBook(readerCard.getId());
    }

    @Test
    public void deleteReaderBookShouldNotDeleteCardWhenItWasNotReturn() {
        //GIVEN
        int count = 0;
        ReaderCard readerCard = new ReaderCard();
        readerCard.setId(1);

        when(readersCardDao.findReaderBookById(readerCard.getId())).thenReturn(Optional.of(readerCard));

        //WHEN
        assertThrows(CannotDeleteException.class, () -> readersBooksService.deleteReaderBook(readerCard.getId()));

        //THEN
        verify(readersCardDao, never()).deleteReaderBook(readerCard.getId());
    }

    @Test
    public void updateReaderBookShouldReturnReadersBooksDtoWhenBookWasNotUpdated() {
        //GIVEN
        LocalDate localDateOfIssue = LocalDate.of(2022, 1, 5);
        LocalDate localDateExpected = LocalDate.of(2022, 2, 5);
        LocalDate localDateActual = LocalDate.of(2022, 2, 10);

        BookDto updatedBookDto = new BookDto(1, "Вечір", 16);
        BookDto bookDtoFromDb = new BookDto(1, "Вечір", 16);

        UserDto readerDto = new UserDto(1, "Tarasova", "Anna", "Andriyvna", "tarasova@gmail.com", "12-15-78");
        UserDto librarianDto = new UserDto(2, "Denysenko", "Olga", "Andriyvna", "denysenko@gmail.com", "13-16-79");

        ReadersCardDto updatedReaderBookDto = generateReaderBookDto(localDateOfIssue, localDateExpected,
                localDateActual, updatedBookDto, readerDto, librarianDto);
        ReadersCardDto readerBookDtoFromDataBase = generateReaderBookDto(localDateOfIssue, localDateExpected,
                localDateActual, bookDtoFromDb, readerDto, librarianDto);

        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);
        when(userService.findLibrarianById(librarianDto.getId())).thenReturn(librarianDto);
        when(bookService.findBookById(updatedBookDto.getId())).thenReturn(bookDtoFromDb);
        when(readersCardDao.findReaderBookById(updatedReaderBookDto.getId())).thenReturn(Optional.of(new ReaderCard()));
        when(readersBookConverter.toReaderBookDto(any(ReaderCard.class))).thenReturn(readerBookDtoFromDataBase);
        when(readersCardDao.updateReaderBook(updatedReaderBookDto)).thenReturn(updatedReaderBookDto);

        //WHEN
        ReadersCardDto actualReadersCardDto = readersBooksService.updateReaderBook(updatedReaderBookDto);

        //THEN
        assertEquals(updatedReaderBookDto, actualReadersCardDto);
        verify(bookService, Mockito.never()).updateBook(updatedBookDto, updatedBookDto.getId());
        verify(bookService, Mockito.never()).updateBook(bookDtoFromDb, bookDtoFromDb.getId());
    }

    @Test
    public void updateReaderBookShouldReturnReadersBooksDtoWhenBookWasUpdated() {
        //GIVEN
        LocalDate localDateOfIssue = LocalDate.of(2022, 1, 5);
        LocalDate localDateExpected = LocalDate.of(2022, 2, 5);
        LocalDate localDateActual = LocalDate.of(2022, 2, 10);

        BookDto updatedBookDto = new BookDto(1, "Sky", 16);
        BookDto bookDtoFromDb = new BookDto(2, "Kobzar", 87);

        UserDto readerDto = new UserDto(1, "Tarasova", "Anna", "Andriyvna", "tarasova@gmail.com", "12-15-78");
        UserDto librarianDto = new UserDto(2, "Denysenko", "Olga", "Andriyvna", "denysenko@gmail.com", "13-16-79");

        ReadersCardDto updatedReaderBookDto = generateReaderBookDto(localDateOfIssue, localDateExpected,
                localDateActual, updatedBookDto, readerDto, librarianDto);
        ReadersCardDto readerBookDtoFromDataBase = generateReaderBookDto(localDateOfIssue, localDateExpected,
                localDateActual, bookDtoFromDb, readerDto, librarianDto);

        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);
        when(userService.findLibrarianById(librarianDto.getId())).thenReturn(librarianDto);
        when(bookService.findBookById(updatedBookDto.getId())).thenReturn(updatedBookDto);
        when(bookService.updateBook(updatedBookDto, updatedBookDto.getId())).thenReturn(updatedBookDto);

        when(readersCardDao.findReaderBookById(updatedReaderBookDto.getId())).thenReturn(Optional.of(new ReaderCard()));
        when(readersBookConverter.toReaderBookDto(any(ReaderCard.class))).thenReturn(readerBookDtoFromDataBase);

        when(readersCardDao.updateReaderBook(updatedReaderBookDto)).thenReturn(updatedReaderBookDto);

        //WHEN
        ReadersCardDto actualReadersCardDto = readersBooksService.updateReaderBook(updatedReaderBookDto);

        //THEN
        assertEquals(updatedReaderBookDto, actualReadersCardDto);
        verify(bookService).updateBook(updatedBookDto, updatedBookDto.getId());
        verify(bookService).updateBook(bookDtoFromDb, bookDtoFromDb.getId());
    }

    @Test
    public void updateReaderBookShouldThrowExceptionWhenBookCountEqualsZero() {
        //GIVEN
        LocalDate localDateOfIssue = LocalDate.of(2022, 1, 5);
        LocalDate localDateExpected = LocalDate.of(2022, 2, 5);
        LocalDate localDateActual = LocalDate.of(2022, 2, 10);

        BookDto updatedBookDto = new BookDto(1, "Вечір", 0);
        BookDto bookDtoFromDb = new BookDto(2, "Кобзар", 12);

        UserDto readerDto = new UserDto(1, "Тарасова", "Ганна", "Андріївна", "tarasova@gmail.com", "12-15-78");
        UserDto librarianDto = new UserDto(2, "Денісова", "Ольга", "Андріївна", "denisova@gmail.com", "13-16-79");

        ReadersCardDto updatedReaderBookDto = generateReaderBookDto(localDateOfIssue, localDateExpected,
                localDateActual, updatedBookDto, readerDto, librarianDto);
        ReadersCardDto readerBookDtoFromDataBase = generateReaderBookDto(localDateOfIssue, localDateExpected,
                localDateActual, bookDtoFromDb, readerDto, librarianDto);

        String expectedException = "Sorry, book is absent";

        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);
        when(userService.findLibrarianById(librarianDto.getId())).thenReturn(librarianDto);
        when(bookService.findBookById(updatedBookDto.getId())).thenReturn(updatedBookDto);
        when(readersCardDao.findReaderBookById(updatedReaderBookDto.getId())).thenReturn(Optional.of(new ReaderCard()));
        when(readersBookConverter.toReaderBookDto(any(ReaderCard.class))).thenReturn(readerBookDtoFromDataBase);

        //WHEN
        Exception actualException = assertThrows(BookAbsentException.class,
                () -> readersBooksService.updateReaderBook(updatedReaderBookDto));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
//        verify(bookService, times(2)).updateBook(any(), anyInt());

        verify(bookService, Mockito.never()).updateBook(updatedBookDto, updatedBookDto.getId());
        verify(bookService, Mockito.never()).updateBook(bookDtoFromDb, bookDtoFromDb.getId());
    }

    @Test
    public void penaltyForLateReturnShouldReturnPenaltyWhenActualReturnDateIsNotNullAndActualDateMoreThenExpectedDate() {
        //GIVEN
        int id = 1;
        int penalty = 25;
        LocalDate localDate = LocalDate.of(2022, 2, 5);
        LocalDate localDate1 = LocalDate.of(2022, 2, 10);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        ReadersCardDto readersCardDto = new ReadersCardDto(id, expectedReturnDate, actualReturnDate);

        //WHEN
        int actualPenalty = readersBooksService.penaltyForLateReturn(readersCardDto);

        //THEN
        assertEquals(penalty, actualPenalty);

    }

    @Test
    public void penaltyForLateReturnShouldReturnPenaltyWhenActualReturnDateIsNull() {
        //GIVEN
        int id = 1;
        int penalty = 50;
        LocalDate localDate = LocalDate.now().minusDays(10);
        Date expectedReturnDate = Date.valueOf(localDate);
        ReadersCardDto readersCardDto = new ReadersCardDto(id, expectedReturnDate, null);

        //WHEN
        int actualPenalty = readersBooksService.penaltyForLateReturn(readersCardDto);

        //THEN
        assertEquals(penalty, actualPenalty);
    }

    @Test
    public void penaltyForLateReturnShouldReturn0WhenActualReturnDateIsNullAndActualDateLessThenExpectedDate() {
        //GIVEN
        int id = 1;
        int penalty = 0;
        LocalDate localDate = LocalDate.now();
        Date expectedReturnDate = Date.valueOf(localDate);
        ReadersCardDto readersCardDto = new ReadersCardDto(id, expectedReturnDate, null);

        //WHEN
        int actualPenalty = readersBooksService.penaltyForLateReturn(readersCardDto);

        //THEN
        assertEquals(penalty, actualPenalty);
    }

    @Test
    public void insertReaderBookShouldReturnReadersBooksDtoWhenBookCountMoreThen0() {
        //GIVEN
        Date expectedReturnDate = Date.valueOf(LocalDate.of(2022, 1, 12));
        Date actualReturnDate = Date.valueOf(LocalDate.of(2022, 1, 12));
        Date dateOfIssue = Date.valueOf(LocalDate.of(2020, 1, 1));

        UserDto userDtoReader = new UserDto(5);
        userDtoReader.setBlocked(false);

        UserDto userDtoLibrarian = new UserDto(3);
        BookDto bookDto = new BookDto(2);
        bookDto.setCount(7);
        ReadersCardDto readersCardDto = new ReadersCardDto(1, dateOfIssue, expectedReturnDate, actualReturnDate,
                userDtoReader, userDtoLibrarian, bookDto, 0);

        when(userService.findReaderById(readersCardDto.getReader().getId())).thenReturn(userDtoReader);
        when(bookService.findBookById(bookDto.getId())).thenReturn(bookDto);
        when(readersCardDao.insertReaderBook(readersCardDto)).thenReturn(readersCardDto);

        //WHEN
        ReadersCardDto actualReadersCardDto = readersBooksService.insertReaderBook(readersCardDto);

        //THEN
        assertNotNull(actualReadersCardDto);
        assertEquals(readersCardDto, actualReadersCardDto);
    }

    @Test
    public void insertReaderBookShouldThrowBookAbsentExceptionWhenBookCountEquals0() {
        //GIVEN
        String expectedException = "Sorry, book is absent";
        Date expectedReturnDate = Date.valueOf(LocalDate.of(2022, 1, 12));
        Date actualReturnDate = Date.valueOf(LocalDate.of(2022, 1, 12));
        Date dateOfIssue = Date.valueOf(LocalDate.of(2020, 1, 1));

        UserDto userDtoReader = new UserDto(5);
        userDtoReader.setBlocked(false);

        UserDto userDtoLibrarian = new UserDto(3);
        BookDto bookDto = new BookDto(2);
        bookDto.setCount(0);
        ReadersCardDto readersCardDto = new ReadersCardDto(1, dateOfIssue, expectedReturnDate, actualReturnDate,
                userDtoReader, userDtoLibrarian, bookDto, 0);

        when(userService.findReaderById(readersCardDto.getReader().getId())).thenReturn(userDtoReader);
        when(bookService.findBookById(bookDto.getId())).thenReturn(bookDto);

        //WHEN
        Exception actualException = assertThrows(BookAbsentException.class, () -> readersBooksService.insertReaderBook(readersCardDto));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
    }

    @Test
    public void findReaderBookByLibrarianIdShouldReturnListReadersCardDto() {
        //GIVEN
        int librarianId = 5;
        List<ReaderCard> readerCardList = new ArrayList<>();
        readerCardList.add(generateReaderBook(1, new User(4), new User(librarianId), new Book(7)));
        readerCardList.add(generateReaderBook(2, new User(5), new User(librarianId), new Book(9)));

        List<ReadersCardDto> expectedReadersCardDto = new ArrayList<>();
        expectedReadersCardDto.add(generateReaderBookDtoFromReaderBook(readerCardList.get(0)));
        expectedReadersCardDto.add(generateReaderBookDtoFromReaderBook(readerCardList.get(1)));

        when(readersCardDao.findReaderBookByLibrarianId(librarianId)).thenReturn(readerCardList);
        when(readersBookConverter.toListReaderBookDto(readerCardList)).thenReturn(expectedReadersCardDto);

        //WHEN
        List<ReadersCardDto> actualReadersCardDto = readersBooksService.findReaderBookByLibrarianId(librarianId);

        //THEN
        assertEquals(expectedReadersCardDto, actualReadersCardDto);
    }

    @Test
    public void findReaderBookByReaderIdShouldReturnListReadersCardDto() {
        //GIVEN
        int readerId = 5;
        List<ReaderCard> readerCardList = new ArrayList<>();
        readerCardList.add(generateReaderBook(1, new User(readerId), new User(3), new Book(7)));
        readerCardList.add(generateReaderBook(2, new User(readerId), new User(5), new Book(9)));

        List<ReadersCardDto> expectedReadersCardDto = new ArrayList<>();
        expectedReadersCardDto.add(generateReaderBookDtoFromReaderBook(readerCardList.get(0)));
        expectedReadersCardDto.add(generateReaderBookDtoFromReaderBook(readerCardList.get(1)));

        when(readersCardDao.findReaderBookByReaderId(readerId)).thenReturn(readerCardList);
        when(readersBookConverter.toListReaderBookDto(readerCardList)).thenReturn(expectedReadersCardDto);

        //WHEN
        List<ReadersCardDto> actualReadersCardDto = readersBooksService.findReaderBookByReaderID(readerId);

        //THEN
        assertEquals(expectedReadersCardDto, actualReadersCardDto);
    }

    @Test
    public void insertReaderBookShouldThrowReaderBlockedExceptionWhenReaderIsBlocked() {
        //GIVEN
        Date expectedReturnDate = Date.valueOf(LocalDate.of(2022, 1, 12));
        Date actualReturnDate = Date.valueOf(LocalDate.of(2022, 1, 12));
        Date dateOfIssue = Date.valueOf(LocalDate.of(2020, 1, 1));

        UserDto userDtoReader = new UserDto(1, "Tarasova A.А.", "Tarasova", "Anna", "Andriyvna",
                "tarasova@gmail.com", "12-15-78", RolesEnum.READER, true);

        UserDto userDtoLibrarian = new UserDto(3);
        BookDto bookDto = new BookDto(2);
        bookDto.setCount(7);
        ReadersCardDto readersCardDto = new ReadersCardDto(1, dateOfIssue, expectedReturnDate, actualReturnDate,
                userDtoReader, userDtoLibrarian, bookDto, 0);

        when(userService.findReaderById(readersCardDto.getReader().getId())).thenReturn(userDtoReader);

        String expectedException = "Sorry, reader is blocked";

        //WHEN
        Exception actualException = assertThrows(ReaderBlockedException.class, () -> readersBooksService.insertReaderBook(readersCardDto));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
    }

}