package com.epam.service.impl;

import com.epam.dao.BooksDao;
import com.epam.dao.PublishingHousesDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.Book;
import com.epam.model.City;
import com.epam.model.PublishingHouse;
import com.epam.service.CityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PublishingHouseServiceImplTest {
    @Mock
    private PublishingHousesDao publishingHousesDao;
    @Mock
    private CityService cityService;
    @Mock
    private BooksDao booksDao;

    @InjectMocks
    private PublishingHouseServiceImpl publishingHouseService;

    @Test
    public void findPHByIdShouldThrowObjectNotFoundByIDExceptionWhenPHNotFound() {
        //GIVEN
        String expectedException = "Publishing house does not exist";
        int id = 1;
        when(publishingHousesDao.findPubHouseById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class,
                () -> publishingHouseService.findPubHouseById(id));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
    }

    @Test
    public void findPHByIdShouldReturnPublishingHouse() {
        //GIVEN
        PublishingHouse expectedPubHouse = new PublishingHouse(1, "Фоліо");
        when(publishingHousesDao.findPubHouseById(expectedPubHouse.getId())).thenReturn(Optional.of(expectedPubHouse));

        //WHEN
        PublishingHouse actualPublishingHouse = publishingHouseService.findPubHouseById(expectedPubHouse.getId());

        //THEN
        assertEquals(expectedPubHouse, actualPublishingHouse);

    }

    @Test
    public void findAllPubHousesShouldReturnListOfPubHouses() {
        //GIVEN
        List<PublishingHouse> expectedPublishingHouseList = new ArrayList<>();
        PublishingHouse publishingHouse = new PublishingHouse(1, "Пірамида");
        PublishingHouse publishingHouse1 = new PublishingHouse(2, "Фоліо");
        PublishingHouse publishingHouse2 = new PublishingHouse(3, "Дух і літера");
        expectedPublishingHouseList.add(publishingHouse);
        expectedPublishingHouseList.add(publishingHouse1);
        expectedPublishingHouseList.add(publishingHouse2);
        when(publishingHousesDao.findAllPublishingHouses()).thenReturn(expectedPublishingHouseList);

        //WHEN
        List<PublishingHouse> actualPubHouses = publishingHouseService.findAll();

        //THEN
        assertFalse(actualPubHouses.isEmpty());
        assertSame(expectedPublishingHouseList.size(), actualPubHouses.size());
        assertEquals(expectedPublishingHouseList, actualPubHouses);

    }

    @Test
    public void updatePublishingHouseShouldReturnPubHouses() {
        //GIVEN
        PublishingHouse expectedPublishingHouse = new PublishingHouse(3, "Країна мрій");
        when(publishingHousesDao.updatePublishingHouse(expectedPublishingHouse)).thenReturn(expectedPublishingHouse);

        //WHEN
        PublishingHouse actualPubHouse = publishingHouseService.updatePublishingHouse(expectedPublishingHouse);

        //THEN
        assertNotNull(actualPubHouse);
        assertEquals(expectedPublishingHouse, actualPubHouse);
    }

    @Test
    public void deletePublishingHouseShouldReturnCountWhenPubHouseDoesNotRelatedWithAnyBooks() {
        //GIVEN
        int id = 2;
        int count = 0;
        PublishingHouse publishingHouse = new PublishingHouse(id, "Fabul");

        when(publishingHousesDao.findPubHouseById(id)).thenReturn(Optional.of(publishingHouse));
        when(booksDao.findBookBySearchParameters(null, publishingHouse.getName(), null))
                .thenReturn(new ArrayList<>());
        when(publishingHousesDao.deletePublishingHouse(id)).thenReturn(count);

        //WHEN
        int actualCount = publishingHouseService.deletePublishingHouse(id);

        //THEN
        assertEquals(count, actualCount);
    }

    @Test
    public void deletePublishingHouseShouldThrowCannotDeleteExceptionWhenPubHouseRelatedWithBooks() {
        //GIVEN
        int id = 2;
        int count = 0;
        PublishingHouse publishingHouse = new PublishingHouse(id, "Fabul");

        when(publishingHousesDao.findPubHouseById(id)).thenReturn(Optional.of(publishingHouse));
        when(booksDao.findBookBySearchParameters(null, publishingHouse.getName(), null))
                .thenReturn(Collections.singletonList(new Book()));

        //WHEN
        assertThrows(CannotDeleteException.class, () -> publishingHouseService.deletePublishingHouse(id));

        //THEN
        verify(publishingHousesDao, never()).deletePublishingHouse(id);
    }

    @Test
    public void insertPublishingHouseShouldReturnPublishingHouseWhenPublishingHouseAbsent() {
        //GIVEN
        City city = new City(1, "Дніпро");
        PublishingHouse publishingHouse = new PublishingHouse(2, "Ранок", city);
        when(cityService.findCityById(city.getId())).thenReturn(city);
        when(publishingHousesDao.existPubHouse("Ранок", city.getId())).thenReturn(false);
        when(publishingHousesDao.insertPublishingHouse(publishingHouse)).thenReturn(publishingHouse);

        //WHEN
        PublishingHouse actualPubHouse = publishingHouseService.insertPublishingHouse(publishingHouse);

        //THEN
        assertEquals(publishingHouse, actualPubHouse);
        verify(publishingHousesDao).insertPublishingHouse(publishingHouse);
    }

    @Test
    public void insertPublishingHouseShouldThrowObjectAlreadyExistExceptionWhenPublishingHouseExist() {
        //GIVEN
        City city = new City(2, "Київ");
        PublishingHouse publishingHouse = new PublishingHouse(3, "Човен", city);
        String expectedException = "Sorry, publishing house already exists";

        when(cityService.findCityById(city.getId())).thenReturn(city);
        when(publishingHousesDao.existPubHouse(publishingHouse.getName(), city.getId())).thenReturn(true);

        //WHEN
        Exception actualException = assertThrows(ObjectAlreadyExistException.class,
                () -> publishingHouseService.insertPublishingHouse(publishingHouse));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
        Mockito.verify(publishingHousesDao, Mockito.never()).insertPublishingHouse(publishingHouse);
    }

}
