package com.epam.service.impl;

import com.epam.dao.ReadersCardDao;
import com.epam.dao.UsersDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.exception.UserDoesNotExistException;
import com.epam.model.ReaderCard;
import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import com.epam.web.converter.UserConverter;
import com.epam.web.dto.LoginDto;
import com.epam.web.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UsersDao usersDao;
    @Mock
    private ReadersCardDao readersCardDao;
    @Mock
    private UserConverter userConverter;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void findReaderByIdShouldThrowObjectNotFoundByIDExceptionWhenReaderNotFound() {
        //GIVEN
        String expectedExceptionMessage = "Reader does not exist";
        int id = 1;
        when(usersDao.findActiveReaderById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class, () -> userService.findReaderById(id));

        //THEN
        assertEquals(expectedExceptionMessage, actualException.getMessage());
        Mockito.verify(userConverter, Mockito.never()).toUserDto(Mockito.any());
    }

    @Test
    public void findReaderByIdShouldReturnUserDtoWhenReaderExist() {
        //GIVEN
        int id = 2;
        User user = new User(id);
        UserDto userDto = new UserDto(id);
        when(usersDao.findActiveReaderById(id)).thenReturn(Optional.of(user));
        when(userConverter.toUserDto(user)).thenReturn(userDto);

        //WHEN
        UserDto actualUserDto = userService.findReaderById(id);

        //THEN
        assertEquals(userDto, actualUserDto);
    }

    @Test
    public void findLibrarianByIdShouldThrowObjectNotFoundByIDExceptionWhenLibrarianNotFound() {
        //GIVEN
        String expectedMassageException = "Librarian does not exist";
        int id = 3;
        when(usersDao.findActiveLibrarianById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class, () -> userService.findLibrarianById(id));

        //THEN
        assertEquals(expectedMassageException, actualException.getMessage());
        Mockito.verify(userConverter, Mockito.never()).toUserDto(Mockito.any());
    }

    @Test
    public void findLibrarianByIdShouldReturnUserDtoWhenLibrarianExist() {
        //GIVEN
        int id = 5;
        User user = new User(id);
        UserDto userDto = userConverter.toUserDto(user);
        when(usersDao.findActiveLibrarianById(id)).thenReturn(Optional.of(user));
        when(userConverter.toUserDto(user)).thenReturn(userDto);

        //WHEN
        UserDto actualUserDto = userService.findLibrarianById(id);

        //THEN
        assertEquals(userDto, actualUserDto);
    }

    @Test
    public void findAdminByIdShouldThrowObjectNotFoundByIDExceptionWhenAdminNotFound() {
        //GIVEN
        int adminId = 7;
        String expectedExceptionMessage = "Admin does not exist";
        when(usersDao.findAdminById(adminId)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class, () -> userService.findAdminById(adminId));

        //THEN
        assertEquals(expectedExceptionMessage, actualException.getMessage());

    }

    @Test
    public void findAdminByIdShouldReturnUserDtoWhenAdminExist() {
        //GIVEN
        int id = 5;
        User admin = new User(5);
        UserDto expectedAdminDto = userConverter.toUserDto(admin);
        when(usersDao.findAdminById(id)).thenReturn(Optional.of(admin));

        //WHEN
        UserDto actualAdminDto = userService.findAdminById(id);

        //THEN
        assertEquals(expectedAdminDto, actualAdminDto);
    }

    @Test
    public void findAllReadersShouldReturnListOfReaders() {
        //GIVEN
        List<User> expectedListAllUsers = new ArrayList<>();
        expectedListAllUsers.add(new User(1, "Iryna"));
        expectedListAllUsers.add(new User(2, "Yuliia"));

        List<UserDto> userDtoList = new ArrayList<>();
        userDtoList.add(new UserDto(1, "Iryna"));
        userDtoList.add(new UserDto(2, "Yuliia"));

        when(usersDao.findAllReaders()).thenReturn(expectedListAllUsers);
        when(userConverter.toUserDtoList(expectedListAllUsers)).thenReturn(userDtoList);

        //WHEN
        List<UserDto> actualUserDtoList = userService.findAllReaders();

        //THEN
        assertEquals(userDtoList, actualUserDtoList);
        assertSame(userDtoList.size(), actualUserDtoList.size());
        assertFalse(actualUserDtoList.isEmpty());
    }

    @Test
    public void findAllLLibrariansShouldReturnListOfLibrarians() {
        //GIVEN
        List<User> expectedListAllUsers = new ArrayList<>();
        expectedListAllUsers.add(new User(3, "Taras"));
        expectedListAllUsers.add(new User(4, "Ostap"));

        List<UserDto> userDtoList = new ArrayList<>();
        userDtoList.add(new UserDto(3, "Taras"));
        userDtoList.add(new UserDto(4, "Ostap"));

        when(usersDao.findAllLibrarian()).thenReturn(expectedListAllUsers);
        when(userConverter.toUserDtoList(expectedListAllUsers)).thenReturn(userDtoList);

        //WHEN
        List<UserDto> actualUserDtoList = userService.findAllLibrarians();

        //THEN
        assertFalse(actualUserDtoList.isEmpty());
        assertSame(userDtoList.size(), actualUserDtoList.size());
        assertEquals(userDtoList, actualUserDtoList);
    }

    @Test
    public void insertUserShouldReturnObjectAlreadyExistExceptionWhenUserExist() {
        //GIVEN
        User user = new User(1, "Dasha");
        String expectedExceptionMassage = "Sorry, user already exists";
        when(usersDao.existUserByPhoneOrEmail(user)).thenReturn(true);

        //WHEN
        Exception actualException = assertThrows(ObjectAlreadyExistException.class, () -> userService.insertUser(user));

        //THEN
        assertEquals(expectedExceptionMassage, actualException.getMessage());
        Mockito.verify(userConverter, Mockito.never()).toUserDto(user);
    }

    @Test
    public void insertUserShouldReturnUserDtoWhenUserAbsent() {
        //GIVEN
        User user = new User(4, "Sasha");
        UserDto userDto = new UserDto(4, "Sasha");
        when(usersDao.existUserByPhoneOrEmail(user)).thenReturn(false);
        when(usersDao.insertUser(user)).thenReturn(user);
        when(userConverter.toUserDto(user)).thenReturn(userDto);

        //WHEN
        UserDto actualUserDto = userService.insertUser(user);

        //THEN
        assertEquals(userDto, actualUserDto);
        verify(usersDao).insertUser(user);
    }

    @Test
    public void updateUserShouldReturnUSerDto() {
        //GIVEN
        User user = new User(5, "Tanya");
        UserDto userDto = new UserDto(5, "Tanya");
        when(usersDao.updateUser(user, user.getId())).thenReturn(user);
        when(userConverter.toUserDto(user)).thenReturn(userDto);

        //WHEN
        UserDto actualUserDto = userService.updateUser(user, user.getId());

        //THEN
        assertEquals(userDto, actualUserDto);
        assertNotNull(actualUserDto);
    }

    @Test
    public void deleteLibrarianShouldReturnCount() {
        //GIVEN
        int id = 5;
        int count = 1;

        when(usersDao.findActiveLibrarianById(id)).thenReturn(Optional.of(new User()));
        when(userConverter.toUserDto(any())).thenReturn(new UserDto());

        when(usersDao.deleteUser(id)).thenReturn(count);

        //WHEN
        int actualCount = userService.deleteLibrarian(id);

        //THEN
        assertEquals(count, actualCount);
        verify(usersDao).deleteUser(id);
    }


    @Test
    public void deleteReaderShouldReturnCountWhenReaderReturnedAllBooks() {
        //GIVEN
        int id = 5;
        int count = 1;

        ReaderCard readerBookWithNotNullActualDate = new ReaderCard();
        readerBookWithNotNullActualDate.setActualReturnDate(Date.valueOf(LocalDate.now().minusMonths(2)));

        ReaderCard readerBookWithNullActualDate = new ReaderCard();
        readerBookWithNullActualDate.setActualReturnDate(Date.valueOf(LocalDate.now().minusMonths(3)));

        List<ReaderCard> readerCards = Arrays.asList(readerBookWithNotNullActualDate, readerBookWithNullActualDate);

        when(usersDao.findActiveReaderById(id)).thenReturn(Optional.of(new User()));
        when(userConverter.toUserDto(any())).thenReturn(new UserDto());

        when(readersCardDao.findReaderBookByReaderId(id)).thenReturn(readerCards);
        when(usersDao.deleteUser(id)).thenReturn(count);

        //WHEN
        int actualCount = userService.deleteReader(id);

        //THEN
        assertEquals(count, actualCount);
        verify(usersDao).deleteUser(id);
    }

    @Test
    public void deleteReaderShouldThrowCannotDeleteExceptionWhenReaderDoesNotReturnBooks() {
        //GIVEN
        int id = 5;

        ReaderCard readerBookWithNotNullActualDate = new ReaderCard();
        readerBookWithNotNullActualDate.setActualReturnDate(Date.valueOf(LocalDate.now().minusMonths(2)));

        ReaderCard readerBookWithNullActualDate = new ReaderCard();
        readerBookWithNullActualDate.setActualReturnDate(null);

        List<ReaderCard> readerCards = Arrays.asList(readerBookWithNotNullActualDate, readerBookWithNullActualDate);

        when(usersDao.findActiveReaderById(id)).thenReturn(Optional.of(new User()));
        when(userConverter.toUserDto(any())).thenReturn(new UserDto());

        when(readersCardDao.findReaderBookByReaderId(id)).thenReturn(readerCards);

        //WHEN
        assertThrows(CannotDeleteException.class, () -> userService.deleteReader(id));

        //THEN
        verify(usersDao, never()).deleteUser(id);
    }

    @Test
    public void loginUserShouldReturnRole() {
        //GIVEN
        String email = "karpenko.taras@gmail.com";
        String password = "password123";
        LoginDto loginDto = new LoginDto(email, 1, RolesEnum.READER);
        when(usersDao.getLoginDtoByEmailAndPassword(email, password)).thenReturn(Optional.of(loginDto));

        //WHEN
        LoginDto actualLoginDto = userService.loginUser(email, password);

        //THEN
        assertEquals(loginDto, actualLoginDto);
    }

    @Test
    public void loginUserThrowUserDoesNotExistExceptionWhenRoleEqualsNull() {
        //GIVEN
        String email = "karpenko.taras@gmail.com";
        String password = "password123";
        String expectedMassage = "User with such email or password does not exist";
        when(usersDao.getLoginDtoByEmailAndPassword(email, password)).thenReturn(Optional.empty());

        //GIVEN
        Exception actualException = assertThrows(UserDoesNotExistException.class, () -> userService.loginUser(email, password));

        //THEN
        assertEquals(expectedMassage, actualException.getMessage());
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    public void blockUnblockReaderShouldReturnUserDto(boolean isReaderBlock) {
        //GIVEN
        User user = new User(5, "Тарасова Г.А.", "Тарасова", "Ганна", "Андріївна",
                "tarasova@gmail.com", "12-15-78", RolesEnum.READER, isReaderBlock);
        UserDto readerDto = new UserDto(5, "Тарасова Г.А.", "Тарасова", "Ганна", "Андріївна",
                "tarasova@gmail.com", "12-15-78", RolesEnum.READER, isReaderBlock);

        UserDto expectedReader = new UserDto(5, "Тарасова Г.А.", "Тарасова", "Ганна", "Андріївна",
                "tarasova@gmail.com", "12-15-78", RolesEnum.READER, !isReaderBlock);

        when(usersDao.findActiveReaderById(user.getId())).thenReturn(Optional.of(user));
        when(userConverter.toUserDto(user)).thenReturn(readerDto);

        //WHEN
        UserDto actualReader = userService.blockUnblockReader(readerDto.getId());

        //THEN
        assertEquals(expectedReader, actualReader);
        verify(usersDao).updateReaderBlock(user.getId(), !isReaderBlock);
    }
}
