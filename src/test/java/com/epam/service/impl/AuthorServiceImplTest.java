package com.epam.service.impl;

import com.epam.dao.AuthorsBooksDao;
import com.epam.dao.AuthorsDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.Author;
import com.epam.model.Book;
import com.epam.web.converter.AuthorConverter;
import com.epam.web.converter.BookConverter;
import com.epam.web.dto.AuthorDto;
import com.epam.web.dto.BookDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class AuthorServiceImplTest {

    @Mock
    private AuthorsDao authorsDao;
    @Mock
    private AuthorConverter authorConverter;
    @Mock
    private BookConverter bookConverter;
    @Mock
    private BookServiceImpl bookService;
    @Mock
    private AuthorsBooksDao authorsBooksDao;

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Test
    public void findAuthorByIdShouldThrowObjectNotFoundByIDExceptionWhenAuthorNotExist() {
        //GIVEN
        int id = 5;
        String expectedExceptionMassage = "Author does not exist";
        when(authorsDao.findAuthorById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class, () -> authorService.findAuthorById(id));

        //THEN
        assertEquals(expectedExceptionMassage, actualException.getMessage());
    }

    @Test
    public void findAuthorByIdShouldReturnAuthor() {
        //GIVEN
        int id = 3;
        Author author = new Author(id, "Іріна", "Дольнікова", "Володимирівна");
        AuthorDto authorDto = new AuthorDto(id, "Іріна", "Дольнікова", "Володимирівна");
        when(authorsDao.findAuthorById(id)).thenReturn(Optional.of(author));
        when(authorConverter.toAuthorDto(author)).thenReturn(authorDto);

        //WHEN
        AuthorDto actualAuthorDto = authorService.findAuthorById(id);

        //THEN
        assertNotNull(actualAuthorDto);
        assertEquals(authorDto, actualAuthorDto);

    }

    @Test
    public void findAllShouldReturnListAuthorDto() {
        //GIVEN
        List<Author> authorList = new ArrayList<>();
        authorList.add(new Author(1, "Іріна", "Дольнікова", "Володимирівна"));
        authorList.add(new Author(2, "Тетяна", "Гончарова", "Сергіївна"));

        List<AuthorDto> authorDtoList = new ArrayList<>();
        authorDtoList.add(new AuthorDto(1, "Іріна", "Дольнікова", "Володимирівна"));
        authorDtoList.add(new AuthorDto(2, "Тетяна", "Гончарова", "Сергіївна"));

        when(authorsDao.findAllAuthors()).thenReturn(authorList);
        when(authorConverter.toAuthorDtoList(authorList)).thenReturn(authorDtoList);

        //WHEN
        List<AuthorDto> actualAuthorDtoList = authorService.findAll();

        //THEN
        assertFalse(actualAuthorDtoList.isEmpty());
        assertSame(authorDtoList.size(), actualAuthorDtoList.size());
        assertEquals(authorDtoList, actualAuthorDtoList);
    }

    @Test
    public void findAuthorByNameShouldReturnListAuthorDto() {
        //GIVEN
        String name = "Дольнікова";
        List<Author> authorList = new ArrayList<>();
        authorList.add(new Author(1, "Іріна", "Дольнікова", "Володимирівна"));
        authorList.add(new Author(2, "Тетяна", "Гончарова", "Сергіївна"));

        List<AuthorDto> authorDtoList = new ArrayList<>();
        authorDtoList.add(new AuthorDto(1, "Іріна", "Дольнікова", "Володимирівна"));
        authorDtoList.add(new AuthorDto(2, "Тетяна", "Гончарова", "Сергіївна"));

        when(authorsDao.findAuthorsByName(name)).thenReturn(authorList);
        when(authorConverter.toAuthorDtoList(authorList)).thenReturn(authorDtoList);

        //WHEN
        List<AuthorDto> actualAuthorDtoList = authorService.findAuthorByName(name);

        //THEN
        assertFalse(actualAuthorDtoList.isEmpty());
        assertSame(authorDtoList.size(), actualAuthorDtoList.size());
        assertEquals(authorDtoList, actualAuthorDtoList);
    }

    @Test
    public void insertAuthorShouldThrowObjectAlreadyExistExceptionWhenAuthorExist() {
        //GIVEN
        List<Integer> booksId = Arrays.asList(1, 2, 3);

        String expectedException = "Sorry, author already exists";
        Author author = new Author(1, "Іріна", "Дольнікова", "Володимирівна");
        when(authorsDao.existAuthorByName(author.getFirstName(), author.getSurname(), author.getPatronymic()))
                .thenReturn(true);

        //WHEN
        Exception actualException = assertThrows(ObjectAlreadyExistException.class,
                () -> authorService.insertAuthor(author, booksId));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
        verify(bookService, Mockito.never()).findBookById(anyInt());
        verify(authorsBooksDao, never()).authorAndBookAddById(anyInt(), anyInt());
    }

    @Test
    public void insertAuthorShouldReturnAuthorDtoWhenAuthorAbsent() {
        //GIVEN
        List<Integer> booksId = Arrays.asList(1, 2, 3);
        Author author = new Author(1, "Іріна", "Дольнікова", "Володимирівна");
        AuthorDto expectedAuthorDto = new AuthorDto(1, "Іріна", "Дольнікова", "Володимирівна");

        when(authorsDao.existAuthorByName(author.getFirstName(), author.getSurname(), author.getPatronymic())).thenReturn(false);
        when(authorsDao.insertAuthor(author)).thenReturn(author);
        when(authorConverter.toAuthorDto(author)).thenReturn(expectedAuthorDto);

        //WHEN
        AuthorDto actualAuthorDto = authorService.insertAuthor(author, booksId);

        //THEN
        assertNotNull(actualAuthorDto);
        assertEquals(expectedAuthorDto, actualAuthorDto);
        verify(authorsDao).insertAuthor(author);
        verify(authorsBooksDao, times(3)).authorAndBookAddById(eq(actualAuthorDto.getId()), anyInt());
        verify(bookService, times(3)).findBookById(anyInt());
    }

    @Test
    public void updateAuthorShouldReturnAuthorDto() {
        //GIVEN
        List<Integer> booksId = Arrays.asList(1, 2, 3);

        Author author = new Author(1, "Іріна", "Дольнікова", "Володимирівна");
        AuthorDto authorDto = new AuthorDto(1, "Іріна", "Дольнікова", "Володимирівна");
        when(authorsDao.updateAuthor(author)).thenReturn(author);
        when(authorConverter.toAuthorDto(author)).thenReturn(authorDto);

        //WHEN
        AuthorDto actualAuthorDto = authorService.updateAuthor(author, booksId);

        //THEN
        verify(authorsBooksDao).deleteAuthorFromAuthorsBook(author.getId());
        verify(authorsDao).updateAuthor(author);
        verify(bookService, times(3)).findBookById(anyInt());
        verify(authorsBooksDao, times(3)).authorAndBookAddById(anyInt(), anyInt());
        assertEquals(authorDto, actualAuthorDto);

    }

    @Test
    public void deleteAuthorShouldDeleteAuthorWhenHeDoesNotHaveAnyBooks() {
        //GIVEN
        int authorId = 1;
        int count = 0;

        Author author = new Author();
        author.setId(authorId);

        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(authorId);

        when(authorsDao.findAuthorById(authorId)).thenReturn(Optional.of(author));
        when(authorConverter.toAuthorDto(author)).thenReturn(authorDto);
        when(bookConverter.toBookDtoList(author.getBooks())).thenReturn(authorDto.getBooks());

        when(authorsDao.deleteAuthor(authorId)).thenReturn(count);

        //WHEN
        int actualCount = authorService.deleteAuthor(authorId);

        //THEN
        assertEquals(count, actualCount);
    }

    @Test
    public void deleteAuthorShouldThrowCannotDeleteExceptionWhenAuthorRelatedWithBooks() {
        //GIVEN
        int authorId = 1;
        Book book = new Book(1, "1984", 7);
        BookDto bookDto = new BookDto(1, "1984", 7);

        Author author = new Author();
        author.setId(authorId);
        author.setBooks(Collections.singletonList(book));

        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(authorId);
        authorDto.setBooks(Collections.singletonList(bookDto));

        when(authorsDao.findAuthorById(authorId)).thenReturn(Optional.of(author));
        when(authorConverter.toAuthorDto(author)).thenReturn(authorDto);
        when(bookConverter.toBookDtoList(author.getBooks())).thenReturn(authorDto.getBooks());

        //WHEN
        assertThrows(CannotDeleteException.class, () -> authorService.deleteAuthor(authorId));

        //THEN
        verify(authorsDao, never()).deleteAuthor(authorId);
    }
}
