package com.epam.service.impl;

import com.epam.dao.OrderDao;
import com.epam.exception.BookAbsentException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.exception.ReaderBlockedException;
import com.epam.service.BookService;
import com.epam.service.UserService;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.OrderSearchDto;
import com.epam.web.dto.UserDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.datagenerator.OrderDataGenerator.generateOrder;
import static com.epam.datagenerator.UserDataGenerator.generateReaderDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class OrderServiceImplTest {

    @Mock
    private OrderDao orderDao;
    @Mock
    private UserService userService;
    @Mock
    private BookService bookService;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void findOrderByIdThrowObjectNotFoundByIDExceptionWhenOrderNotExist() {
        //GIVEN
        String expectedException = "Order does not exist";
        int id = 3;
        when(orderDao.findOrderById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class, () -> orderService.findOrderById(id));

        //THEN
        assertEquals(expectedException, actualException.getMessage());
    }

    @Test
    public void findOrderByIdShouldReturnOrderDto() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, new UserDto(), new BookDto());
        when(orderDao.findOrderById(id)).thenReturn(Optional.of(expectedOrderDto));

        //WHEN
        OrderDto actualOrderDto = orderService.findOrderById(id);

        //THEN
        assertNotNull(actualOrderDto);
        assertEquals(expectedOrderDto, actualOrderDto);
    }

    @Test
    @DisplayName("Find all orders when all their count = 0")
    public void findAllOrdersShouldReturnEmptyListAndPageCountIs0AndCurrentPageIs0() {
        //GIVEN
        int currentPage = 0;
        List<OrderDto> orderDtos = new ArrayList<>();
        OrderSearchDto expectedOrderSearchDto = new OrderSearchDto(orderDtos, 0, 0);
        when(orderDao.findOrdersBySearchParameter(null)).thenReturn(orderDtos);

        //WHEN
        OrderSearchDto actualOrderSearchDto = orderService.findAllOrders(null, null, currentPage);

        //THEN
        assertEquals(expectedOrderSearchDto, actualOrderSearchDto);
    }

    @Test
    @DisplayName("Find all orders when all search parameters are null and current page bigger that pages count")
    public void findAllOrdersShouldReturnListOrderDto() {
        //GIVEN
        int id = 5;
        int id2 = 7;
        int currentPage = 1;

        UserDto userDto = new UserDto();
        userDto.setSurname("Shevchenko");

        List<OrderDto> orderDtos = new ArrayList<>();
        orderDtos.add(generateOrder(id, userDto, new BookDto(1, "1984", 40)));
        orderDtos.add(generateOrder(id2, userDto, new BookDto(2, "Harry Potter", 40)));

        OrderSearchDto expectedOrderSearchDto = new OrderSearchDto(orderDtos, 1, 0);
        when(orderDao.findOrdersBySearchParameter(null)).thenReturn(orderDtos);

        //WHEN
        OrderSearchDto actualOrderSearchDto = orderService.findAllOrders(null, null, currentPage);

        //THEN
        assertEquals(expectedOrderSearchDto, actualOrderSearchDto);
    }

    @Test
    @DisplayName("Find all ReadersCards by readerName for first page")
    public void findAllReadersBooksShouldReturnFilterListReadersBooksDtoByReaderName() {
        //GIVEN
        int currentPage = 0;
        UserDto reader1 = generateReaderDto(1, "Tarasova", "Anna", "tarasova@gmail.com", "12-15-78");
        UserDto reader2 = generateReaderDto(2, "Tarasenko", "Hanna", "tarasenko@gmail.com", "12-18-75");
        UserDto reader3 = generateReaderDto(3, "Kuzmenko", "Serhiy", "kuzmenko@gmail.com", "13-20-75");

        List<OrderDto> orderList = new ArrayList<>();
        orderList.add(generateOrder(5, reader1, new BookDto(1, "Palyanitsa", 3)));
        orderList.add(generateOrder(7, reader2, new BookDto(2, "Magic", 5)));
        orderList.add(generateOrder(8, reader3, new BookDto(3, "Sky", 12)));
        orderList.add(generateOrder(9, reader1, new BookDto(1, "1984", 3)));
        orderList.add(generateOrder(10, reader2, new BookDto(2, "Dumy", 5)));
        orderList.add(generateOrder(11, reader2, new BookDto(3, "Internat", 12)));
        orderList.add(generateOrder(5, reader1, new BookDto(1, "Dim", 3)));

        List<OrderDto> expectedOrderList = new ArrayList<>();
        expectedOrderList.add(orderList.get(0));
        expectedOrderList.add(orderList.get(1));
        expectedOrderList.add(orderList.get(3));
        expectedOrderList.add(orderList.get(4));
        expectedOrderList.add(orderList.get(5));

        OrderSearchDto expectedOrderSearchDto = new OrderSearchDto(expectedOrderList, 2, currentPage);
        when(orderDao.findOrdersBySearchParameter(null)).thenReturn(orderList);


        //WHEN
        OrderSearchDto actualOrderSearchDto = orderService.findAllOrders(null, "Tar", currentPage);

        //THEN
        assertEquals(expectedOrderSearchDto, actualOrderSearchDto);
    }

    @Test
    @DisplayName("Find all orders by reader id for sacond page")
    public void findAllOrdersByReaderIdShouldReturnFilteredOrders() {
        //GIVEN
        int currentPage = 1;
        UserDto reader1 = generateReaderDto(1, "Tarasova", "Anna", "tarasova@gmail.com", "12-15-78");

        List<OrderDto> orderList = new ArrayList<>();
        orderList.add(generateOrder(5, reader1, new BookDto(1, "Palyanitsa", 3)));
        orderList.add(generateOrder(7, reader1, new BookDto(2, "Magic", 5)));
        orderList.add(generateOrder(8, reader1, new BookDto(3, "Sky", 12)));
        orderList.add(generateOrder(9, reader1, new BookDto(1, "1984", 3)));
        orderList.add(generateOrder(10, reader1, new BookDto(2, "Dumy", 5)));
        orderList.add(generateOrder(11, reader1, new BookDto(3, "Internat", 12)));
        orderList.add(generateOrder(5, reader1, new BookDto(1, "Dim", 3)));

        List<OrderDto> expectedOrderList = new ArrayList<>();
        expectedOrderList.add(orderList.get(5));
        expectedOrderList.add(orderList.get(6));

        when(orderDao.findOrderByReaderId(reader1.getId(), null)).thenReturn(orderList);
        OrderSearchDto expectedOrderSearchDto = new OrderSearchDto(expectedOrderList, 2, currentPage);


        //WHEN
        OrderSearchDto actualOrderSearchDto = orderService.findOrdersByReaderID(reader1.getId(),
                null, null, currentPage);

        //THEN
        assertEquals(expectedOrderSearchDto, actualOrderSearchDto);
    }

    @Test
    public void insertOrderShouldReturnOrderDtoWhenReaderIsNotBlocked() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(false);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);
        when(bookService.findBookById(bookDto.getId())).thenReturn(bookDto);
        when(orderDao.insertOrder(expectedOrderDto)).thenReturn(expectedOrderDto);

        //WHEN
        OrderDto actualOrderDto = orderService.insertOrder(expectedOrderDto);

        //THEN
        assertNotNull(actualOrderDto);
        assertEquals(expectedOrderDto, actualOrderDto);
    }

    @Test
    public void insertOrderShouldThrowReaderBlockedExceptionWhenReaderIsBlocked() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(true);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);

        //WHEN
        assertThrows(ReaderBlockedException.class, () -> orderService.insertOrder(expectedOrderDto));

        //THEN
        verify(bookService, never()).findBookById(bookDto.getId());
        verify(orderDao, never()).insertOrder(expectedOrderDto);
    }

    @Test
    public void updateOrderShouldReturnOrderDtoWhenReaderIsNotBlocked() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(false);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto orderDtoFromDb = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);
        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(orderDao.findOrderById(id)).thenReturn(Optional.of(orderDtoFromDb));
        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);
        when(bookService.findBookById(bookDto.getId())).thenReturn(bookDto);
        when(orderDao.updateOrder(expectedOrderDto)).thenReturn(expectedOrderDto);

        //WHEN
        OrderDto actualOrderDto = orderService.updateOrder(expectedOrderDto);

        //THEN
        assertNotNull(actualOrderDto);
        assertEquals(expectedOrderDto, actualOrderDto);
    }

    @Test
    public void updateOrderShouldThrowObjectNotFoundByIDExceptionWhenOrderDoesNotExist() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(false);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(orderDao.findOrderById(id)).thenReturn(Optional.empty());

        //WHEN
        assertThrows(ObjectNotFoundByIDException.class, () -> orderService.updateOrder(expectedOrderDto));

        //THEN
        verify(userService, never()).findReaderById(readerDto.getId());
        verify(bookService, never()).findBookById(bookDto.getId());
        verify(orderDao, never()).updateOrder(expectedOrderDto);
    }

    @Test
    public void updateOrderShouldThrowReaderBlockedExceptionWhenReaderIsBlocked() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(true);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(orderDao.findOrderById(id)).thenReturn(Optional.of(expectedOrderDto));
        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);

        //WHEN
        assertThrows(ReaderBlockedException.class, () -> orderService.updateOrder(expectedOrderDto));

        //THEN
        verify(bookService, never()).findBookById(bookDto.getId());
        verify(orderDao, never()).updateOrder(expectedOrderDto);
    }

    @Test
    public void approveOrderShouldApproveItWhenReaderIsNotBlockedAndBookCountBiggerThan0() {
        //GIVEN
        int id = 5;
        int librarianId = 6;

        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(false);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto orderDtoFromDb = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);
        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(orderDao.findOrderById(id)).thenReturn(Optional.of(orderDtoFromDb));
        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);
        when(bookService.findBookById(bookDto.getId())).thenReturn(bookDto);

        //WHEN
        orderService.approveOrder(expectedOrderDto.getId(), librarianId);

        //THEN
        bookDto.setCount(bookDto.getCount() - 1);
        verify(bookService).updateBook(bookDto, bookDto.getId());
        verify(orderDao).approveOrder(expectedOrderDto.getId(), librarianId);
    }

    @Test
    public void approveOrderShouldThrowBookAbsentExceptionWhenBookCountLessThan0() {
        //GIVEN
        int id = 5;
        int librarianId = 6;

        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(false);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 0);
        OrderDto orderDtoFromDb = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);
        OrderDto expectedOrderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(orderDao.findOrderById(id)).thenReturn(Optional.of(orderDtoFromDb));
        when(userService.findReaderById(readerDto.getId())).thenReturn(readerDto);
        when(bookService.findBookById(bookDto.getId())).thenReturn(bookDto);

        //WHEN
        assertThrows(BookAbsentException.class, () -> orderService.approveOrder(expectedOrderDto.getId(), librarianId));

        //THEN
        bookDto.setCount(bookDto.getCount() - 1);
        verify(bookService, never()).updateBook(bookDto, bookDto.getId());
        verify(orderDao, never()).approveOrder(expectedOrderDto.getId(), librarianId);
    }

    @Test
    public void deleteOrderShouldDeleteOrderDtoWhenItExists() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(false);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto orderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);
        when(orderDao.findOrderById(orderDto.getId())).thenReturn(Optional.of(orderDto));
        when(orderDao.deleteOrder(id)).thenReturn(1);

        //WHEN
        int actualResult = orderService.deleteOrder(orderDto.getId());

        //THEN
        assertEquals(1, actualResult);
    }

    @Test
    public void deleteOrderShouldThrowObjectNotFoundByIDExceptionWhenDoesNotExist() {
        //GIVEN
        int id = 5;
        LocalDate localDate = LocalDate.of(2022, 1, 12);
        LocalDate localDate1 = LocalDate.of(2022, 1, 18);
        Date expectedReturnDate = Date.valueOf(localDate);
        Date actualReturnDate = Date.valueOf(localDate1);

        UserDto readerDto = new UserDto();
        readerDto.setId(2);
        readerDto.setBlocked(false);

        BookDto bookDto = new BookDto(1, "Palyanitsa", 3);
        OrderDto orderDto = new OrderDto(id, expectedReturnDate, actualReturnDate, readerDto, bookDto);

        when(orderDao.findOrderById(orderDto.getId())).thenReturn(Optional.empty());

        //WHEN
        assertThrows(ObjectNotFoundByIDException.class, () -> orderService.deleteOrder(orderDto.getId()));

        //THEN
        verify(orderDao, never()).deleteOrder(anyInt());
    }

}