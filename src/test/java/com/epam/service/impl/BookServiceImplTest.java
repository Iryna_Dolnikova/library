package com.epam.service.impl;

import com.epam.dao.AuthorsBooksDao;
import com.epam.dao.BooksDao;
import com.epam.dao.ReadersCardDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.Author;
import com.epam.model.Book;
import com.epam.model.PublishingHouse;
import com.epam.model.ReaderCard;
import com.epam.model.enums.SortingEnum;
import com.epam.service.AuthorService;
import com.epam.service.PublishingHouseService;
import com.epam.web.converter.AuthorConverter;
import com.epam.web.converter.BookConverter;
import com.epam.web.dto.AuthorDto;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.BookSearchDto;
import com.epam.web.dto.SortDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class BookServiceImplTest {
    @Mock
    private BooksDao booksDao;
    @Mock
    private BookConverter bookConverter;
    @Mock
    private AuthorConverter authorConverter;
    @Mock
    private PublishingHouseService publishingHouseService;
    @Mock
    private AuthorService authorService;
    @Mock
    private AuthorsBooksDao authorsBooksDao;
    @Mock
    private ReadersCardDao readersCardDao;
    @Mock
    private SortDto sortDto;

    @InjectMocks
    private BookServiceImpl bookService;

    public BookServiceImplTest() {
    }

    @Test
    public void findBookByIdThrowObjectNotFoundByIDExceptionWhenBookNotExist() {
        //GIVEN
        int id = 1;
        String expectedExceptionMassage = "Book does not exist";
        when(booksDao.findBookById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class, () -> bookService.findBookById(id));
        //THEN
        assertEquals(expectedExceptionMassage, actualException.getMessage());

    }

    @Test
    public void findBookByIdShouldReturnBookDto() {
        //GIVEN
        int id = 3;
        Book book = new Book(id);
        BookDto bookDto = new BookDto(id);
        when(booksDao.findBookById(id)).thenReturn(Optional.of(book));
        when(bookConverter.toBookDto(book)).thenReturn(bookDto);

        //WHEN
        BookDto actualBookDto = bookService.findBookById(id);

        //THEN
        assertNotNull(actualBookDto);
        assertEquals(bookDto, actualBookDto);


    }

    @Test
    public void findAllShouldReturnListBookDto() {
        //GIVEN
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book(1));
        bookList.add(new Book(2));
        List<BookDto> bookDtoList = new ArrayList<>();
        bookDtoList.add(new BookDto(1));
        bookDtoList.add(new BookDto(2));

        when(booksDao.findAllBooks()).thenReturn(bookList);
        when(bookConverter.toBookDtoList(bookList)).thenReturn(bookDtoList);

        //WHEN
        List<BookDto> actualBookDto = bookService.findAll();
        //THEN
        assertFalse(actualBookDto.isEmpty());
        assertSame(bookDtoList.size(), actualBookDto.size());
        assertEquals(bookDtoList, actualBookDto);

    }

    @Test
    public void updateBookShouldReturnBookDto() {
        //GIVEN
        BookDto bookDto = new BookDto(12);
        PublishingHouse publishingHouse = new PublishingHouse(4);
        bookDto.setPublishingHouse(publishingHouse);
        when(booksDao.updateBook(bookDto, bookDto.getId())).thenReturn(bookDto);
        when(booksDao.findBookById(bookDto.getId())).thenReturn(Optional.of(new Book()));
        //WHEN
        BookDto actualBookDto = bookService.updateBook(bookDto, bookDto.getId());

        //THEN
        verify(publishingHouseService).findPubHouseById(publishingHouse.getId());
        assertEquals(bookDto, actualBookDto);
    }

    @Test
    public void updateBookWithAuthorsShouldReturnBookDto() {
        //GIVEN
        List<Integer> authorsId = Arrays.asList(1, 2, 4);
        PublishingHouse publishingHouse = new PublishingHouse(1);
        BookDto bookDto = new BookDto(10);
        bookDto.setPublishingHouse(publishingHouse);
        when(booksDao.findBookById(bookDto.getId())).thenReturn(Optional.of(new Book()));
        when(booksDao.updateBook(bookDto, bookDto.getId())).thenReturn(bookDto);

        //WHEN
        BookDto actualBookDto = bookService.updateBook(bookDto, bookDto.getId(), authorsId);

        //THEN
        verify(publishingHouseService).findPubHouseById(publishingHouse.getId());
        verify(authorsBooksDao, atLeastOnce()).deleteBookFromAuthorsBook(bookDto.getId());
        verify(authorService, times(3)).findAuthorById(anyInt());
        verify(authorsBooksDao, times(3)).authorAndBookAddById(anyInt(), eq(bookDto.getId()));
        assertEquals(bookDto, actualBookDto);
    }

    @Test
    @DisplayName("Should return empty list with pages count is zero")
    public void findBookBySearchParametersShouldReturnBookSearchDtoWhenBooksListIsEmptyAndPagesCountIsZero() {
        //GIVEN
        int currentPage = 0;
        int pagesCount = 0;

        List<Book> booksList = new ArrayList<>();
        List<BookDto> bookDtoList = new ArrayList<>();
        String bookName = "Book";

        BookSearchDto expectedBookSearchDto = new BookSearchDto(bookDtoList, pagesCount);
        when(booksDao.findBookBySearchParameters(bookName, null, null)).thenReturn(booksList);

        //WHEN
        BookSearchDto actualBookSearchDto = bookService.findBookBySearchParameters("Book", null,
                null, sortDto, currentPage);

        //THEN
        assertEquals(expectedBookSearchDto, actualBookSearchDto);
        verify(bookConverter, never()).toBookDtoList(anyList());
    }

    @Test
    @DisplayName("Should filter by BookName, sort by count ASC when currentPage is equal to max pages count")
    public void findBookBySearchParametersShouldReturnBookSearchDtoSortedByCountAscWhenCurrentPageEqualToPagesCountAndPagesCountIsOne() {
        //GIVEN
        int currentPage = 1;
        int pagesCount = 1;

        List<Book> booksList = new ArrayList<>();
        booksList.add(new Book(1, "Book1", 10));
        booksList.add(new Book(3, "Book3", 3));
        booksList.add(new Book(2, "Book2", 5));

        List<BookDto> bookDtoList = new ArrayList<>();
        bookDtoList.add(new BookDto(3, "Book3", 3));
        bookDtoList.add(new BookDto(2, "Book2", 5));
        bookDtoList.add(new BookDto(1, "Book1", 10));

        String bookName = "Book";
        SortDto sortDto = new SortDto("count", SortingEnum.ASC);

        BookSearchDto expectedBookSearchDto = new BookSearchDto(bookDtoList, pagesCount);

        when(booksDao.findBookBySearchParameters(bookName, null, null)).thenReturn(booksList);
        when(bookConverter.toBookDtoList(booksList)).thenReturn(bookDtoList);

        //WHEN
        BookSearchDto actualBookSearchDto = bookService.findBookBySearchParameters("Book", null,
                null, sortDto, currentPage);

        //THEN
        assertEquals(expectedBookSearchDto, actualBookSearchDto);
    }

    @Test
    @DisplayName("Should filter by BookName & phName, sort by issuerYear DESC when currentPage = 0")
    public void findBookBySearchParametersShouldReturnBookSearchDtoSortedDescByIssuerYearWhenCurrentPageIsZeroAndPagesCountIsOne() {
        //GIVEN
        int currentPage = 0;
        int pagesCount = 1;

        List<Book> booksList = new ArrayList<>();
        PublishingHouse pHouse = new PublishingHouse(5, "Фоліо");
        booksList.add(new Book(1, "Book1", null, pHouse, 1987, 3));
        booksList.add(new Book(5, "Book5", null, pHouse, 2001, 5));
        booksList.add(new Book(3, "Book3", null, pHouse, 1995, 7));

        List<BookDto> bookDtoList = new ArrayList<>();
        bookDtoList.add(new BookDto(5, "Book5", null, pHouse, 2001, 5));
        bookDtoList.add(new BookDto(3, "Book3", null, pHouse, 1995, 7));
        bookDtoList.add(new BookDto(1, "Book1", null, pHouse, 1987, 3));

        String bookName = "Book";
        SortDto sortDto = new SortDto("issuerYear", SortingEnum.DESC);
        BookSearchDto expectedBookSearchDto = new BookSearchDto(bookDtoList, pagesCount);

        when(booksDao.findBookBySearchParameters(bookName, pHouse.getName(), null)).thenReturn(booksList);
        when(bookConverter.toBookDtoList(booksList)).thenReturn(bookDtoList);

        //WHEN
        BookSearchDto actualBookSearchDto = bookService.findBookBySearchParameters(bookName, pHouse.getName(),
                null, sortDto, currentPage);

        //THEN
        assertEquals(expectedBookSearchDto, actualBookSearchDto);
    }

    @Test
    @DisplayName("Should filter by BookName when currentPage = 0 and sortDto = null")
    public void findBookBySearchParametersShouldReturnBookSearchDtoWhenSortDtoListEqualsNullAndCurrentPageIsZeroAndPagesCountIsOne() {
        //GIVEN
        int currentPage = 0;
        int pagesCount = 1;

        List<Book> booksList = new ArrayList<>();
        booksList.add(new Book(1, "Book1", 10));
        booksList.add(new Book(3, "Book3", 3));
        booksList.add(new Book(2, "Book2", 5));

        List<BookDto> bookDtoList = new ArrayList<>();
        bookDtoList.add(new BookDto(1, "Book1", 10));
        bookDtoList.add(new BookDto(3, "Book3", 3));
        bookDtoList.add(new BookDto(2, "Book2", 5));

        String bookName = "Book";
        BookSearchDto expectedBookSearchDto = new BookSearchDto(bookDtoList, pagesCount);
        when(booksDao.findBookBySearchParameters(bookName, null, null)).thenReturn(booksList);
        when(bookConverter.toBookDtoList(booksList)).thenReturn(bookDtoList);

        //WHEN
        BookSearchDto actualBookSearchDto = bookService.findBookBySearchParameters(bookName, null,
                null, null, currentPage);

        //THEN
        assertEquals(expectedBookSearchDto, actualBookSearchDto);
    }

    @Test
    @DisplayName("Should filter by BookName and return first page when books count is 7")
    public void findBookBySearchParametersShouldReturnBookSearchDtoWhenCurrentPageIsZeroAndBooksCountIsBiggerThan5() {
        //GIVEN
        int currentPage = 0;
        int pagesCount = 2;

        List<Book> booksList = new ArrayList<>();
        booksList.add(new Book(1, "Book1", 10));
        booksList.add(new Book(2, "Book2", 6));
        booksList.add(new Book(3, "Book3", 15));
        booksList.add(new Book(4, "Book4", 8));
        booksList.add(new Book(5, "Book5", 19));
        booksList.add(new Book(6, "Book6", 3));
        booksList.add(new Book(7, "Book7", 7));

        List<BookDto> bookDtoList = new ArrayList<>();
        bookDtoList.add(new BookDto(1, "Book1", 10));
        bookDtoList.add(new BookDto(2, "Book2", 6));
        bookDtoList.add(new BookDto(3, "Book3", 15));
        bookDtoList.add(new BookDto(4, "Book4", 8));
        bookDtoList.add(new BookDto(5, "Book5", 19));
        bookDtoList.add(new BookDto(6, "Book6", 3));
        bookDtoList.add(new BookDto(7, "Book7", 7));

        List<BookDto> expectedBookDtoList = new ArrayList<>();
        expectedBookDtoList.add(new BookDto(6, "Book6", 3));
        expectedBookDtoList.add(new BookDto(2, "Book2", 6));
        expectedBookDtoList.add(new BookDto(7, "Book7", 7));
        expectedBookDtoList.add(new BookDto(4, "Book4", 8));
        expectedBookDtoList.add(new BookDto(1, "Book1", 10));

        String bookName = "Book";
        SortDto sortDto = new SortDto("count", SortingEnum.ASC);

        BookSearchDto expectedBookSearchDto = new BookSearchDto(expectedBookDtoList, pagesCount);
        when(booksDao.findBookBySearchParameters(bookName, null, null)).thenReturn(booksList);
        when(bookConverter.toBookDtoList(booksList)).thenReturn(bookDtoList);

        //WHEN
        BookSearchDto actualBookSearchDto = bookService.findBookBySearchParameters(bookName, null,
                null, sortDto, currentPage);

        //THEN
        assertEquals(expectedBookSearchDto, actualBookSearchDto);
    }

    @Test
    public void insertBookShouldReturnBookDto() {
        //GIVEN
        List<Integer> authorsId = Arrays.asList(1, 2, 4);
        AuthorDto authorDto = new AuthorDto();
        BookDto bookDto = new BookDto(9);
        PublishingHouse publishingHouse = new PublishingHouse(1, "Веселка");
        bookDto.setPublishingHouse(publishingHouse);

        when(publishingHouseService.findPubHouseById(publishingHouse.getId())).thenReturn(publishingHouse);
        when(authorService.findAuthorById(anyInt())).thenReturn(authorDto);
        when(booksDao.insertBook(bookDto)).thenReturn(bookDto);


        //WHEN
        BookDto actualBookDto = bookService.insertBook(bookDto, authorsId);

        //THEN
        assertEquals(bookDto, actualBookDto);
        verify(authorsBooksDao, times(3)).authorAndBookAddById(anyInt(), eq(bookDto.getId()));
    }

    @Test
    public void deleteBookByIdShouldDeleteBookWhenItDoesNotRelatedWithAnyCards() {
        // GIVEN
        int bookId = 1;
        int expectedCount = 1;

        Book book = new Book(1, "1984", 7);
        BookDto bookDto = new BookDto(1, "1984", 7);

        when(booksDao.findBookById(bookId)).thenReturn(Optional.of(book));
        when(bookConverter.toBookDto(book)).thenReturn(bookDto);
        when(authorConverter.toAuthorDtoList(book.getAuthors())).thenReturn(bookDto.getAuthors());
        when(readersCardDao.findReaderBookByBookId(bookId)).thenReturn(new ArrayList<>());
        when(booksDao.deleteBook(bookId)).thenReturn(1);

        // WHEN
        int actualCount = bookService.deleteBook(bookId);

        // THEN
        assertEquals(expectedCount, actualCount);
        verify(booksDao).deleteBook(bookId);
    }

    @Test
    public void deleteBookByIdShouldThrowCannotDeleteExceptionWhenItRelatedWithSomeCards() {
        // GIVEN
        int bookId = 1;

        Author author = new Author(1, "Shevchenko", "Taras", "Hryhorovych");
        AuthorDto authorDto = new AuthorDto(1, "Shevchenko", "Taras", "Hryhorovych");

        Book book = new Book(1, "1984", 7);
        book.setAuthors(Collections.singletonList(author));

        BookDto bookDto = new BookDto(1, "1984", 7);
        bookDto.setAuthors(Collections.singletonList(authorDto));

        when(booksDao.findBookById(bookId)).thenReturn(Optional.of(book));
        when(bookConverter.toBookDto(book)).thenReturn(bookDto);
        when(authorConverter.toAuthorDtoList(book.getAuthors())).thenReturn(bookDto.getAuthors());
        when(readersCardDao.findReaderBookByBookId(bookId)).thenReturn(Arrays.asList(new ReaderCard()));

        // WHEN
        assertThrows(CannotDeleteException.class, () -> bookService.deleteBook(bookId));

        // THEN
        verify(booksDao, never()).deleteBook(bookId);
    }

}
