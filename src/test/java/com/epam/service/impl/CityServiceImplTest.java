package com.epam.service.impl;

import com.epam.dao.CitiesDao;
import com.epam.dao.PublishingHousesDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.City;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith({MockitoExtension.class})
public class CityServiceImplTest {

    @Mock
    private CitiesDao citiesDao;
    @Mock
    private PublishingHousesDao publishingHousesDao;
    @InjectMocks
    private CityServiceImpl cityService;

    @Test
    @DisplayName("Should throw Throw ObjectNotFoundByIDException when city id not found")
    public void findCityByIdShouldThrowObjectNotFoundByIDExceptionWhenCityNotFound() {
        // GIVEN
        String expectedExceptionMessage = "City does not exist";
        int id = 101;
        Mockito.when(citiesDao.findCityById(id)).thenReturn(Optional.empty());

        //WHEN
        Exception actualException = assertThrows(ObjectNotFoundByIDException.class,
                () -> cityService.findCityById(id));

        //THEN
        assertEquals(expectedExceptionMessage, actualException.getMessage());
    }

    @Test
    public void findCityByIdShouldReturnCity() {
        //GIVEN
        City expectedCity = new City(1, "Київ");
        Mockito.when(citiesDao.findCityById(expectedCity.getId())).thenReturn(Optional.of(expectedCity));

        //WHEN
        City actualCity = cityService.findCityById(expectedCity.getId());

        //THEN
        assertEquals(expectedCity, actualCity);
    }

    @Test
    public void findAllCitiesShouldReturnListOfCity() {
        //GIVEN
        List<City> expectedCities = new ArrayList<>();
        City city = new City(1, "Дніпро");
        City city1 = new City(2, "Львів");
        expectedCities.add(city);
        expectedCities.add(city1);
        Mockito.when(citiesDao.findAllCities()).thenReturn(expectedCities);

        //WHEN
        List<City> actualCities = cityService.findAllCities();

        //THEN
        assertFalse(actualCities.isEmpty());
        assertSame(expectedCities.size(), actualCities.size());
        assertEquals(expectedCities, actualCities);
    }

    @Test
    public void updateCityShouldReturnCity() {
        //GIVEN
        City expectedCity = new City(3, "Київ");
        Mockito.when(citiesDao.updateCity(expectedCity.getName(), expectedCity.getId())).thenReturn(expectedCity);

        //WHEN
        City actualCity = cityService.updateCity(expectedCity.getName(), expectedCity.getId());

        //THEN
        assertNotNull(actualCity);
        assertEquals(expectedCity, actualCity);
    }

    @Test
    public void deleteCityShouldReturnCountOfDeletedCitiesWhenCityDoesNotRelatedWithAnyPubHouses() {
        //GIVEN
        int id = 2;
        int expectedCount = 1;
        Mockito.when(publishingHousesDao.existPubHouseByCityId(id)).thenReturn(false);
        Mockito.when(citiesDao.deleteCity(id)).thenReturn(expectedCount);

        //WHEN
        int actualCount = cityService.deleteCity(id);

        //THEN
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void deleteCityShouldThrowCannotDeleteExceptionWhenCityRelatedWithAnyPubHouses() {
        //GIVEN
        int id = 2;
        Mockito.when(publishingHousesDao.existPubHouseByCityId(id)).thenReturn(true);

        //WHEN
        assertThrows(CannotDeleteException.class, () -> cityService.deleteCity(id));

        //THEN
        Mockito.verify(citiesDao, Mockito.never()).deleteCity(id);
    }

    @Test
    public void insertCityShouldInsertCityWhenCityAbsent() {
        //GIVEN
        City city = new City();
        String name = "Житомир";
        city.setName(name);
        Mockito.when(citiesDao.existCityByName(name)).thenReturn(false);
        Mockito.when(citiesDao.insertCity(name)).thenReturn(city);

        //WHEN
        City actualCity = cityService.insertCity(name);

        //THEN
        assertEquals(city, actualCity);
        Mockito.verify(citiesDao).insertCity(name);
        Mockito.verify(citiesDao, Mockito.times(1)).insertCity(name);
    }

    @Test
    public void insertCityShouldThrowObjectAlreadyExistExceptionWhenCityExist() {
        //GIVEN
        String expectedExceptionMessage = "Sorry, city already exists";
        String name = "Житомир";
        Mockito.when(citiesDao.existCityByName(name)).thenReturn(true);

        //WHEN
        Exception actualException = assertThrows(ObjectAlreadyExistException.class,
                () -> cityService.insertCity(name));

        //THEN
        assertEquals(expectedExceptionMessage, actualException.getMessage());
        Mockito.verify(citiesDao, Mockito.never()).insertCity(name);
    }

}
