package com.epam.util;

import liquibase.pro.packaged.V;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringToNumberConverterTest {


    public static Stream<Arguments> nameParameters() {
        return Stream.of(
                Arguments.of("15", 15),
                Arguments.of("124", 124),
                Arguments.of("New York", -1),
                Arguments.of("-16", -16),
                Arguments.of("Odesa", -1),
                Arguments.of("Borispol", -1)
        );
    }

    @ParameterizedTest
    @MethodSource("nameParameters")
    public void convertStringToIntShouldReturnIntOr(String values, int expectedResult) {
        //WHEN
        int actualResult = StringToNumberConverter.convertStringToInt(values);
        //THEN
        assertEquals(expectedResult, actualResult);

    }
}
