package com.epam.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.sql.Date;

import static com.epam.util.StringToDateConverter.convertStringToDate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringToDateConverterTest {

    @Test
    public void convertStringToDateShouldReturnSqlDateWhenStringIsValid() {
        //GIVEN
        String date = "2020-04-21";
        Date expectedDate = Date.valueOf(date);

        //WHEN
        Date actualDate = convertStringToDate(date);

        //THEN
        assertNotNull(actualDate);
        assertEquals(expectedDate, actualDate);
    }

    @ParameterizedTest
    @EmptySource
    @NullSource
    public void convertStringToDateShouldReturnNullWhenStringIsNullOrEmpty(String date) {
        //WHEN
        Date actualDate = convertStringToDate(date);

        //THEN
        assertNull(actualDate);
    }

    @ParameterizedTest
    @ValueSource(strings = {"aaa", "20200101", "-2020"})
    public void convertStringToDateShouldThrowRuntimeExceptionWhenStringIsIvalid(String date) {
        //WHEN
        assertThrows(RuntimeException.class, () -> convertStringToDate(date));
    }

}
