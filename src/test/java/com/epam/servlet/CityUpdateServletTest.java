package com.epam.servlet;

import com.epam.model.City;
import com.epam.service.CityService;
import com.epam.validator.Validator;
import com.epam.web.servlet.city.CityUpdateServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CityUpdateServletTest {

    @Mock
    private CityService cityService;

    @Mock
    private RequestDispatcher requestDispatcher;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private Validator<City> validator;

    private final CityUpdateServlet cityUpdateServlet = new CityUpdateServlet();

    @BeforeEach
    public void setUp() {
        cityUpdateServlet.setCityService(cityService);
        cityUpdateServlet.setValidator(validator);
    }

    @Test
    public void doPostShouldUpdateCityByIdWhenListInvalidFieldsIsEmpty() throws ServletException, IOException {
        //GIVEN
        City city = new City(1, "Dnipro");
        List<String> invalidFields = new ArrayList<>();
        when(httpServletRequest.getParameter("id")).thenReturn(String.valueOf(1));
        when(httpServletRequest.getParameter("name")).thenReturn("Dnipro");
        when(validator.validate(city, httpServletRequest)).thenReturn(invalidFields);
        when(cityService.updateCity("Dnipro", city.getId())).thenReturn(city);
        when(httpServletRequest.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        //WHEN
        cityUpdateServlet.doPost(httpServletRequest, httpServletResponse);

        //THEN
        verify(httpServletRequest).setAttribute("city", city);
        verify(requestDispatcher).forward(httpServletRequest, httpServletResponse);
    }

    @Test
    public void doPostShouldNotUpdateCityByIdWhenListInvalidFieldsIsNotEmpty() throws ServletException, IOException {
        //GIVEN
        City city = new City(2, "Dnipro7");
        List<String> invalidFields = Arrays.asList("name");
        when(httpServletRequest.getParameter("id")).thenReturn(String.valueOf(2));
        when(httpServletRequest.getParameter("name")).thenReturn("Dnipro7");
        when(validator.validate(city, httpServletRequest)).thenReturn(invalidFields);
        when(httpServletRequest.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        //WHEN
        cityUpdateServlet.doPost(httpServletRequest, httpServletResponse);

        //THEN
        verify(httpServletRequest).setAttribute("city", city);
        verify(httpServletRequest).setAttribute("error", "Verify your input. This field is invalid: name");
        verify(requestDispatcher).forward(httpServletRequest, httpServletResponse);
        verify(cityService, Mockito.never()).updateCity(city.getName(), city.getId());
    }

    @Test
    public void doPostShouldNotUpdateCityByIdWhenIdLessThenZero() throws ServletException, IOException {
        //GIVEN
        List<String> invalidFields = new ArrayList<>();
        when(httpServletRequest.getParameter("id")).thenReturn(String.valueOf(-2));
        when(httpServletRequest.getParameter("name")).thenReturn("Dnipro");
        when(validator.validate(any(), eq(httpServletRequest))).thenReturn(invalidFields);
        when(httpServletRequest.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        //WHEN
        cityUpdateServlet.doPost(httpServletRequest, httpServletResponse);

        //THEN
        verify(cityService, Mockito.never()).updateCity(any(), anyInt());
        verify(httpServletRequest, never()).setAttribute(eq("city"), any());
        verify(requestDispatcher).forward(httpServletRequest, httpServletResponse);
    }
}

