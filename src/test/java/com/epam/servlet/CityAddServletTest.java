package com.epam.servlet;

import com.epam.model.City;
import com.epam.service.CityService;
import com.epam.validator.Validator;
import com.epam.web.servlet.city.CityAddServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CityAddServletTest {

    @Mock
    private CityService cityService;

    @Mock
    private RequestDispatcher requestDispatcher;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private Validator<City> validator;

    private final CityAddServlet cityAddServlet = new CityAddServlet();

    @BeforeEach
    public void setUp() {
        cityAddServlet.setCityService(cityService);
        cityAddServlet.setValidator(validator);
    }

    @Test
    public void doPostShouldCreateCityByName() throws ServletException, IOException {
        //GIVEN
        City city = new City(1, "Dnipro");
        List<String> invalidFields = new ArrayList<>();
        when(httpServletRequest.getParameter("name")).thenReturn("Dnipro");
        when(validator.validate(new City("Dnipro"), httpServletRequest)).thenReturn(invalidFields);
        when(cityService.insertCity("Dnipro")).thenReturn(city);

        //WHEN
        cityAddServlet.doPost(httpServletRequest, httpServletResponse);

        //THEN
        verify(httpServletResponse).sendRedirect("/Library/cities?id=1");
    }

    @Test
    public void doPostShouldNotCreateCityByNameWhenNameIsInvalid() throws ServletException, IOException {
        //GIVEN
        City city = new City(2, "Dnipro7");
        List<String> invalidFields = Arrays.asList("name");
        when(httpServletRequest.getParameter("name")).thenReturn("Dnipro7");
        when(validator.validate(new City("Dnipro7"), httpServletRequest)).thenReturn(invalidFields);
        when(httpServletRequest.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        //WHEN
        cityAddServlet.doPost(httpServletRequest, httpServletResponse);

        //THEN
        verify(httpServletRequest).setAttribute("error", "Verify your input. This field is invalid: name");
        verify(requestDispatcher).forward(httpServletRequest, httpServletResponse);
        verify(cityService, Mockito.never()).insertCity(city.getName());
    }
}

