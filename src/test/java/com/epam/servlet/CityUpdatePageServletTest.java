package com.epam.servlet;

import com.epam.model.City;
import com.epam.service.CityService;
import com.epam.web.servlet.city.CityUpdatePageServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CityUpdatePageServletTest {
    @Mock
    private CityService cityService;

    @Mock
    private RequestDispatcher requestDispatcher;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    private final CityUpdatePageServlet cityUpdatePageServlet = new CityUpdatePageServlet();

    @BeforeEach
    public void setUp() {
        cityUpdatePageServlet.setCityService(cityService);
    }

    @Test
    public void doGetShouldReturnPageForeUpdateByCityId() throws ServletException, IOException {
        //GIVEN
        City city = new City(3, "Odesa");
        when(httpServletRequest.getParameter("id")).thenReturn(String.valueOf(3));
        when(cityService.findCityById(3)).thenReturn(city);
        when(httpServletRequest.getRequestDispatcher("templates/cities/editCity.jsp")).thenReturn(requestDispatcher);
        //WHEN
        cityUpdatePageServlet.doGet(httpServletRequest, httpServletResponse);

        //THEN
        verify(httpServletRequest).setAttribute("city", city);
        verify(requestDispatcher).include(httpServletRequest, httpServletResponse);
    }

    @Test
    public void doGetShouldReturnPageWithAllCitiesWhenIdIsAbsent() throws ServletException, IOException {
        //GIVEN
        when(httpServletRequest.getParameter("id")).thenReturn(null);
        when(httpServletRequest.getRequestDispatcher("templates/cities/cities.jsp")).thenReturn(requestDispatcher);

        //WHEN
        cityUpdatePageServlet.doGet(httpServletRequest, httpServletResponse);

        //THEN
        verify(cityService, never()).findCityById(3);
        verify(httpServletRequest, never()).setAttribute(eq("city"), any());
        verify(requestDispatcher).include(httpServletRequest, httpServletResponse);
    }

}

