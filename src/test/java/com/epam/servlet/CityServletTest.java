package com.epam.servlet;

import com.epam.model.City;
import com.epam.service.CityService;
import com.epam.web.servlet.city.CityServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class CityServletTest {

    @Mock
    private CityService cityService;

    @Mock
    private RequestDispatcher requestDispatcher;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    private final CityServlet cityServlet = new CityServlet();

    @BeforeEach
    public void setUp() {
        cityServlet.setCityService(cityService);
    }

    @Test
    public void doGetShouldReturnCityById() throws ServletException, IOException {
        //GIVEN
        City city = new City(1, "Dnipro");
        when(httpServletRequest.getParameter("id")).thenReturn(String.valueOf(1));
        when(cityService.findCityById(1)).thenReturn(city);
        when(httpServletRequest.getRequestDispatcher("templates/cities/city.jsp")).thenReturn(requestDispatcher);

        //WHEN
        cityServlet.doGet(httpServletRequest, httpServletResponse);

        //THEN
        verify(cityService).findCityById(1);
        verify(httpServletRequest).setAttribute("city", city);
        verify(requestDispatcher).forward(httpServletRequest, httpServletResponse);
    }

    @Test
    public void doGetShouldReturnAllCitiesWhenIdIsAbsentOnTheRequest() throws ServletException, IOException {
        //GIVEN
        List<City> cities = Arrays.asList(new City(1, "Dnipro"), new City(2, "Kyiv"));
        when(httpServletRequest.getParameter("id")).thenReturn("");
        when(cityService.findAllCities()).thenReturn(cities);
        when(httpServletRequest.getRequestDispatcher("templates/cities/cities.jsp")).thenReturn(requestDispatcher);

        //WHEN
        cityServlet.doGet(httpServletRequest, httpServletResponse);

        //THEN
        verify(cityService).findAllCities();
        verify(httpServletRequest).setAttribute("cities", cities);
        verify(requestDispatcher).forward(httpServletRequest, httpServletResponse);
    }

}
