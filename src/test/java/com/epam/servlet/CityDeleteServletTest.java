package com.epam.servlet;

import com.epam.service.CityService;
import com.epam.web.servlet.city.CityDeleteServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class CityDeleteServletTest {

    @Mock
    private CityService cityService;

    @Mock
    private RequestDispatcher requestDispatcher;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    private final CityDeleteServlet cityDeleteServlet = new CityDeleteServlet();

    @BeforeEach
    public void setUp() {
        cityDeleteServlet.setCityService(cityService);
    }

    @Test
    public void doGetShouldDeleteCityAndRedirectIntoAllCities() throws ServletException, IOException {
        //GIVEN
        when(httpServletRequest.getParameter("id")).thenReturn(String.valueOf(1));

        //WHEN
        cityDeleteServlet.doGet(httpServletRequest, httpServletResponse);

        //THEN
        verify(cityService).deleteCity(1);
        verify(httpServletResponse).sendRedirect("/Library/cities");
    }

    @Test
    public void doGetShouldNotDeleteCityWhenIDISInvalidAndRedirectIntoAllCities() throws ServletException, IOException {
        //GIVEN
        when(httpServletRequest.getParameter("id")).thenReturn(String.valueOf(-1));

        //WHEN
        cityDeleteServlet.doGet(httpServletRequest, httpServletResponse);

        //THEN
        verify(cityService, never()).deleteCity(1);
        verify(httpServletResponse).sendRedirect("/Library/cities");
    }
}
