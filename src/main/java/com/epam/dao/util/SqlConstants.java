package com.epam.dao.util;

/**
 * This class contains constant fields with SQL columns
 */
public class SqlConstants {
    public static final String SQL_CITY_COLUMNS = "cities.id AS cityId, cities.name AS cityName";
    public static final String SQL_PUBLISHING_HOUSE_COLUMNS = "publishing_houses.id AS phId, publishing_houses.name AS phName";
    public static final String SQL_AUTHOR_COLUMNS = "authors.id AS authorId, authors.first_name AS authorName, "
            + "authors.surname AS authorSurname, authors.patronymic AS authorPatronymic";
    public static final String SQL_BOOKS_COLUMNS = "books.id AS bookId, books.name AS bookName, "
            + "books.count AS bookCount, books.issue_year AS bookPubDate";
    public static final String SQL_AUTHORS_BOOKS_COLUMNS = "authors_books.book_id AS abBookId, "
            + "authors_books.author_id AS abAuthorId";

    public static final String SQL_USERS_COLUMNS = "users.id AS userId, users.surname AS userSurname, "
            + "users.first_name AS userFirstName, users.patronymic AS userPatronymic, users.email AS userEmail, "
            + "users.phone AS userPhone, users.password AS userPassword, users.role AS userRole, "
            + "users.is_blocked AS readerBlock, users.is_active AS userIsActive";


}
