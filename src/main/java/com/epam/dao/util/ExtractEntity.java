package com.epam.dao.util;

import com.epam.model.Author;
import com.epam.model.Book;
import com.epam.model.City;
import com.epam.model.PublishingHouse;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class contains methods that convert result sets into Java objects
 */
public class ExtractEntity {

    public static int extractId(PreparedStatement preparedStatement) throws SQLException {
        int id = 0;
        try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                id = generatedKeys.getInt(1);
            }
        }
        return id;
    }

    public static City extractCity(ResultSet resultSet) throws SQLException {
        City city = new City();
        city.setId(resultSet.getInt("cityId"));
        city.setName(resultSet.getString("cityName"));
        return city;
    }

    public static PublishingHouse extractPublishingHouse(ResultSet resultSet) throws SQLException {
        PublishingHouse publishingHouse = new PublishingHouse();
        publishingHouse.setId(resultSet.getInt("phId"));
        publishingHouse.setName(resultSet.getString("phName"));
        publishingHouse.setCity(extractCity(resultSet));
        return publishingHouse;
    }

    public static Author extractAuthor(ResultSet resultSet) throws SQLException {
        Author author = new Author();
        author.setId(resultSet.getInt("authorId"));
        author.setFirstName(resultSet.getString("authorName"));
        author.setSurname(resultSet.getString("authorSurname"));
        author.setPatronymic(resultSet.getString("authorPatronymic"));
        return author;
    }

    public static Book extractBook(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        book.setId(resultSet.getInt("bookId"));
        book.setName(resultSet.getString("bookName"));
        book.setCount(resultSet.getInt("bookCount"));
        book.setIssueYear(resultSet.getInt("bookPubDate"));
        book.setPublishingHouse(extractPublishingHouse(resultSet));
        return book;
    }

}
