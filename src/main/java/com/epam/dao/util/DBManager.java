package com.epam.dao.util;

import com.epam.exception.DataBaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * This class manage DB connections
 */
public class DBManager {

    private final static Properties properties = new Properties();
    private static final Logger logger = LogManager.getLogger(DBManager.class);

    static {
        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("db.properties")) {
            properties.load(in);
        } catch (IOException e) {
            logger.error("Cannot read database properties {}", e.getMessage());
        }
    }

    public static Connection getConnection() {
        String urlConnect = properties.getProperty("connection.url");
        String userConnect = properties.getProperty("connection.user");
        String passwordConnect = properties.getProperty("connection.password");

        try {
            Class.forName(properties.getProperty("connection.driver"));
            return DriverManager.getConnection(urlConnect, userConnect, passwordConnect);
        } catch (ClassNotFoundException | SQLException e) {
            logger.error("Cannot connect to database {}", e.getMessage());
            throw new DataBaseException("Cannot connect to database");
        }
    }

    public static void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
           logger.error("Cannot close Result Set");
        }
    }

}
