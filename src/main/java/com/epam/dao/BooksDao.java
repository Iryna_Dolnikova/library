package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.exception.DataBaseException;
import com.epam.model.Book;
import com.epam.web.dto.BookDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.dao.BooksDao.BooksSql.Other.SQL_DELETE_BOOK;
import static com.epam.dao.BooksDao.BooksSql.Other.SQL_INSERT_BOOK;
import static com.epam.dao.BooksDao.BooksSql.Other.SQL_UPDATE_BOOK;
import static com.epam.dao.BooksDao.BooksSql.Select.SQL_SELECT_BOOKS;
import static com.epam.dao.BooksDao.BooksSql.Select.SQL_SELECT_BOOKS_BY_AUTHOR_ID;
import static com.epam.dao.BooksDao.BooksSql.Select.SQL_SELECT_BOOKS_BY_SEARCH_PARAMS_WITH_BOOKS_WITHOUT_AUTHORS;
import static com.epam.dao.BooksDao.BooksSql.Select.SQL_SELECT_BOOK_BY_ID;
import static com.epam.dao.BooksDao.BooksSql.Select.SQL_SELECT_BY_BOOK_NAME_AND_PH_NAME;
import static com.epam.dao.util.ExtractEntity.extractBook;
import static com.epam.dao.util.ExtractEntity.extractId;
import static com.epam.dao.util.SqlConstants.SQL_AUTHORS_BOOKS_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_BOOKS_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_CITY_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_PUBLISHING_HOUSE_COLUMNS;

/**
 * Class works with books table
 * This table store info about books
 */
public class BooksDao {
    private static final Logger logger = LogManager.getLogger(BooksDao.class);
    private static final AuthorsDao authorsDao = AuthorsDao.getInstance();

    public static class BooksSql {
        public static class Select {
            static final String SQL_SELECT_BOOKS = String.format("SELECT %s, %s, %s FROM books "
                            + "LEFT JOIN publishing_houses ON books.publishing_house_id = publishing_houses.id "
                            + "LEFT JOIN cities ON publishing_houses.city_id = cities.id ",
                    SQL_BOOKS_COLUMNS, SQL_PUBLISHING_HOUSE_COLUMNS, SQL_CITY_COLUMNS);

            static final String SQL_SELECT_BY_BOOK_NAME_AND_PH_NAME = SQL_SELECT_BOOKS
                    + "WHERE books.name LIKE \"%\"?\"%\" AND publishing_houses.name LIKE \"%\"?\"%\" ";

            static final String SQL_SELECT_BOOKS_BY_SEARCH_PARAMS = SQL_SELECT_BY_BOOK_NAME_AND_PH_NAME
                    + "AND books.id IN (SELECT book_id FROM authors_books "
                    + "LEFT JOIN authors ON authors_books.author_id = authors.id "
                    + "WHERE authors.first_name LIKE  \"%\"?\"%\" OR authors.surname LIKE  \"%\"?\"%\" OR authors.patronymic LIKE  \"%\"?\"%\") ";
            static final String SQL_SELECT_BOOKS_BY_SEARCH_PARAMS_WITH_BOOKS_WITHOUT_AUTHORS = SQL_SELECT_BY_BOOK_NAME_AND_PH_NAME
                    + "AND books.id IN (SELECT book_id FROM authors_books "
                    + "LEFT JOIN authors ON authors_books.author_id = authors.id "
                    + "WHERE authors.first_name LIKE  \"%\"?\"%\" OR authors.surname LIKE  \"%\"?\"%\" OR authors.patronymic LIKE  \"%\"?\"%\" "
                    + "OR books.id NOT IN (SELECT book_id FROM authors_books))";

            static final String SQL_SELECT_BOOKS_BY_AUTHOR_ID = String.format("SELECT %s, %s, %s, %s FROM authors_books ",
                    SQL_AUTHORS_BOOKS_COLUMNS, SQL_BOOKS_COLUMNS, SQL_PUBLISHING_HOUSE_COLUMNS, SQL_CITY_COLUMNS)
                    + "LEFT JOIN books ON authors_books.book_id = books.id "
                    + "LEFT JOIN publishing_houses ON books.publishing_house_id = publishing_houses.id "
                    + "LEFT JOIN cities ON publishing_houses.city_id = cities.id "
                    + "WHERE authors_books.author_id = ?";

            static final String SQL_SELECT_BOOK_BY_ID = SQL_SELECT_BOOKS + " WHERE books.id = ?";
        }

        public static class Other {

            static final String SQL_INSERT_BOOK = "INSERT INTO books (name, publishing_house_id, issue_year, count)  VALUES (?,?,?,?)";

            static final String SQL_UPDATE_BOOK = "UPDATE books SET name = ?, publishing_house_id = ?, "
                    + "issue_year = ?, count = ? WHERE id = ?";

            static final String SQL_DELETE_BOOK = "DELETE FROM books WHERE id=?";
        }
    }

    private static BooksDao instance;

    private BooksDao() {
    }

    public static synchronized BooksDao getInstance() {
        if (instance == null) {
            instance = new BooksDao();
        }

        return instance;
    }

    /**
     * Select book with its authors by id
     *
     * @param id - book id
     * @return book with its authors
     */
    public Optional<Book> findBookById(int id) {
        Optional<Book> optionalBook = Optional.empty();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BOOK_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Book book = extractBookWithAuthors(resultSet);
                optionalBook = Optional.of(book);
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find book by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return optionalBook;
    }

    /**
     * Select all books with theirs authors
     *
     * @return books with theirs authors
     */
    public List<Book> findAllBooks() {
        List<Book> books = new ArrayList<>();

        try (Connection connection = DBManager.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_BOOKS)) {

            while (resultSet.next()) {
                Book book = extractBookWithAuthors(resultSet);
                books.add(book);
            }
        } catch (SQLException e) {
            String errorMessage = "Cannot find all books";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return books;
    }

    /**
     * Select all books that were written by author with such id (without their authors)
     *
     * @param authorId - book id
     * @return books without theirs authors
     */
    public List<Book> findAllBooksByAuthorId(int authorId) {
        List<Book> books = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BOOKS_BY_AUTHOR_ID)) {
            preparedStatement.setInt(1, authorId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Book book = extractBook(resultSet);
                books.add(book);
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find all books by author id=%d", authorId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }

        return books;
    }

    /**
     * Select all books with their authors by current parameters
     *
     * @param bookName - book name
     * @param phName   - publishing house name
     * @return books with such values
     */
    public List<Book> findBookBySearchParameters(String bookName, String phName, String authorName) {
        List<Book> booksByName = new ArrayList<>();
        String sqlQuery = SQL_SELECT_BOOKS_BY_SEARCH_PARAMS_WITH_BOOKS_WITHOUT_AUTHORS;

        if (isNullOrEmpty(authorName) && isNullOrEmpty(bookName) && isNullOrEmpty(phName)) {
            sqlQuery = SQL_SELECT_BOOKS;
        } else if (isNullOrEmpty(authorName) && isNullOrEmpty(bookName) && !isNullOrEmpty(phName)) {
            sqlQuery = SQL_SELECT_BY_BOOK_NAME_AND_PH_NAME;
        } else if (isNullOrEmpty(authorName) && !isNullOrEmpty(bookName) && isNullOrEmpty(phName)) {
            sqlQuery = SQL_SELECT_BY_BOOK_NAME_AND_PH_NAME;
        } else if (isNullOrEmpty(authorName) && !isNullOrEmpty(bookName) && !isNullOrEmpty(phName)) {
            sqlQuery = SQL_SELECT_BY_BOOK_NAME_AND_PH_NAME;
        }

        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ) {
            if (sqlQuery.equals(SQL_SELECT_BOOKS)) {
               return findBooks(preparedStatement);
            }
            preparedStatement.setString(1, bookName != null ? bookName : "");
            preparedStatement.setString(2, phName != null ? phName : "");
            if (sqlQuery.equals(SQL_SELECT_BOOKS_BY_SEARCH_PARAMS_WITH_BOOKS_WITHOUT_AUTHORS)) {
                preparedStatement.setString(3, authorName != null ? authorName : "");
                preparedStatement.setString(4, authorName != null ? authorName : "");
                preparedStatement.setString(5, authorName != null ? authorName : "");
            }
            booksByName = findBooks(preparedStatement);
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find all books by name=%s and authorName=%s and phName=%s",
                    bookName, authorName, phName);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return booksByName;
    }

    private boolean isNullOrEmpty(String value) {
        return value == null || value.isEmpty();
    }

    private List<Book> findBooks(PreparedStatement preparedStatement) throws SQLException {
        List<Book> books = new ArrayList<>();

        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Book book = extractBookWithAuthors(resultSet);
                books.add(book);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return books;
    }

    public BookDto insertBook(BookDto book) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prSt = connection.prepareStatement(SQL_INSERT_BOOK, Statement.RETURN_GENERATED_KEYS)) {
            createPreparedStatementForInsertUpdate(prSt, book);
            prSt.executeUpdate();
            book.setId(extractId(prSt));
        } catch (Exception e) {
            String errorMessage = "Cannot save book";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return book;
    }

    public BookDto updateBook(BookDto book, int id) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prSt = connection.prepareStatement(SQL_UPDATE_BOOK)) {
            createPreparedStatementForInsertUpdate(prSt, book);
            prSt.setInt(5, id);
            prSt.executeUpdate();
        } catch (Exception e) {
            String errorMessage = String.format("Cannot update book by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return book;
    }

    public int deleteBook(int id) {
        int count = 0;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prSt = connection.prepareStatement(SQL_DELETE_BOOK)) {
            prSt.setInt(1, id);
            count = prSt.executeUpdate();
        } catch (Exception e) {
            String errorMessage = String.format("Cannot delete book by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return count;
    }

    private void createPreparedStatementForInsertUpdate(PreparedStatement preparedStatement,
                                                        BookDto book) throws SQLException {
        preparedStatement.setString(1, book.getName());
        preparedStatement.setInt(2, book.getPublishingHouse().getId());
        preparedStatement.setInt(3, book.getIssueYear());
        preparedStatement.setInt(4, book.getCount());
    }

    private Book extractBookWithAuthors(ResultSet resultSet) throws SQLException {
        Book book = extractBook(resultSet);
        book.setAuthors(authorsDao.findAllAuthorsByBookId(book.getId()));
        return book;
    }
}




