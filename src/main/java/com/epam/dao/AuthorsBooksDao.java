package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.exception.DataBaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Class works with authors_books table
 * This table store Many-to-Many relationship between authors and books
 */
public class AuthorsBooksDao {

    private static final Logger logger = LogManager.getLogger(AuthorsBooksDao.class);
    private static final String SQL_INSERT_AUTHORS_BOOKS = "INSERT INTO authors_books (author_id, book_id) VALUES (?,?)";
    private static final String SQL_DELETE_AUTHOR_FROM_AUTHORS_BOOKS = "DELETE FROM authors_books WHERE author_id=?";
    private static final String SQL_DELETE_BOOKS_FROM_AUTHORS_BOOKS = "DELETE FROM authors_books WHERE book_id=?";

    private static AuthorsBooksDao instance;

    private AuthorsBooksDao() {
    }

    public static synchronized AuthorsBooksDao getInstance() {
        if (instance == null) {
            instance = new AuthorsBooksDao();
        }
        return instance;
    }

    public void authorAndBookAddById(int authorId, int bookId) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHORS_BOOKS);
        ) {
            preparedStatement.setInt(1, authorId);
            preparedStatement.setInt(2, bookId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            String errorMessage = String.format("Cannot save authors-book relationship with authorId=%d and bookId=%d",
                    authorId, bookId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
    }

    public void deleteAuthorFromAuthorsBook(int authorId) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR_FROM_AUTHORS_BOOKS);
        ) {
            preparedStatement.setInt(1, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot delete relationship author id=%d from authors-book relationship",
                    authorId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
    }

    public void deleteBookFromAuthorsBook(int bookId) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BOOKS_FROM_AUTHORS_BOOKS);
        ) {
            preparedStatement.setInt(1, bookId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot delete relationship by book id=%d from authors-book relationship", bookId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
    }
}