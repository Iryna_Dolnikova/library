package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.dao.util.ExtractEntity;
import com.epam.exception.DataBaseException;
import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import com.epam.web.dto.LoginDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.dao.util.SqlConstants.SQL_USERS_COLUMNS;

/**
 * Class works with users table
 * This table store info about users
 */
public class UsersDao {
    private static final Logger logger = LogManager.getLogger(UsersDao.class);
    private static UsersDao instance;

    private static final String SQL_SELECT_USERS = String.format("SELECT %s FROM users "
                    + "WHERE users.role = ? AND users.is_active = ?",
            SQL_USERS_COLUMNS);

    private static final String SQL_SELECT_USER_BY_ID = String.format(
            "SELECT %s FROM users WHERE users.id = ? AND users.role = ?", SQL_USERS_COLUMNS);
    private static final String SQL_SELECT_ACTIVE_USER_BY_ID = String.format(
            "SELECT %s FROM users WHERE users.id = ? AND users.role = ? AND users.is_active = ?", SQL_USERS_COLUMNS);
    private static final String SQL_SELECT_USER_ID_BY_PHONE_OR_EMAIL = "SELECT id FROM users WHERE email = ? OR phone = ?";

    // request for login
    private static final String SQL_SELECT_USER_ID_BY_EMAIL_AND_PASSWORD =
            "SELECT id, role FROM users WHERE email = ? AND password = SHA(?) AND is_active = true";

    private static final String SQL_INSERT_USER = "INSERT INTO users "
            + "(surname, first_name, patronymic, email, phone, role, password, is_blocked, is_active) "
            + "VALUES (?,?,?,?,?,?, SHA(?), ?,?)";

    private static final String SQL_UPDATE_USER = "UPDATE users SET surname=?, first_name=?," +
            "patronymic=?, email=?, phone=?, role=? WHERE id = ? AND is_active=true";

    private static final String SQL_DELETE_USER = "UPDATE users SET is_active=false WHERE id=?";

    private static final String SQL_UPDATE_READER_BLOCK = "UPDATE users SET is_blocked = ? WHERE id = ?";

    private UsersDao() {
    }

    public static synchronized UsersDao getInstance() {
        if (instance == null) {
            instance = new UsersDao();
        }
        return instance;
    }

    public Optional<User> findActiveReaderById(int id) {
        return findUserById(id, RolesEnum.READER, true);
    }

    public Optional<User> findReaderById(int id) {
        return findUserById(id, RolesEnum.READER, false);
    }

    public Optional<User> findActiveLibrarianById(int id) {
        return findUserById(id, RolesEnum.LIBRARIAN, true);
    }

    public Optional<User> findLibrarianById(int id) {
        return findUserById(id, RolesEnum.LIBRARIAN, false);
    }

    public Optional<User> findAdminById(int id) {
        return findUserById(id, RolesEnum.ADMIN, true);
    }

    private Optional<User> findUserById(int id, RolesEnum role, boolean isActive) {
        Optional<User> optionalUser = Optional.empty();
        ResultSet resultSet = null;
        String sqlQuery = isActive ? SQL_SELECT_ACTIVE_USER_BY_ID : SQL_SELECT_USER_BY_ID;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, role.name());
            if (isActive) {
                preparedStatement.setBoolean(3, true);
            }
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User user = extractUser(resultSet);
                optionalUser = Optional.of(user);
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find user by id=%d and role=%s", id, role);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return optionalUser;
    }


    public List<User> findAllReaders() {
        return findAllUsersByTheirRole(RolesEnum.READER);
    }

    public List<User> findAllLibrarian() {
        return findAllUsersByTheirRole(RolesEnum.LIBRARIAN);
    }

    private List<User> findAllUsersByTheirRole(RolesEnum role) {
        List<User> users = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USERS)) {
            preparedStatement.setString(1, role.name());
            preparedStatement.setBoolean(2, true);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = extractUser(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find all users by role=%s", role);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return users;
    }


    public User insertUser(User newUser) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prSt = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            createPreparedStatementForInsertUpdate(prSt, newUser);
            prSt.setString(7, newUser.getPassword());
            prSt.setBoolean(8, false);
            prSt.setBoolean(9, true);
            prSt.executeUpdate();
            newUser.setId(ExtractEntity.extractId(prSt));
        } catch (Exception e) {
            String errorMessage = String.format("Cannot insert user");
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return newUser;
    }

    public boolean existUserByPhoneOrEmail(User user) {
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_ID_BY_PHONE_OR_EMAIL)) {
            preparedStatement.setString(1, user.getEmail().trim());
            preparedStatement.setString(2, user.getPhone().trim());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot check if user with email=%s and phone=%s exist",
                    user.getEmail().trim(), user.getPhone().trim());
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return false;
    }

    public Optional<LoginDto> getLoginDtoByEmailAndPassword(String email, String password) {
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_ID_BY_EMAIL_AND_PASSWORD)) {
            preparedStatement.setString(1, email.trim());
            preparedStatement.setString(2, password.trim());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                LoginDto loginDto = new LoginDto();
                loginDto.setEmail(email);
                loginDto.setRole(RolesEnum.valueOf(resultSet.getString("role")));
                loginDto.setId(resultSet.getInt("id"));
                return Optional.of(loginDto);
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot get login data when user try to login for user=%s", email);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return Optional.empty();
    }


    public User updateUser(User userForUpdate, int id) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prSt = connection.prepareStatement(SQL_UPDATE_USER)) {

            createPreparedStatementForInsertUpdate(prSt, userForUpdate);
            prSt.setInt(7, id);
            prSt.executeUpdate();
            userForUpdate.setId(id);
        } catch (Exception e) {
            String errorMessage = String.format("Cannot update user by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return userForUpdate;
    }

    public int deleteUser(int id) {
        int count = 0;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prSt = connection.prepareStatement(SQL_DELETE_USER);
        ) {
            prSt.setInt(1, id);
            count = prSt.executeUpdate();
        } catch (Exception e) {
            String errorMessage = String.format("Cannot delete user by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return count;
    }

    public void updateReaderBlock(int id, boolean isBlocked) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prSt = connection.prepareStatement(SQL_UPDATE_READER_BLOCK)) {

            prSt.setBoolean(1, isBlocked);
            prSt.setInt(2, id);
            prSt.executeUpdate();

        } catch (Exception e) {
            String errorMessage = String.format("Cannot update reader block status for reader=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
    }


    private void createPreparedStatementForInsertUpdate(PreparedStatement preparedStatement,
                                                        User user) throws SQLException {
        preparedStatement.setString(1, user.getSurname());
        preparedStatement.setString(2, user.getFirstName());
        preparedStatement.setString(3, user.getPatronymic());
        preparedStatement.setString(4, user.getEmail());
        preparedStatement.setString(5, user.getPhone());
        preparedStatement.setString(6, user.getRole().name());
        preparedStatement.setBoolean(7, user.isBlocked());
    }

    private User extractUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("userId"));
        user.setFirstName(resultSet.getString("userFirstName"));
        user.setSurname(resultSet.getString("userSurname"));
        user.setPatronymic(resultSet.getString("userPatronymic"));
        user.setEmail(resultSet.getString("userEmail"));
        user.setPhone(resultSet.getString("userPhone"));
        user.setRole(RolesEnum.valueOf(resultSet.getString("userRole")));
        user.setBlocked(resultSet.getBoolean("readerBlock"));
        user.setActive(resultSet.getBoolean("userIsActive"));
        return user;
    }

}
