package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.exception.DataBaseException;
import com.epam.model.ReaderCard;
import com.epam.model.User;
import com.epam.web.dto.ReadersCardDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.dao.util.ExtractEntity.extractBook;
import static com.epam.dao.util.ExtractEntity.extractId;
import static com.epam.dao.util.SqlConstants.SQL_BOOKS_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_CITY_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_PUBLISHING_HOUSE_COLUMNS;

/**
 * Class works with readers_books table
 * This table store info about books that was taken by readers
 */
public class ReadersCardDao {
    private static final Logger logger = LogManager.getLogger(ReadersCardDao.class);

    private static ReadersCardDao instance;

    private static final String SQL_INSERT_READERS_BOOKS = "INSERT INTO readers_books "
            + "(reader_id, librarian_id, book_id, date_of_issue, expected_return_date, actual_return_date) "
            + "VALUES (?, ?, ?, ?, ?, ?) ";

    private static final String SQL_SELECT_READERS_BOOKS = String.format("SELECT %s, %s, %s, rb.id AS rbId, "
                    + "rb.reader_id AS readerId, rb.librarian_id AS librarianId, "
                    + "rb.date_of_issue AS dateOfUse, rb.expected_return_date AS expectedReturnDate, "
                    + "rb.actual_return_date AS actualReturnDate FROM readers_books AS rb "
                    + "LEFT JOIN books ON rb.book_id = books.id "
                    + "LEFT JOIN publishing_houses ON books.publishing_house_id = publishing_houses.id "
                    + "LEFT JOIN cities ON publishing_houses.city_id = cities.id "
                    + "WHERE librarian_id IS NOT NULL ",
            SQL_BOOKS_COLUMNS, SQL_PUBLISHING_HOUSE_COLUMNS, SQL_CITY_COLUMNS);

    private static final String SQL_SELECT_READERS_BOOKS_BY_BOOK_NAME = SQL_SELECT_READERS_BOOKS
            + "AND books.name LIKE \"%\"?\"%\"";
    private static final String SQL_SELECT_READERS_BOOKS_BY_ID = SQL_SELECT_READERS_BOOKS + "AND rb.id = ?";
    private static final String SQL_SELECT_ALL_BOOKS_BY_BOOK_ID = SQL_SELECT_READERS_BOOKS + "AND book_id = ?";
    private static final String SQL_SELECT_ALL_BOOKS_BY_READER_ID = SQL_SELECT_READERS_BOOKS + "AND reader_id = ?";
    private static final String SQL_SELECT_ALL_BOOKS_BY_LIBRARIAN_ID = SQL_SELECT_READERS_BOOKS + "AND librarian_id = ?";

    private static final String SQL_UPDATE_READERS_BOOKS = "UPDATE readers_books SET "
            + "reader_id = ?, librarian_id = ?, book_id = ?, date_of_issue = ?, expected_return_date = ?, actual_return_date = ? "
            + "WHERE id = ?";

    private static final String SQL_DELETE_READERS_BOOKS = "DELETE FROM readers_books WHERE id = ?";

    public ReadersCardDao() {

    }

    public static synchronized ReadersCardDao getInstance() {
        if (instance == null) {
            instance = new ReadersCardDao();
        }
        return instance;
    }

    public ReadersCardDto insertReaderBook(ReadersCardDto readerBook) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_READERS_BOOKS,
                     Statement.RETURN_GENERATED_KEYS);
        ) {
            createPreparedStatementForInsertUpdate(preparedStatement, readerBook);
            preparedStatement.executeUpdate();
            readerBook.setId(extractId(preparedStatement));
        } catch (SQLException e) {
            String errorMessage = "Cannot insert reader-book card";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return readerBook;
    }

    public Optional<ReaderCard> findReaderBookById(int id) {
        Optional<ReaderCard> optionalReaderBook = Optional.empty();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_READERS_BOOKS_BY_ID)) {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                ReaderCard readerCard = extractReaderBook(resultSet);
                optionalReaderBook = Optional.of(readerCard);
            }

        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find reader-book card by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return optionalReaderBook;
    }

    public List<ReaderCard> findReaderBookByBookId(int bookId) {
        return findReaderBookById(bookId, SQL_SELECT_ALL_BOOKS_BY_BOOK_ID);
    }

    public List<ReaderCard> findReaderBookByReaderId(int readerId) {
        return findReaderBookById(readerId, SQL_SELECT_ALL_BOOKS_BY_READER_ID);
    }

    public List<ReaderCard> findReaderBookByLibrarianId(int librarianId) {
        return findReaderBookById(librarianId, SQL_SELECT_ALL_BOOKS_BY_LIBRARIAN_ID);
    }

    private List<ReaderCard> findReaderBookById(int id, String sqlRequest) {
        List<ReaderCard> readerCardList = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prStatement = connection.prepareStatement(sqlRequest)) {
            prStatement.setInt(1, id);
            resultSet = prStatement.executeQuery();

            while (resultSet.next()) {
                readerCardList.add(extractReaderBook(resultSet));
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find reader-book card by user id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return readerCardList;
    }

    public List<ReaderCard> findReadersBooksBySearchParameter(String bookName) {
        List<ReaderCard> readersBooks = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_READERS_BOOKS_BY_BOOK_NAME)) {
            statement.setString(1, bookName != null ? bookName : "");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                readersBooks.add(extractReaderBook(resultSet));
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot find all reader-book cards by bookName=%s", bookName);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }

        return readersBooks;
    }

    public ReadersCardDto updateReaderBook(ReadersCardDto readerBook) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_READERS_BOOKS);
        ) {
            System.out.println("preparedStatement1: " + preparedStatement);
            createPreparedStatementForInsertUpdate(preparedStatement, readerBook);
            preparedStatement.setInt(7, readerBook.getId());
            System.out.println("preparedStatement2: " + preparedStatement);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot update reader-book card by id=%d", readerBook.getId());
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return readerBook;
    }

    public int deleteReaderBook(int id) {
        int count = 0;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_READERS_BOOKS)) {
            preparedStatement.setInt(1, id);
            count = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            String errorMessage = String.format("Cannot delete reader-book card by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return count;
    }

    private void createPreparedStatementForInsertUpdate(PreparedStatement preparedStatement,
                                                        ReadersCardDto readerBook) throws SQLException {
        preparedStatement.setInt(1, readerBook.getReader().getId());
        preparedStatement.setInt(2, readerBook.getLibrarian().getId());
        preparedStatement.setInt(3, readerBook.getBook().getId());
        preparedStatement.setDate(4, readerBook.getDateOfIssue());
        preparedStatement.setDate(5, readerBook.getExpectedReturnDate());
        preparedStatement.setDate(6, readerBook.getActualReturnDate());
    }

    private ReaderCard extractReaderBook(ResultSet resultSet) throws SQLException {
        int readerId = resultSet.getInt("readerId");
        int libraryId = resultSet.getInt("librarianId");

        return ReaderCard.Builder.builder()
                .id(resultSet.getInt("rbId"))
                .dateOfIssue(resultSet.getDate("dateOfUse"))
                .expectedReturnDate(resultSet.getDate("expectedReturnDate"))
                .actualReturnDate(resultSet.getDate("actualReturnDate"))
                .book(extractBook(resultSet))
                .reader(UsersDao.getInstance().findReaderById(readerId).orElse(new User()))
                .librarian(UsersDao.getInstance().findLibrarianById(libraryId).orElse(new User()))
                .build();
    }

}

