package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.exception.DataBaseException;
import com.epam.model.Author;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.dao.AuthorsDao.AuthorsSql.Other.SQL_DELETE_AUTHOR;
import static com.epam.dao.AuthorsDao.AuthorsSql.Other.SQL_INSERT_AUTHOR;
import static com.epam.dao.AuthorsDao.AuthorsSql.Other.SQL_UPDATE_AUTHOR;
import static com.epam.dao.AuthorsDao.AuthorsSql.Select.SQL_SELECT_AUTHORS;
import static com.epam.dao.AuthorsDao.AuthorsSql.Select.SQL_SELECT_AUTHORS_BY_BOOK_ID;
import static com.epam.dao.AuthorsDao.AuthorsSql.Select.SQL_SELECT_AUTHORS_BY_NAME;
import static com.epam.dao.AuthorsDao.AuthorsSql.Select.SQL_SELECT_AUTHOR_BY_ID;
import static com.epam.dao.AuthorsDao.AuthorsSql.Select.SQL_SELECT_AUTHOR_ID_BY_NAME_UNIQUE;
import static com.epam.dao.util.ExtractEntity.extractAuthor;
import static com.epam.dao.util.ExtractEntity.extractId;
import static com.epam.dao.util.SqlConstants.SQL_AUTHORS_BOOKS_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_AUTHOR_COLUMNS;

/**
 * Class works with authors table
 * This table store info about authors
 */
public class AuthorsDao {
    private static final Logger logger = LogManager.getLogger(AuthorsDao.class);
    private static final BooksDao booksDao = BooksDao.getInstance();

    public static class AuthorsSql {
        public static class Select {
            static final String SQL_SELECT_COMMON_AUTHORS = String.format("SELECT %s FROM authors ",
                    SQL_AUTHOR_COLUMNS);
            static final String SQL_SELECT_AUTHORS = SQL_SELECT_COMMON_AUTHORS + " ORDER BY authors.surname";
            static final String SQL_SELECT_AUTHORS_BY_NAME = SQL_SELECT_COMMON_AUTHORS
                    + "WHERE authorName LIKE \"%\"?\"%\" ORDER BY authors.surname";

            static final String SQL_SELECT_AUTHORS_BY_BOOK_ID = String.format("SELECT %s, %s FROM authors_books ",
                    SQL_AUTHORS_BOOKS_COLUMNS, SQL_AUTHOR_COLUMNS)
                    + "LEFT JOIN authors ON authors_books.author_id = authors.id "
                    + "WHERE authors_books.book_id = ? ORDER BY authors.surname";

            static final String SQL_SELECT_AUTHOR_BY_ID = SQL_SELECT_COMMON_AUTHORS + "WHERE authors.id = ? ORDER BY authors.surname";

            static final String SQL_SELECT_AUTHOR_ID_BY_NAME_UNIQUE = "SELECT id FROM authors "
                    + "WHERE first_name = ? AND surname = ? AND patronymic = ?";

        }

        public static class Other {
            static final String SQL_INSERT_AUTHOR = "INSERT INTO authors (first_name, surname, patronymic) VALUES (?,?,?)";

            static final String SQL_UPDATE_AUTHOR = "UPDATE authors SET first_name = ?, surname = ?, patronymic = ?  WHERE id = ?";

            static final String SQL_DELETE_AUTHOR = "DELETE FROM authors WHERE id=?";
        }
    }

    private static AuthorsDao instance;

    private AuthorsDao() {

    }

    public static synchronized AuthorsDao getInstance() {
        if (instance == null) {
            instance = new AuthorsDao();
        }

        return instance;
    }

    /**
     * Save new author
     *
     * @param author - new author
     * @return new created author with his id
     */
    public Author insertAuthor(Author author) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR,
                     Statement.RETURN_GENERATED_KEYS)
        ) {
            createPreparedStatementForInsertUpdate(preparedStatement, author);
            preparedStatement.executeUpdate();
            author.setId(extractId(preparedStatement));

        } catch (SQLException e) {
            String errorMessage = "Cannot save author";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return author;
    }

    public boolean existAuthorByName(String firstName, String surname, String patronymic) {
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_ID_BY_NAME_UNIQUE)
        ) {
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, patronymic);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot check if author with full name \"%s %s %s\"exist",
                    firstName, surname, patronymic);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return false;
    }


    /**
     * Select author with his books by id
     *
     * @param id - author id
     * @return author with his books
     */
    public Optional<Author> findAuthorById(int id) {
        Optional<Author> optionalAuthor = Optional.empty();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID)) {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Author author = extractAuthorWithBooks(resultSet);
                optionalAuthor = Optional.of(author);
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot find author by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }

        return optionalAuthor;
    }

    /**
     * Select all authors with their books
     *
     * @return all authors with their books
     */
    public List<Author> findAllAuthors() {
        List<Author> authors = new ArrayList<>();

        try (Connection connection = DBManager.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_AUTHORS)) {

            while (resultSet.next()) {
                Author author = extractAuthorWithBooks(resultSet);
                authors.add(author);
            }
        } catch (SQLException e) {
            String errorMessage = "Cannot find all authors";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return authors;
    }

    /**
     * Select all authors with their books by similar author name
     *
     * @param name - author name
     * @return all authors by name
     */
    public List<Author> findAuthorsByName(String name) {
        List<Author> authors = new ArrayList<>();

        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHORS_BY_NAME);) {
            preparedStatement.setString(1, name);
            authors = findAuthors(preparedStatement);
        } catch (Exception e) {
            String errorMessage = String.format("Cannot find all authors by name=%s", name);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return authors;
    }

    /**
     * Select all authors who has written book with such id without their books
     *
     * @param bookId - book id
     * @return authors without books
     */
    public List<Author> findAllAuthorsByBookId(int bookId) {
        List<Author> authors = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHORS_BY_BOOK_ID)) {
            preparedStatement.setInt(1, bookId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Author author = extractAuthor(resultSet);
                authors.add(author);
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find all authors by book id=%d", bookId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }

        return authors;
    }

    /**
     * Update existed author
     *
     * @param author - author with existent id and updated fields
     * @return updated author
     */
    public Author updateAuthor(Author author) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
            createPreparedStatementForInsertUpdate(preparedStatement, author);
            preparedStatement.setInt(4, author.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = "Cannot update author";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return author;
    }

    /**
     * Delete author by id
     *
     * @param id - author's id
     * @return number of affected rows. If return 0 - any author wasn't delete.
     */
    public int deleteAuthor(int id) {
        int count = 0;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR)) {
            preparedStatement.setInt(1, id);
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot delete author by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return count;
    }

    private void createPreparedStatementForInsertUpdate(PreparedStatement preparedStatement,
                                                        Author author) throws SQLException {
        preparedStatement.setString(1, author.getFirstName());
        preparedStatement.setString(2, author.getSurname());
        preparedStatement.setString(3, author.getPatronymic());
    }

    private List<Author> findAuthors(PreparedStatement preparedStatement) throws SQLException {
        List<Author> authors = new ArrayList<>();

        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Author author = extractAuthorWithBooks(resultSet);
                authors.add(author);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return authors;
    }

    private Author extractAuthorWithBooks(ResultSet resultSet) throws SQLException {
        Author author = extractAuthor(resultSet);
        author.setBooks(booksDao.findAllBooksByAuthorId(author.getId()));
        return author;
    }

}
