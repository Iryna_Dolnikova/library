package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.exception.DataBaseException;
import com.epam.model.City;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.dao.util.ExtractEntity.extractCity;
import static com.epam.dao.util.ExtractEntity.extractId;
import static com.epam.dao.util.SqlConstants.SQL_CITY_COLUMNS;

/**
 * Class works with cities table
 * This table store info about cities
 */
public class CitiesDao {
    private static final Logger logger = LogManager.getLogger(CitiesDao.class);

    private static final String SQL_INSERT_CITY = "INSERT INTO cities (name) VALUES (?)";
    private static final String SQL_SELECT_CITY = String.format("SELECT %s FROM cities WHERE id=? ORDER BY id",
            SQL_CITY_COLUMNS);
    private static final String SQL_SELECT_CITY_ID_BY_NAME = "SELECT id FROM cities WHERE name = ?";
    private static final String SQL_SELECT_CITIES = String.format("SELECT %s FROM cities ORDER BY id", SQL_CITY_COLUMNS);
    private static final String SQL_UPDATE_CITY = "UPDATE cities SET name = ? WHERE id = ?";
    private static final String SQL_DELETE_CITY = "DELETE FROM cities WHERE id=?";

    private static CitiesDao instance;

    private CitiesDao() {
    }

    public static synchronized CitiesDao getInstance() {
        if (instance == null) {
            instance = new CitiesDao();
        }
        return instance;
    }

    public City insertCity(String name) {
        City city = new City();
        city.setName(name);

        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_CITY,
                     Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, name.trim());
            preparedStatement.executeUpdate();
            city.setId(extractId(preparedStatement));
        } catch (SQLException e) {
            String errorMessage = "Cannot insert city";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return city;
    }

    public Optional<City> findCityById(int id) {
        Optional<City> optionalCity = Optional.empty();
        ResultSet resultSet = null;

        try (Connection con = DBManager.getConnection();
             PreparedStatement stmt = con.prepareStatement(SQL_SELECT_CITY)) {
            stmt.setInt(1, id);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                City city = extractCity(resultSet);
                optionalCity = Optional.of(city);
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find city by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return optionalCity;
    }

    public List<City> findAllCities() {
        List<City> cities = new ArrayList<>();

        try (Connection con = DBManager.getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(SQL_SELECT_CITIES)) {

            while (rs.next()) {
                cities.add(extractCity(rs));
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot find all cities");
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return cities;
    }

    public boolean existCityByName(String name) {
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_CITY_ID_BY_NAME)) {
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return true;
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot check if city exists by name=%s", name);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return false;
    }


    public City updateCity(String name, int id) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prState = connection.prepareStatement(SQL_UPDATE_CITY);
        ) {
            prState.setString(1, name.trim());
            prState.setInt(2, id);
            prState.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot update city by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return new City(id, name);
    }

    public int deleteCity(int id) {
        int count = 0;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prStm = connection.prepareStatement(SQL_DELETE_CITY)) {
            prStm.setInt(1, id);
            count = prStm.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot delete city by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return count;
    }


}
