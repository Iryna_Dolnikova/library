package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.exception.DataBaseException;
import com.epam.model.Book;
import com.epam.model.User;
import com.epam.web.converter.BookConverter;
import com.epam.web.converter.UserConverter;
import com.epam.web.dto.OrderDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.epam.dao.util.ExtractEntity.extractBook;
import static com.epam.dao.util.ExtractEntity.extractId;
import static com.epam.dao.util.SqlConstants.SQL_BOOKS_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_CITY_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_PUBLISHING_HOUSE_COLUMNS;

/**
 * Class works with readers_books table
 * This table store info about books that was ordered by readers
 */
public class OrderDao {
    private static final Logger logger = LogManager.getLogger(OrderDao.class);

    private static OrderDao instance;

    private static final String SQL_INSERT_ORDER = "INSERT INTO readers_books "
            + "(reader_id, book_id, date_of_issue, expected_return_date) VALUES (?, ?, ?, ?) ";

    private static final String SQL_SELECT_ORDERS = String.format("SELECT %s, %s, %s, rb.id AS rbId, "
                    + "rb.reader_id AS readerId, rb.date_of_issue AS dateOfUse, rb.expected_return_date AS expectedReturnDate "
                    + "FROM readers_books AS rb "
                    + "LEFT JOIN books ON rb.book_id = books.id "
                    + "LEFT JOIN publishing_houses ON books.publishing_house_id = publishing_houses.id "
                    + "LEFT JOIN cities ON publishing_houses.city_id = cities.id "
                    + "WHERE librarian_id IS NULL ",
            SQL_BOOKS_COLUMNS, SQL_PUBLISHING_HOUSE_COLUMNS, SQL_CITY_COLUMNS);

    private static final String SQL_SELECT_ORDERS_BY_BOOK_NAME = SQL_SELECT_ORDERS + "AND books.name LIKE \"%\"?\"%\" ";
    private static final String SQL_SELECT_ORDER_BY_ID = SQL_SELECT_ORDERS + "AND rb.id = ? ";
    private static final String SQL_SELECT_ALL_ORDERS_BY_READER_ID = SQL_SELECT_ORDERS
            + "AND reader_id = ? AND books.name LIKE \"%\"?\"%\" ";

    private static final String SQL_UPDATE_ORDER = "UPDATE readers_books SET "
            + "reader_id = ?,  book_id = ?, date_of_issue = ?, expected_return_date = ? WHERE id = ?";
    private static final String SQL_APPROVE_ORDER = "UPDATE readers_books SET "
            + "librarian_id = ?, date_of_issue = ? WHERE id = ?";
    private static final String SQL_DELETE_ORDER = "DELETE FROM readers_books WHERE id = ?";

    public OrderDao() {

    }

    public static synchronized OrderDao getInstance() {
        if (instance == null) {
            instance = new OrderDao();
        }
        return instance;
    }

    public OrderDto insertOrder(OrderDto orderDto) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_ORDER,
                     Statement.RETURN_GENERATED_KEYS);
        ) {
            createPreparedStatementForInsertUpdate(preparedStatement, orderDto);
            preparedStatement.executeUpdate();
            orderDto.setId(extractId(preparedStatement));
        } catch (SQLException e) {
            String errorMessage = "Cannot insert order card";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return orderDto;
    }

    public Optional<OrderDto> findOrderById(int id) {
        Optional<OrderDto> optionalOrder = Optional.empty();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ORDER_BY_ID)) {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                OrderDto order = extractOrder(resultSet);
                optionalOrder = Optional.of(order);
            }

        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find order by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return optionalOrder;
    }

    public List<OrderDto> findOrderByReaderId(int readerId, String bookName) {
        List<OrderDto> orderDtos = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_ORDERS_BY_READER_ID)) {
            statement.setInt(1, readerId);
            statement.setString(2, bookName != null ? bookName : "");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderDtos.add(extractOrder(resultSet));
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot find all orders by bookName=%s", bookName);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }

        return orderDtos;
    }

    public List<OrderDto> findOrdersBySearchParameter(String bookName) {
        List<OrderDto> orderDtos = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ORDERS_BY_BOOK_NAME)) {
            statement.setString(1, bookName != null ? bookName : "");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderDtos.add(extractOrder(resultSet));
            }
        } catch (Exception e) {
            String errorMessage = String.format("Cannot find all orders by bookName=%s", bookName);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }

        return orderDtos;
    }

    public OrderDto updateOrder(OrderDto orderDto) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_ORDER);
        ) {
            createPreparedStatementForInsertUpdate(preparedStatement, orderDto);
            preparedStatement.setInt(5, orderDto.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot update order by id=%d", orderDto.getId());
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return orderDto;
    }

    public void approveOrder(int orderId, int librarianId) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_APPROVE_ORDER);
        ) {
            preparedStatement.setInt(1, librarianId);
            preparedStatement.setDate(2, new java.sql.Date(new Date().getTime()));
            preparedStatement.setInt(3, orderId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot approve order by id=%d", orderId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
    }

    public int deleteOrder(int id) {
        int count = 0;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_ORDER)) {
            preparedStatement.setInt(1, id);
            count = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            String errorMessage = String.format("Cannot delete order by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return count;
    }

    private void createPreparedStatementForInsertUpdate(PreparedStatement preparedStatement,
                                                        OrderDto orderDto) throws SQLException {
        preparedStatement.setInt(1, orderDto.getReader().getId());
        preparedStatement.setInt(2, orderDto.getBook().getId());
        preparedStatement.setDate(3, orderDto.getDateOfIssue());
        preparedStatement.setDate(4, orderDto.getExpectedReturnDate());
    }

    private OrderDto extractOrder(ResultSet resultSet) throws SQLException {
        int readerId = resultSet.getInt("readerId");
        Book book = extractBook(resultSet);
        User user = UsersDao.getInstance().findReaderById(readerId).orElse(new User());

        OrderDto orderDto = new OrderDto();
        orderDto.setId(resultSet.getInt("rbId"));
        orderDto.setDateOfIssue(resultSet.getDate("dateOfUse"));
        orderDto.setExpectedReturnDate(resultSet.getDate("expectedReturnDate"));
        orderDto.setBook(BookConverter.getInstance().toBookDto(book));
        orderDto.setReader(UserConverter.getInstance().toUserDto(user));
        return orderDto;
    }

}

