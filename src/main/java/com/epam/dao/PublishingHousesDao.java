package com.epam.dao;

import com.epam.dao.util.DBManager;
import com.epam.exception.DataBaseException;
import com.epam.model.PublishingHouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.dao.util.ExtractEntity.extractId;
import static com.epam.dao.util.ExtractEntity.extractPublishingHouse;
import static com.epam.dao.util.SqlConstants.SQL_CITY_COLUMNS;
import static com.epam.dao.util.SqlConstants.SQL_PUBLISHING_HOUSE_COLUMNS;

/**
 * Class works with publishing_houses table
 * This table store info about publishing houses
 */
public class PublishingHousesDao {
    private static final Logger logger = LogManager.getLogger(PublishingHousesDao.class);

    private static final String SQL_INSERT_PUBLISHING_HOUSE = "INSERT INTO publishing_houses(name, city_id) VALUES (?, ?)";

    private static final String SQL_SELECT_HOUSE_IDS_BY_CITY_ID = "SELECT publishing_houses.id FROM publishing_houses "
            + "WHERE publishing_houses.city_id=?";
    private static final String SQL_SELECT_ALL_HOUSES = String.format("SELECT %s, %s FROM publishing_houses "
                    + "INNER JOIN cities ON cities.id = publishing_houses.city_id ORDER BY publishing_houses.city_id",
            SQL_PUBLISHING_HOUSE_COLUMNS, SQL_CITY_COLUMNS);

    private static final String SQL_SELECT_HOUSE = String.format("SELECT %s, %s FROM publishing_houses "
            + "LEFT JOIN cities ON publishing_houses.city_id = cities.id "
            + "WHERE publishing_houses.id=? ", SQL_PUBLISHING_HOUSE_COLUMNS, SQL_CITY_COLUMNS);

    private static final String SQL_SELECT_HOUSE_ID_BY_NAME_AND_CITY = "SELECT id FROM publishing_houses WHERE name = ? AND city_id = ?";

    private static final String SQL_UPDATE_PUBLISHING_HOUSE = "UPDATE publishing_houses SET name = ?, city_id = ? WHERE id = ?";

    private static final String SQL_DELETE_PUBLISHING_HOUSE = "DELETE FROM publishing_houses WHERE id=?";

    private static PublishingHousesDao instance;

    private PublishingHousesDao() {
    }

    public static synchronized PublishingHousesDao getInstance() {
        if (instance == null) {
            instance = new PublishingHousesDao();
        }

        return instance;
    }

    public PublishingHouse insertPublishingHouse(PublishingHouse publishingHouse) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_PUBLISHING_HOUSE,
                     Statement.RETURN_GENERATED_KEYS)
        ) {
            createPreparedStatementForInsertUpdate(preparedStatement, publishingHouse);
            preparedStatement.executeUpdate();
            publishingHouse.setId(extractId(preparedStatement));
        } catch (SQLException e) {
            String errorMessage = "Cannot insert publishing house";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return publishingHouse;
    }

    public boolean existPubHouseByCityId(int cityId) {
        ResultSet resultSet = null;

        try (Connection connection = DBManager.getConnection();
             PreparedStatement pState = connection.prepareStatement(SQL_SELECT_HOUSE_IDS_BY_CITY_ID)
        ) {
            pState.setInt(1, cityId);
            resultSet = pState.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find publishing house by city_id=%d", cityId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return false;
    }

    public Optional<PublishingHouse> findPubHouseById(int id) {
        Optional<PublishingHouse> publishingHouseOptional = Optional.empty();
        ResultSet resultSet = null;

        try (Connection connection = DBManager.getConnection();
             PreparedStatement pState = connection.prepareStatement(SQL_SELECT_HOUSE)
        ) {
            pState.setInt(1, id);
            resultSet = pState.executeQuery();
            if (resultSet.next()) {
                PublishingHouse publishingHouse = extractPublishingHouse(resultSet);
                publishingHouseOptional = Optional.of(publishingHouse);
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot find publishing house by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return publishingHouseOptional;
    }

    public boolean existPubHouse(String name, int cityId) {
        ResultSet resultSet = null;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_HOUSE_ID_BY_NAME_AND_CITY);
        ) {
            preparedStatement.setString(1, name.trim());
            preparedStatement.setInt(2, cityId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot check if publishing house exists by name=%s and cityId=%d",
                    name, cityId);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        } finally {
            DBManager.closeResultSet(resultSet);
        }
        return false;
    }


    public List<PublishingHouse> findAllPublishingHouses() {
        List<PublishingHouse> publishingHouses = new ArrayList<>();

        try (Connection connection = DBManager.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_HOUSES)) {
            while (resultSet.next()) {
                PublishingHouse publishingHouse = extractPublishingHouse(resultSet);
                publishingHouses.add(publishingHouse);
            }

        } catch (SQLException e) {
            String errorMessage = "Cannot find all publishing houses";
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }

        return publishingHouses;
    }

    public PublishingHouse updatePublishingHouse(PublishingHouse publishingHouse) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PUBLISHING_HOUSE);
        ) {
            createPreparedStatementForInsertUpdate(preparedStatement, publishingHouse);
            preparedStatement.setInt(3, publishingHouse.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot update publishing house by id=%d", publishingHouse.getId());
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return publishingHouse;

    }

    public int deletePublishingHouse(int id) {
        int count = 0;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_PUBLISHING_HOUSE)) {
            preparedStatement.setInt(1, id);
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            String errorMessage = String.format("Cannot delete publishing house by id=%d", id);
            logger.error(errorMessage + ": " + e);
            throw new DataBaseException(errorMessage);
        }
        return count;
    }

    private void createPreparedStatementForInsertUpdate(PreparedStatement preparedStatement,
                                                        PublishingHouse publishingHouse) throws SQLException {
        preparedStatement.setString(1, publishingHouse.getName().trim());
        preparedStatement.setInt(2, publishingHouse.getCity().getId());
    }

}
