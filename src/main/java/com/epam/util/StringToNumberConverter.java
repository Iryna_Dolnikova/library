package com.epam.util;

/**
 * Utility class that convert string parameter into int
 */
public class StringToNumberConverter {

    public static int convertStringToInt(String param) {
        try {
            return Integer.parseInt(param);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

}
