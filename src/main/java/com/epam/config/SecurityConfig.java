package com.epam.config;

import java.util.Arrays;
import java.util.List;

/**
 * This class contains lists of allowed urls for each role
 */
public class SecurityConfig {
    public static final String LOGINED_USER = "loginedUser";

    private final List<String> allowedNonAuthorizedUrls = Arrays.asList("/Library/", "/Library/login", "/Library/registration");

    private final List<String> allowedReadersUrls = Arrays.asList(
            "/Library/authors", "/Library/books", "/Library/cities", "/Library/publishingHouses",
            "/Library/orders_add", "/Library/orders", "/Library/orders_edit", "/Library/orders_update",
            "/Library/orders_delete",
            "/Library/search", "/Library/profile", "/Library/logout");

    private final List<String> allowedForLibrarian = Arrays.asList(
            "/Library/authors_add", "/Library/authors_delete", "/Library/authors", "/Library/authors_edit", "/Library/authors_update",
            "/Library/books_add", "/Library/books_delete", "/Library/books", "/Library/books_edit", "/Library/books_update",
            "/Library/cities_add", "/Library/cities_delete", "/Library/cities", "/Library/cities_edit", "/Library/cities_update",
            "/Library/publishingHouses_add", "/Library/publishingHouses_delete", "/Library/publishingHouses", "/Library/publishingHouses_edit", "/Library/publishingHouse_update",
            "/Library/readers_add", "/Library/readers_delete", "/Library/readers", "/Library/readers_edit", "/Library/users_update",
            "/Library/readersCards_add", "/Library/readersCards_delete", "/Library/readersCards",
            "/Library/readersCards_edit", "/Library/readersCards_update",
            "/Library/orders", "/Library/orders_approve",
            "/Library/search", "/Library/profile", "/Library/logout");

    public boolean isAllowedForNonAuthorizedUrls(String url) {
        return allowedNonAuthorizedUrls.contains(url);
    }

    public boolean isAllowedForReaders(String url) {
        return allowedReadersUrls.contains(url);
    }

    public boolean isAllowedForLibrarian(String url) {
        return allowedForLibrarian.contains(url);
    }
}
