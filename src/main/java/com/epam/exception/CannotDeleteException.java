package com.epam.exception;

/**
 * Exception is thrown when object was not found by id
 */
public class CannotDeleteException extends RuntimeException {

    public CannotDeleteException(String massage) {
        super(massage);
    }
}
