package com.epam.exception;

/**
 * Exception is thrown when try to give book for blocked reader
 */
public class ReaderBlockedException extends RuntimeException{

    public ReaderBlockedException (String message) {
        super(message);
    }
}

