package com.epam.exception;

/**
 * Exception is thrown when try to create any object that already exists in the data base
 */
public class ObjectAlreadyExistException extends RuntimeException {

    public ObjectAlreadyExistException(String massage) {
        super(massage);
    }
}
