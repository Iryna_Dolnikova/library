package com.epam.exception;

/**
 * Exception is thrown when smb want to take book with count = 0
 */
public class BookAbsentException extends RuntimeException {
    public BookAbsentException(String message) {
        super(message);
    }
}
