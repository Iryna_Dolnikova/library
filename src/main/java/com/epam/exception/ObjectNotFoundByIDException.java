package com.epam.exception;

/**
 * Exception is thrown when object was not found by id
 */
public class ObjectNotFoundByIDException extends RuntimeException {

    public ObjectNotFoundByIDException(String massage) {
        super(massage);
    }
}
