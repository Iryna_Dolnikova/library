package com.epam.exception;

/**
 * Unchecked exception is thrown when some SQLException was catched
 */
public class DataBaseException extends RuntimeException {

    public DataBaseException(String message) {
        super(message);
    }
}
