package com.epam.exception;

/**
 * Exception is thrown when user with invalid email or password try to login
 */
public class UserDoesNotExistException extends RuntimeException {

    public UserDoesNotExistException(String message) {
        super(message);
    }
}
