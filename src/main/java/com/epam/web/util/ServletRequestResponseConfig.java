package com.epam.web.util;

import com.epam.model.City;
import com.epam.model.PublishingHouse;
import com.epam.service.*;
import com.epam.service.impl.*;
import com.epam.web.dto.AuthorDto;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.UserDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Utility class that set data into Http requests & responses
 */
public class ServletRequestResponseConfig {

    public static void setCharacterEncoding(HttpServletRequest req, HttpServletResponse resp) throws UnsupportedEncodingException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
    }

    public static void setCities(HttpServletRequest req) {
        CityService cityService = new CityServiceImpl();
        List<City> cities = cityService.findAllCities();
        req.setAttribute("cities", cities);
    }

    public static void setReaders(HttpServletRequest req) {
        UserService userService = new UserServiceImpl();
        List<UserDto> readers = userService.findAllReaders();
        req.setAttribute("readers", readers);
    }

    public static void setLibrarians(HttpServletRequest req) {
        UserService userService = new UserServiceImpl();
        List<UserDto> librarians = userService.findAllLibrarians();
        req.setAttribute("librarians", librarians);
    }

    public static void setBooks(HttpServletRequest req) {
        BookService bookService = BookServiceImpl.getInstance();
        List<BookDto> books = bookService.findAll();
        req.setAttribute("books", books);
    }

    public static void setAuthors(HttpServletRequest req) {
        AuthorService authorService = new AuthorServiceImpl();
        List<AuthorDto> authorDtos = authorService.findAll();
        req.setAttribute("authors", authorDtos);
    }

    public static void setPublishingHouse(HttpServletRequest req) {
        PublishingHouseService publishingHouseService = new PublishingHouseServiceImpl();
        List<PublishingHouse> publishingHouses = publishingHouseService.findAll();
        req.setAttribute("publishingHouses", publishingHouses);
    }
}
