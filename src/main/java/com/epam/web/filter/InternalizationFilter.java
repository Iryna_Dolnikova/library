package com.epam.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * InternalizationFilter - filter that runs first.
 *
 * Filter checks request parameter "language" and sets locale into session.
 */
public class InternalizationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpSession httpSession = httpServletRequest.getSession();
        servletRequest.setCharacterEncoding("UTF-8");

        String language = httpServletRequest.getParameter("language");
        setLocaleIntoSession(httpSession, language);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * Method set locale into session:
     * If request contains parameter "language" - set locale from request into session.
     * If request doesn't contain parameter "language" and session doesn't contain locale - set default EN locale into session.
     * If request doesn't contain parameter "language" and session contains locale - doesn't change locale from session.
     *
     * @param httpSession - current session
     * @param language    - language parameter from Http request
     */
    private void setLocaleIntoSession(HttpSession httpSession, String language) {
        Object sessionLanguage = httpSession.getAttribute("language");
        if (language == null && sessionLanguage == null) {
            httpSession.setAttribute("language", new Locale("eu", "US"));
        }

        if (language != null) {
            httpSession.setAttribute("language", getLocaleFromLanguage(language));
        }
    }

    /**
     * Method return Locale based on request parameter.
     *
     * @param language - language parameter from Http request
     * @return locale based on request parameter
     */
    private Locale getLocaleFromLanguage(String language) {
        return language != null && language.equalsIgnoreCase("uk")
                ? new Locale("uk", "UA")
                : new Locale("eu", "US");
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
