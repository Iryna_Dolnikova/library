package com.epam.web.filter;


import com.epam.config.SecurityConfig;
import com.epam.model.enums.RolesEnum;
import com.epam.web.dto.LoginDto;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.config.SecurityConfig.LOGINED_USER;

/**
 * SecurityFilter - filter that runs second.
 *
 * Filter checks user access into each request and:
 * if user is not logged in - redirect to login page,
 * if user doesn't have permission - send redirect into main page.
 */
public class SecurityFilter implements Filter {

    private SecurityConfig securityConfig = new SecurityConfig();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpSession httpSession = httpServletRequest.getSession();

        checkError(httpSession);

        String requestURI = httpServletRequest.getRequestURI();
        if (!securityConfig.isAllowedForNonAuthorizedUrls(requestURI)) {
            LoginDto loginDto = (LoginDto) httpSession.getAttribute(LOGINED_USER);

            if (loginDto == null) {
                httpServletResponse.sendRedirect("/Library/");
                return;
            }
            if (loginDto.getRole().equals(RolesEnum.READER) && !securityConfig.isAllowedForReaders(requestURI)) {
                httpServletResponse.sendRedirect("/Library/search");
                return;
            }
            if (loginDto.getRole().equals(RolesEnum.LIBRARIAN) && !securityConfig.isAllowedForLibrarian(requestURI)) {
                httpServletResponse.sendRedirect("/Library/search");
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void checkError(HttpSession httpSession) {
        String error = (String) httpSession.getAttribute("error");
        Integer errorTimes = (Integer) httpSession.getAttribute("error_times");
        if (error != null && errorTimes.equals(0)) {
            httpSession.setAttribute("error_times", 1);
        } else if (error != null && errorTimes.equals(1)) {
            httpSession.setAttribute("error", null);
            httpSession.setAttribute("error_times", null);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
