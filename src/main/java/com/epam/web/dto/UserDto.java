package com.epam.web.dto;

import com.epam.model.enums.RolesEnum;

import java.util.Objects;

public class UserDto {
    private int id;
    private String fullName;
    private String surname;
    private String firstName;
    private String patronymic;
    private String email;
    private String phone;
    private RolesEnum role;
    private boolean isBlocked;
    private boolean isActive;

    public UserDto() {

    }

    public UserDto(int id) {
        this.id = id;
    }

    public UserDto(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    public UserDto(int id, String surname, String firstName, String patronymic, String email, String phone) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.email = email;
        this.phone = phone;
    }

    public UserDto(int id, String fullName, String surname, String firstName, String patronymic, String email, String phone, RolesEnum role, boolean isBlocked) {
        this.id = id;
        this.fullName = fullName;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.isBlocked = isBlocked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public RolesEnum getRole() {
        return role;
    }

    public void setRole(RolesEnum roll) {
        this.role = roll;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public boolean getIsBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDto)) return false;
        UserDto userDto = (UserDto) o;
        return getId() == userDto.getId() && isBlocked() == userDto.isBlocked() && Objects.equals(getFullName(), userDto.getFullName()) && Objects.equals(getSurname(), userDto.getSurname()) && Objects.equals(getFirstName(), userDto.getFirstName()) && Objects.equals(getPatronymic(), userDto.getPatronymic()) && Objects.equals(getEmail(), userDto.getEmail()) && Objects.equals(getPhone(), userDto.getPhone()) && getRole() == userDto.getRole();
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + fullName.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + patronymic.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + role.hashCode();
        result = 31 * result + (isBlocked ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", surname='" + surname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", role=" + role +
                ", isBlocked=" + isBlocked +
                ", isActive=" + isActive +
                '}';
    }
}
