package com.epam.web.dto;

import java.util.List;
import java.util.Objects;

public class OrderSearchDto {

    private List<OrderDto> orders;
    private int pagesCount;
    private int currentPage;

    public OrderSearchDto() {

    }

    public OrderSearchDto(List<OrderDto> orders, int pagesCount, int currentPage) {
        this.orders = orders;
        this.pagesCount = pagesCount;
        this.currentPage = currentPage;
    }

    public List<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDto> orders) {
        this.orders = orders;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString() {
        return "OrderSearchDto{" +
                "orders=" + orders +
                ", pagesCount=" + pagesCount +
                ", currentPage=" + currentPage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderSearchDto)) return false;
        OrderSearchDto that = (OrderSearchDto) o;
        return pagesCount == that.pagesCount && currentPage == that.currentPage && orders.equals(that.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orders, pagesCount, currentPage);
    }
}
