package com.epam.web.dto;

import java.util.List;
import java.util.Objects;

public class BookSearchDto {

    private List<BookDto> bookDtoList;
    private int pagesCount;

    public BookSearchDto(List<BookDto> bookDtoList, int pagesCount) {
        this.bookDtoList = bookDtoList;
        this.pagesCount = pagesCount;
    }

    public List<BookDto> getBookDtoList() {
        return bookDtoList;
    }

    public void setBookDtoList(List<BookDto> bookDtoList) {
        this.bookDtoList = bookDtoList;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    @Override
    public String toString() {
        return "BookSearchDto{" +
                "bookDtoList=" + bookDtoList +
                ", pagesCount=" + pagesCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookSearchDto that = (BookSearchDto) o;
        return pagesCount == that.pagesCount && bookDtoList.equals(that.bookDtoList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookDtoList, pagesCount);
    }
}
