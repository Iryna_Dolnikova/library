package com.epam.web.dto;

import java.util.List;
import java.util.Objects;

public class ReadersCardSearchDto {

    private List<ReadersCardDto> readersCardDtoList;
    private int pagesCount;

    public ReadersCardSearchDto() {

    }

    public ReadersCardSearchDto(List<ReadersCardDto> readersCardDtoList, int pagesCount) {
        this.readersCardDtoList = readersCardDtoList;
        this.pagesCount = pagesCount;
    }

    public List<ReadersCardDto> getReadersCardDtoList() {
        return readersCardDtoList;
    }

    public void setReadersCardDtoList(List<ReadersCardDto> readersCardDtoList) {
        this.readersCardDtoList = readersCardDtoList;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    @Override
    public String toString() {
        return "ReadersCardSearchDto{" +
                "readersCardDtoList=" + readersCardDtoList +
                ", pagesCount=" + pagesCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReadersCardSearchDto)) return false;
        ReadersCardSearchDto that = (ReadersCardSearchDto) o;
        return getPagesCount() == that.getPagesCount() && getReadersCardDtoList().equals(that.getReadersCardDtoList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReadersCardDtoList(), getPagesCount());
    }
}
