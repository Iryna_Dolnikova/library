package com.epam.web.dto;

import java.sql.Date;

public class ReadersCardDto {
    private int id;
    private Date dateOfIssue;
    private Date expectedReturnDate;
    private Date actualReturnDate;
    private UserDto reader;
    private UserDto librarian;
    private BookDto book;

    private int penalty;

    public ReadersCardDto() {
    }

    public ReadersCardDto(int id, Date expectedReturnDate, Date actualReturnDate) {
        this.id = id;
        this.expectedReturnDate = expectedReturnDate;
        this.actualReturnDate = actualReturnDate;
    }

    public ReadersCardDto(int id, Date dateOfIssue, Date expectedReturnDate, Date actualReturnDate,
                          UserDto reader, UserDto librarian, BookDto book, int penalty) {
        this.id = id;
        this.dateOfIssue = dateOfIssue;
        this.expectedReturnDate = expectedReturnDate;
        this.actualReturnDate = actualReturnDate;
        this.reader = reader;
        this.librarian = librarian;
        this.book = book;
        this.penalty = penalty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(Date dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public Date getExpectedReturnDate() {
        return expectedReturnDate;
    }

    public void setExpectedReturnDate(Date expectedReturnDate) {
        this.expectedReturnDate = expectedReturnDate;
    }

    public Date getActualReturnDate() {
        return actualReturnDate;
    }

    public void setActualReturnDate(Date actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }

    public UserDto getReader() {
        return reader;
    }

    public void setReader(UserDto reader) {
        this.reader = reader;
    }

    public UserDto getLibrarian() {
        return librarian;
    }

    public void setLibrarian(UserDto librarian) {
        this.librarian = librarian;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    @Override
    public String toString() {
        return "ReadersBooksDto{" +
                "id=" + id +
                ", dateOfIssue=" + dateOfIssue +
                ", expectedReturnDate=" + expectedReturnDate +
                ", actualReturnDate=" + actualReturnDate +
                ", reader=" + reader +
                ", librarian=" + librarian +
                ", book=" + book +
                ", penalty=" + penalty +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReadersCardDto)) return false;

        ReadersCardDto that = (ReadersCardDto) o;

        if (getId() != that.getId()) return false;
        if (getPenalty() != that.getPenalty()) return false;
        if (getDateOfIssue() != null ? !getDateOfIssue().equals(that.getDateOfIssue()) : that.getDateOfIssue() != null)
            return false;
        if (getExpectedReturnDate() != null ? !getExpectedReturnDate().equals(that.getExpectedReturnDate()) : that.getExpectedReturnDate() != null)
            return false;
        if (getActualReturnDate() != null ? !getActualReturnDate().equals(that.getActualReturnDate()) : that.getActualReturnDate() != null)
            return false;
        if (getReader() != null ? !getReader().equals(that.getReader()) : that.getReader() != null) return false;
        if (getLibrarian() != null ? !getLibrarian().equals(that.getLibrarian()) : that.getLibrarian() != null)
            return false;
        return getBook() != null ? getBook().equals(that.getBook()) : that.getBook() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getDateOfIssue() != null ? getDateOfIssue().hashCode() : 0);
        result = 31 * result + (getExpectedReturnDate() != null ? getExpectedReturnDate().hashCode() : 0);
        result = 31 * result + (getActualReturnDate() != null ? getActualReturnDate().hashCode() : 0);
        result = 31 * result + (getReader() != null ? getReader().hashCode() : 0);
        result = 31 * result + (getLibrarian() != null ? getLibrarian().hashCode() : 0);
        result = 31 * result + (getBook() != null ? getBook().hashCode() : 0);
        result = 31 * result + getPenalty();
        return result;
    }

    public static class Builder {
        private int id;
        private Date dateOfIssue;
        private Date expectedReturnDate;
        private Date actualReturnDate;
        private UserDto reader;
        private UserDto librarian;
        private BookDto book;
        private int penalty;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder dateOfIssue(Date dateOfIssue) {
            this.dateOfIssue = dateOfIssue;
            return this;
        }

        public Builder expectedReturnDate(Date expectedReturnDate) {
            this.expectedReturnDate = expectedReturnDate;
            return this;
        }

        public Builder actualReturnDate(Date actualReturnDate) {
            this.actualReturnDate = actualReturnDate;
            return this;
        }

        public Builder reader(UserDto readerDto) {
            this.reader = readerDto;
            return this;
        }

        public Builder librarian(UserDto librarianDto) {
            this.librarian = librarianDto;
            return this;
        }

        public Builder book(BookDto bookDto) {
            this.book = bookDto;
            return this;
        }

        public Builder penalty(int penalty) {
            this.penalty = penalty;
            return this;
        }

        public ReadersCardDto build() {
            return new ReadersCardDto(id, dateOfIssue, expectedReturnDate, actualReturnDate, reader, librarian, book, penalty);
        }
    }

}
