package com.epam.web.dto;

import com.epam.model.PublishingHouse;

import java.util.List;
import java.util.Objects;

public class BookDto {
    private int id;
    private String name;
    private List<AuthorDto> authors;
    private PublishingHouse publishingHouse;
    private Integer issueYear;
    private int count;

    public BookDto() {

    }

    public BookDto(int id) {
        this.id = id;
    }

    public BookDto(int id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public BookDto(int id, String name, List<AuthorDto> authors,
                   PublishingHouse publishingHouse, Integer issueYear, int count) {
        this.id = id;
        this.name = name;
        this.authors = authors;
        this.publishingHouse = publishingHouse;
        this.issueYear = issueYear;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AuthorDto> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorDto> authors) {
        this.authors = authors;
    }

    public PublishingHouse getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(PublishingHouse publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Integer getIssueYear() {
        return issueYear;
    }

    public void setIssueYear(Integer issueYear) {
        this.issueYear = issueYear;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", publishingHouse=" + publishingHouse +
                ", publishDate=" + issueYear +
                ", count=" + count +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDto bookDto = (BookDto) o;
        return id == bookDto.id && count == bookDto.count
                && ((name == null && bookDto.name == null) || (name != null && name.equals(bookDto.name)))
                && ((publishingHouse == null && bookDto.publishingHouse == null) || (publishingHouse != null && publishingHouse.equals(bookDto.publishingHouse)))
                && ((issueYear == null && bookDto.issueYear == null) || (issueYear != null && issueYear.equals(bookDto.issueYear)));
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, publishingHouse, issueYear, count);
    }
}
