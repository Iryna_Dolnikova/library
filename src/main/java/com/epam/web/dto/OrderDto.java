package com.epam.web.dto;

import java.sql.Date;
import java.util.Objects;

public class OrderDto {
    private int id;
    private Date dateOfIssue;
    private Date expectedReturnDate;
    private UserDto reader;
    private BookDto book;


    public OrderDto() {
    }


    public OrderDto(int id, Date expectedReturnDate) {
        this.id = id;
        this.expectedReturnDate = expectedReturnDate;
    }

    public OrderDto(int id, Date dateOfIssue, Date expectedReturnDate, UserDto reader, BookDto book) {
        this.id = id;
        this.dateOfIssue = dateOfIssue;
        this.expectedReturnDate = expectedReturnDate;
        this.reader = reader;
        this.book = book;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(Date dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public Date getExpectedReturnDate() {
        return expectedReturnDate;
    }

    public void setExpectedReturnDate(Date expectedReturnDate) {
        this.expectedReturnDate = expectedReturnDate;
    }

    public UserDto getReader() {
        return reader;
    }

    public void setReader(UserDto reader) {
        this.reader = reader;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "ReadersBooksDto{" +
                "id=" + id +
                ", dateOfIssue=" + dateOfIssue +
                ", expectedReturnDate=" + expectedReturnDate +
                ", reader=" + reader +
                ", book=" + book +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDto)) return false;
        OrderDto orderDto = (OrderDto) o;
        return id == orderDto.id && dateOfIssue.equals(orderDto.dateOfIssue) && expectedReturnDate.equals(orderDto.expectedReturnDate) && reader.equals(orderDto.reader) && book.equals(orderDto.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateOfIssue, expectedReturnDate, reader, book);
    }
}
