package com.epam.web.dto;

import com.epam.model.enums.RolesEnum;

public class LoginDto {

    private String email;

    private int id;
    private RolesEnum role;

    public LoginDto (){

    }
    public LoginDto(String email, int id, RolesEnum role) {
        this.email = email;
        this.id = id;
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RolesEnum getRole() {
        return role;
    }

    public void setRole(RolesEnum role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "LoginDto{" +
                "email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

}
