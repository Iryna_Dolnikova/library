package com.epam.web.dto;

import java.util.List;

public class AuthorDto {

    private int id;
    private String fullName;
    private String firstName;
    private String surname;
    private String patronymic;
    private List<BookDto> books;

    public AuthorDto() {

    }

    public AuthorDto(int id, String firstName, String surname, String patronymic) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public List<BookDto> getBooks() {
        return books;
    }

    public void setBooks(List<BookDto> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "AuthorDto{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", books='" + books + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthorDto)) return false;

        AuthorDto authorDto = (AuthorDto) o;

        if (getId() != authorDto.getId()) return false;
        if (getFullName() != null ? !getFullName().equals(authorDto.getFullName()) : authorDto.getFullName() != null)
            return false;
        if (getFirstName() != null ? !getFirstName().equals(authorDto.getFirstName()) : authorDto.getFirstName() != null)
            return false;
        if (getSurname() != null ? !getSurname().equals(authorDto.getSurname()) : authorDto.getSurname() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(authorDto.getPatronymic()) : authorDto.getPatronymic() != null)
            return false;
        return getBooks() != null ? getBooks().equals(authorDto.getBooks()) : authorDto.getBooks() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getFullName() != null ? getFullName().hashCode() : 0);
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getSurname() != null ? getSurname().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + (getBooks() != null ? getBooks().hashCode() : 0);
        return result;
    }
}
