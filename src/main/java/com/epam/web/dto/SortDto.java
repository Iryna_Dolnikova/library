package com.epam.web.dto;

import com.epam.model.enums.SortingEnum;

public class SortDto {
    private String fieldName;
    private SortingEnum sortingEnum;

    public SortDto(String fieldName, SortingEnum sortingEnum) {
        this.fieldName = fieldName;
        this.sortingEnum = sortingEnum;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public SortingEnum getSortingEnum() {
        return sortingEnum;
    }

    public void setSortingEnum(SortingEnum sortingEnum) {
        this.sortingEnum = sortingEnum;
    }

    @Override
    public String toString() {
        return "SortDto{" +
                "fieldName='" + fieldName + '\'' +
                ", sortingEnum=" + sortingEnum +
                '}';
    }
}
