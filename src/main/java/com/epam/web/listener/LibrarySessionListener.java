package com.epam.web.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.time.LocalDateTime;

@WebListener
public class LibrarySessionListener implements HttpSessionListener {

    private static final Logger logger = LogManager.getLogger(LibrarySessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        logger.info("Session {} was created on {}", httpSessionEvent.getSession().getId(), LocalDateTime.now());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        logger.info("Session {} was destroyed on {}", httpSessionEvent.getSession().getId(), LocalDateTime.now());
    }
}
