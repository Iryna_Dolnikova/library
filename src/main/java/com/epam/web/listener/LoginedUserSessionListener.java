package com.epam.web.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.time.LocalDateTime;

import static com.epam.config.SecurityConfig.LOGINED_USER;

@WebListener
public class LoginedUserSessionListener implements HttpSessionAttributeListener {

    private static final Logger logger = LogManager.getLogger(LoginedUserSessionListener.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (event.getName().equalsIgnoreCase(LOGINED_USER)) {
            logger.info("User {} logged in at {}", event.getValue(), LocalDateTime.now());
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        if (event.getName().equalsIgnoreCase(LOGINED_USER)) {
            logger.info("User {} logged out at {}", event.getValue(), LocalDateTime.now());
        }
    }

}
