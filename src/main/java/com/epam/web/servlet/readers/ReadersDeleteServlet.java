package com.epam.web.servlet.readers;

import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for deleting readers
 */
@WebServlet("/readers_delete")
public class ReadersDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(userId);

        if (id >= 1) {
            UserService userService = new UserServiceImpl();
            userService.deleteReader(id);
        }
        resp.sendRedirect("/Library/readers");
    }
}