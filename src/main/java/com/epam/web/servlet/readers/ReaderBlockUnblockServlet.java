package com.epam.web.servlet.readers;

import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setReaders;

/**
 * Servlet for ADMIN only to block and unblock readers
 */
@WebServlet("/readers_block")
public class ReaderBlockUnblockServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String readerId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(readerId);
        UserService userService = new UserServiceImpl();
        if (id >= 1) {
            UserDto reader = userService.blockUnblockReader(id);
            resp.sendRedirect("/Library/readers?id=" + id);
        } else {
            setReaders(req);
            req.getRequestDispatcher("templates/readers/readers.jsp").forward(req, resp);
        }

    }
}
