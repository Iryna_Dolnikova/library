package com.epam.web.servlet.readers;

import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.validator.UserValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes 2 requests:
 * GET - return page for adding reader
 * POST - validate & save new reader in db
 */
@WebServlet("/readers_add")
public class ReaderAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        req.getRequestDispatcher("templates/readers/addReader.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        setCharacterEncoding(req, resp);

        User user = new User();
        user.setSurname(req.getParameter("surname"));
        user.setFirstName(req.getParameter("firstName"));
        user.setPatronymic(req.getParameter("patronymic"));
        user.setEmail(req.getParameter("email"));
        user.setPhone(req.getParameter("phone"));
        user.setPassword(req.getParameter("password"));
        user.setRole(RolesEnum.READER);

        Validator<User> validator = new UserValidator();
        List<String> invalidFields = validator.validate(user, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            doGet(req, resp);
            return;
        }

        UserService userService = new UserServiceImpl();
        UserDto userDto = userService.insertUser(user);

        resp.sendRedirect("/Library/readers?id=" + userDto.getId());
    }

}

