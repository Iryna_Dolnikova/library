package com.epam.web.servlet.readers;

import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request that return page for updating reader
 */
@WebServlet("/readers_edit")
public class ReaderUpdatePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String userId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(userId);
        if (id >= 1) {
            UserService userService = new UserServiceImpl();
            UserDto reader = userService.findReaderById(id);
            req.setAttribute("reader", reader);
            req.getRequestDispatcher("templates/readers/editReader.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("templates/readers/readers.jsp").include(req, resp);
        }
    }

}

