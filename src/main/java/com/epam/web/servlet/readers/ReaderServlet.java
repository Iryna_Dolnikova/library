package com.epam.web.servlet.readers;

import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setReaders;

/**
 * Servlet that processes GET request for getting all readers & reader by id
 */
@WebServlet("/readers")
public class ReaderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String readerId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(readerId);
        UserService userService = new UserServiceImpl();

        if (id >= 1) {
            UserDto readerDto = userService.findReaderById(id);
            req.setAttribute("reader", readerDto);
            req.getRequestDispatcher("templates/readers/reader.jsp").forward(req, resp);
        } else {
            setReaders(req);
            req.getRequestDispatcher("templates/readers/readers.jsp").forward(req, resp);
        }
    }

}
