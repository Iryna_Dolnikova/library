package com.epam.web.servlet.book;

import com.epam.service.BookService;
import com.epam.service.impl.BookServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.AuthorDto;
import com.epam.web.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

import static com.epam.web.util.ServletRequestResponseConfig.*;

/**
 * Servlet that processes GET request that return page for updating book
 */
@WebServlet("/books_edit")
public class BookUpdatePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String bookId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(bookId);

        if (id >= 1) {
            BookService bookService = BookServiceImpl.getInstance();
            BookDto book = bookService.findBookById(id);

            setAuthors(req);
            setPublishingHouse(req);
            req.setAttribute("book", book);
            req.setAttribute("authorBooks", book.getAuthors().stream().map(AuthorDto::getId).collect(Collectors.toList()));
            req.getRequestDispatcher("templates/books/editBook.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("templates/books/books.jsp").include(req, resp);
        }
    }
}