package com.epam.web.servlet.book;

import com.epam.model.PublishingHouse;
import com.epam.service.BookService;
import com.epam.service.impl.BookServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.BookValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.web.util.ServletRequestResponseConfig.*;

/**
 * Servlet that processes POST request for updating book
 */
@WebServlet("/books_update")
public class BookUpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String idOfBook = req.getParameter("id");
        int bookId = StringToNumberConverter.convertStringToInt(idOfBook);
        String countStr = req.getParameter("count");
        int count = StringToNumberConverter.convertStringToInt(countStr);
        String idOfPhouse = req.getParameter("phId");
        int phId = StringToNumberConverter.convertStringToInt(idOfPhouse);

        BookDto book = new BookDto();
        book.setId(bookId);
        book.setName(req.getParameter("name"));
        book.setIssueYear(Integer.valueOf(req.getParameter("issueYear")));
        book.setCount(count);

        PublishingHouse publishingHouse = new PublishingHouse();
        publishingHouse.setId(phId);
        book.setPublishingHouse(publishingHouse);

        String[] authorIds = req.getParameterValues("authorsIds");
        List<Integer> authorIdList = authorIds == null? new ArrayList<>(): Arrays.stream(authorIds)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        Validator<BookDto> validator = new BookValidator();
        List<String> invalidFields = validator.validate(book, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            setAuthors(req);
            setPublishingHouse(req);
            req.setAttribute("book", book);
            req.setAttribute("authorBooks", authorIdList);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            req.getRequestDispatcher("templates/books/editBook.jsp").forward(req, resp);
            return;
        }

        BookService bookService = BookServiceImpl.getInstance();
        BookDto bookDto = bookService.updateBook(book, bookId, authorIdList);

        resp.sendRedirect("/Library/books?id=" + bookDto.getId());
    }
}
