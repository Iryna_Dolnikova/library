package com.epam.web.servlet.book;

import com.epam.service.BookService;
import com.epam.service.impl.BookServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for deleting book
 */
@WebServlet("/books_delete")
public class BookDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookIdStr = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(bookIdStr);

        if (id >= 1) {
            BookService bookService = BookServiceImpl.getInstance();
            bookService.deleteBook(id);
        }
        resp.sendRedirect("/Library/search");
    }
}
