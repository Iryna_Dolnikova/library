package com.epam.web.servlet.book;

import com.epam.service.BookService;
import com.epam.service.impl.BookServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for getting all books & book by id
 */
@WebServlet("/books")
public class BookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String bookId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(bookId);
        if (id >= 1) {
            BookService bookService = BookServiceImpl.getInstance();
            BookDto book = bookService.findBookById(id);
            req.setAttribute("book", book);
            req.getRequestDispatcher("templates/books/book.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("/Library/search");
        }
    }

}
