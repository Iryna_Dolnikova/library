package com.epam.web.servlet.book;

import com.epam.model.PublishingHouse;
import com.epam.service.AuthorService;
import com.epam.service.BookService;
import com.epam.service.PublishingHouseService;
import com.epam.service.impl.AuthorServiceImpl;
import com.epam.service.impl.BookServiceImpl;
import com.epam.service.impl.PublishingHouseServiceImpl;
import com.epam.validator.BookValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.util.StringToNumberConverter.convertStringToInt;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes 2 requests:
 * GET - return page for adding book
 * POST - validate & save new book in db
 */
@WebServlet("/books_add")
public class BookAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        
        AuthorService authorService = new AuthorServiceImpl();
        PublishingHouseService publishingHouseService = new PublishingHouseServiceImpl();
        req.setAttribute("authors", authorService.findAll());
        req.setAttribute("pHouses", publishingHouseService.findAll());
        req.getRequestDispatcher("templates/books/addBook.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        BookDto newBook = new BookDto();
        newBook.setName(req.getParameter("name"));
        newBook.setPublishingHouse(new PublishingHouse(convertStringToInt(req.getParameter("phId"))));
        newBook.setIssueYear(convertStringToInt(req.getParameter("issueYear")));
        newBook.setCount(convertStringToInt(req.getParameter("count")));

        String[] authorsIds = req.getParameterValues("authorsIds");
        List<Integer> authorIdList = authorsIds == null ? new ArrayList<>() : Arrays.stream(authorsIds)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        Validator<BookDto> validator = new BookValidator();
        List<String> invalidFields = validator.validate(newBook, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            req.setAttribute("authorsIds", authorIdList);
            doGet(req, resp);
            return;
        }

        BookService bookService = BookServiceImpl.getInstance();
        BookDto createdBook = bookService.insertBook(newBook, authorIdList);

        resp.sendRedirect("/Library/books?id=" + createdBook.getId());
    }
}
