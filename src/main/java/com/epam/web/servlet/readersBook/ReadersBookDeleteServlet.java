package com.epam.web.servlet.readersBook;

import com.epam.service.ReadersCardService;
import com.epam.service.impl.ReadersCardServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for deleting readers books
 */
@WebServlet("/readersCards_delete")
public class ReadersBookDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idSt = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(idSt);

        if (id >= 1) {
            ReadersCardService readersCardService = new ReadersCardServiceImpl();
            readersCardService.deleteReaderBook(id);
        }
        resp.sendRedirect("/Library/readersCards");
    }
}
