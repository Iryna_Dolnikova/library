package com.epam.web.servlet.readersBook;

import com.epam.service.ReadersCardService;
import com.epam.service.impl.ReadersCardServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.ReadersCardSearchDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for getting all readers cards & reader card by id
 */
@WebServlet("/readersCards")
public class ReadersBooksServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String readerBookId = req.getParameter("id");
        String currentPageStr = req.getParameter("currentPage");
        int id = StringToNumberConverter.convertStringToInt(readerBookId);
        int currentPage = StringToNumberConverter.convertStringToInt(currentPageStr);
        currentPage = currentPage <= -1 ? 0 : currentPage;

        String readerName = req.getParameter("readerName");
        String librarianName = req.getParameter("librarianName");
        String bookName = req.getParameter("bookName");
        ReadersCardService readersCardService = new ReadersCardServiceImpl();

        if (id >= 1) {
            ReadersCardDto readerBook = readersCardService.findReadersBooksById(id);
            req.setAttribute("readerCard", readerBook);
            req.getRequestDispatcher("templates/readersBooks/readerBook.jsp").forward(req, resp);
        } else {
            ReadersCardSearchDto cardSearchDto = readersCardService.findAllReadersBooks(bookName, readerName, librarianName, currentPage);
            int pagesCount = cardSearchDto.getPagesCount();
            List<Integer> pages = IntStream.range(0, pagesCount).boxed().collect(Collectors.toList());

            req.setAttribute("readerCards", cardSearchDto.getReadersCardDtoList());
            req.setAttribute("readerName", readerName);
            req.setAttribute("librarianName", librarianName);
            req.setAttribute("bookName", bookName);
            req.setAttribute("pages", pages);
            req.setAttribute("currentPage", currentPage >= pagesCount ? pagesCount - 1 : currentPage);
            req.getRequestDispatcher("templates/readersBooks/readerBooks.jsp").forward(req, resp);
        }
    }
}
