package com.epam.web.servlet.readersBook;

import com.epam.service.ReadersCardService;
import com.epam.service.impl.ReadersCardServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.ReadersCardDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.*;

/**
 * Servlet that processes GET request that return page for updating readers books
 */
@WebServlet("/readersCards_edit")
public class ReadersBookUpdatePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String rdId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(rdId);

        if (id >= 1) {
            ReadersCardService readersCardService = new ReadersCardServiceImpl();
            ReadersCardDto readerBook = readersCardService.findReadersBooksById(id);
            req.setAttribute("readerCard", readerBook);
            req.setAttribute("readersBookId", readerBook.getId());
            req.setAttribute("dateOfIssue", readerBook.getDateOfIssue());
            req.setAttribute("expectedReturnDate", readerBook.getExpectedReturnDate());
            req.setAttribute("actualReturnDate", readerBook.getActualReturnDate());

            setBooks(req);
            setReaders(req);
            setLibrarians(req);
            req.getRequestDispatcher("templates/readersBooks/editReaderBook.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("templates/readersBooks/readersBooks.jsp").include(req, resp);
        }
    }
}
