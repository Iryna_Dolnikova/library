package com.epam.web.servlet.readersBook;

import com.epam.service.ReadersCardService;
import com.epam.service.impl.ReadersCardServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.ReadersCardValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.util.StringToDateConverter.convertStringToDate;
import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setLibrarians;
import static com.epam.web.util.ServletRequestResponseConfig.setReaders;

/**
 * Servlet that processes POST request for updating readers books
 */
@WebServlet("/readersCards_update")
public class ReadersBookUpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        int id = StringToNumberConverter.convertStringToInt(req.getParameter("id"));
        int bookId = StringToNumberConverter.convertStringToInt(req.getParameter("bookId"));
        int readerId = StringToNumberConverter.convertStringToInt(req.getParameter("readerId"));
        int librarianId = StringToNumberConverter.convertStringToInt(req.getParameter("librarianId"));

        String dateOfIssueStr = req.getParameter("dateOfIssue");
        java.sql.Date dateOfIssue = convertStringToDate(dateOfIssueStr);

        String expectedReturnDateStr = req.getParameter("expectedReturnDate");
        java.sql.Date expectedReturnDate = convertStringToDate(expectedReturnDateStr);
        String actualReturnDateStr = req.getParameter("actualReturnDate");
        java.sql.Date actualReturnDate = convertStringToDate(actualReturnDateStr);

        ReadersCardDto readerCard = ReadersCardDto.Builder.builder()
                .id(id)
                .dateOfIssue(dateOfIssue)
                .expectedReturnDate(expectedReturnDate)
                .actualReturnDate(actualReturnDate)
                .book(new BookDto(bookId))
                .reader(new UserDto(readerId))
                .librarian(new UserDto(librarianId))
                .build();

        Validator<ReadersCardDto> validator = new ReadersCardValidator();
        List<String> invalidFields = validator.validate(readerCard, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            setBooks(req);
            setReaders(req);
            setLibrarians(req);
            req.setAttribute("readerCard", readerCard);
            req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
            req.getRequestDispatcher("templates/readersBooks/editReaderBook.jsp").forward(req, resp);
            return;

        }

        ReadersCardService readersCardService = new ReadersCardServiceImpl();
        ReadersCardDto readersCardDto = readersCardService.updateReaderBook(readerCard);

        resp.sendRedirect("/Library/readersCards?id=" + readersCardDto.getId());
    }

}
