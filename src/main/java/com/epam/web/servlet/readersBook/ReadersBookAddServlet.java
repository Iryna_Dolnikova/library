package com.epam.web.servlet.readersBook;

import com.epam.service.ReadersCardService;
import com.epam.service.impl.ReadersCardServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.ReadersCardValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.util.StringToDateConverter.convertStringToDate;
import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setLibrarians;
import static com.epam.web.util.ServletRequestResponseConfig.setReaders;

/**
 * Servlet that processes 2 requests:
 * GET - return page for adding readers card
 * POST - validate & save new readers card in db
 */
@WebServlet("/readersCards_add")
public class ReadersBookAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        setBooks(req);
        setReaders(req);
        setLibrarians(req);
        req.getRequestDispatcher("templates/readersBooks/addReaderBook.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        int bookId = StringToNumberConverter.convertStringToInt(req.getParameter("bookId"));
        int readerId = StringToNumberConverter.convertStringToInt(req.getParameter("readerId"));
        int librarianId = StringToNumberConverter.convertStringToInt(req.getParameter("librarianId"));

        String dateOfIssueStr = req.getParameter("dateOfIssue");
        java.sql.Date dateOfIssue = convertStringToDate(dateOfIssueStr);
        String expectedReturnDateStr = req.getParameter("expectedReturnDate");
        java.sql.Date expectedReturnDate = convertStringToDate(expectedReturnDateStr);
        String actualReturnDateStr = req.getParameter("actualReturnDate");
        java.sql.Date actualReturnDate = convertStringToDate(actualReturnDateStr);

        ReadersCardDto readerBook = ReadersCardDto.Builder.builder()
                .dateOfIssue(dateOfIssue)
                .expectedReturnDate(expectedReturnDate)
                .actualReturnDate(actualReturnDate)
                .book(new BookDto(bookId))
                .reader(new UserDto(readerId))
                .librarian(new UserDto(librarianId))
                .build();

        Validator<ReadersCardDto> validator = new ReadersCardValidator();
        List<String> invalidFields = validator.validate(readerBook, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
            doGet(req, resp);
            return;
        }

        ReadersCardService readersCardService = new ReadersCardServiceImpl();
        ReadersCardDto readersCardDto = readersCardService.insertReaderBook(readerBook);
        resp.sendRedirect("/Library/readersCards?id=" + readersCardDto.getId());
    }

}
