package com.epam.web.servlet;

import com.epam.model.enums.SortingEnum;
import com.epam.service.BookService;
import com.epam.service.impl.BookServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.BookSearchDto;
import com.epam.web.dto.SortDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet for process search options on the main page
 */
@WebServlet("/search")
public class LibrarySearchServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        int currentPage = StringToNumberConverter.convertStringToInt(req.getParameter("currentPage"));
        currentPage = (currentPage <= -1) ? 0 : currentPage;

        String bookName = req.getParameter("bookName");
        String authorName = req.getParameter("authorName");
        String phName = req.getParameter("phName");
        SortDto sortDto = getSortingDto(req);

        BookService bookService = BookServiceImpl.getInstance();
        BookSearchDto bookSearchDto = bookService.findBookBySearchParameters(bookName, phName, authorName, sortDto, currentPage);

        int pagesCount = bookSearchDto.getPagesCount();
        List<Integer> pages = IntStream.range(0, pagesCount).boxed().collect(Collectors.toList());

        req.setAttribute("currentPage", currentPage >= pagesCount ? pagesCount - 1 : currentPage);
        req.setAttribute("bookName", bookName);
        req.setAttribute("authorName", authorName);
        req.setAttribute("phName", phName);
        req.setAttribute("pages", pages);

        req.setAttribute("books", bookSearchDto.getBookDtoList());
        req.getRequestDispatcher("templates/library.jsp").forward(req, resp);
    }

    private SortDto getSortingDto(HttpServletRequest req) {
        String sortingField = req.getParameter("sort");
        if (sortingField != null && !sortingField.isEmpty()) {
            String[] sortingValues = sortingField.split(":");
            req.setAttribute("sort", sortingField);
            SortingEnum sortingEnum = SortingEnum.valueOf(sortingValues[1]);
            return new SortDto(sortingValues[0], sortingEnum);
        }
        return null;
    }
}
