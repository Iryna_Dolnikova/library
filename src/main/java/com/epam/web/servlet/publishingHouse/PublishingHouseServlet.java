package com.epam.web.servlet.publishingHouse;

import com.epam.model.PublishingHouse;
import com.epam.service.PublishingHouseService;
import com.epam.service.impl.PublishingHouseServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setPublishingHouse;

/**
 * Servlet that processes GET request for getting all publishingHouses & publishingHouse by id
 */
@WebServlet("/publishingHouses")
public class PublishingHouseServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String phId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(phId);
        PublishingHouseService publishingHouseService = new PublishingHouseServiceImpl();

        if (id >= 1) {
            PublishingHouse pubHouse = publishingHouseService.findPubHouseById(id);
            req.setAttribute("publishingHouse", pubHouse);
            req.getRequestDispatcher("templates/publishingHouses/publishingHouse.jsp").forward(req, resp);
        } else {
            setPublishingHouse(req);
            req.getRequestDispatcher("templates/publishingHouses/publishingHouses.jsp").forward(req, resp);
        }
    }
}
