package com.epam.web.servlet.publishingHouse;

import com.epam.model.PublishingHouse;
import com.epam.service.PublishingHouseService;
import com.epam.service.impl.PublishingHouseServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCities;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request that return page for updating publishing house
 */
@WebServlet("/publishingHouses_edit")
public class PublishingHouseUpdatePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
       
        String phId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(phId);

        if (id >= 1) {
            PublishingHouseService publishingHouseService = new PublishingHouseServiceImpl();
            PublishingHouse publishingHouse = publishingHouseService.findPubHouseById(id);

            setCities(req);
            req.setAttribute("publishingHouse", publishingHouse);
            req.getRequestDispatcher("templates/publishingHouses/editPublishingHouse.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("templates/publishingHouses/publishingHouses.jsp").include(req, resp);
        }
    }
}

