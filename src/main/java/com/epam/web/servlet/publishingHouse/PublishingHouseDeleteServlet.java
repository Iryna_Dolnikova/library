package com.epam.web.servlet.publishingHouse;

import com.epam.service.PublishingHouseService;
import com.epam.service.impl.PublishingHouseServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for deleting publishingHouse by id
 */
@WebServlet("/publishingHouses_delete")
public class PublishingHouseDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String phId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(phId);

        if (id >= 1) {
            PublishingHouseService publishingHouseService = new PublishingHouseServiceImpl();
            publishingHouseService.deletePublishingHouse(id);
        }
        resp.sendRedirect("/Library/publishingHouses");
    }
}
