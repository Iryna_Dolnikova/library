package com.epam.web.servlet.publishingHouse;

import com.epam.model.City;
import com.epam.model.PublishingHouse;
import com.epam.service.PublishingHouseService;
import com.epam.service.impl.PublishingHouseServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.PublishingHouseValidator;
import com.epam.validator.Validator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setCities;

/**
 * Servlet that processes POST request for updating publishing house
 */
@WebServlet("/publishingHouse_update")
public class PublishingHouseUpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String phIdStr = req.getParameter("id");
        int phId = StringToNumberConverter.convertStringToInt(phIdStr);
        String cityIdStr = req.getParameter("cityId");
        int cityId = StringToNumberConverter.convertStringToInt(cityIdStr);

        PublishingHouse publishingHouseForUpdating = new PublishingHouse();
        publishingHouseForUpdating.setId(phId);
        publishingHouseForUpdating.setName(req.getParameter("name"));

        City city = new City(cityId);
        publishingHouseForUpdating.setCity(city);

        Validator<PublishingHouse> validator = new PublishingHouseValidator();
        List<String> invalidFields = validator.validate(publishingHouseForUpdating, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            setCities(req);
            req.setAttribute("publishingHouse", publishingHouseForUpdating);
            req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
            req.getRequestDispatcher("templates/publishingHouses/editPublishingHouse.jsp").forward(req, resp);
            return;
        }

        PublishingHouseService publishingHouseService = new PublishingHouseServiceImpl();
        PublishingHouse updatedPublishingHouse = publishingHouseService.updatePublishingHouse(publishingHouseForUpdating);

        resp.sendRedirect("/Library/publishingHouses?id=" + updatedPublishingHouse.getId());
    }

}
