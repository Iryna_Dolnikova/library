package com.epam.web.servlet.publishingHouse;

import com.epam.model.City;
import com.epam.model.PublishingHouse;
import com.epam.service.PublishingHouseService;
import com.epam.service.impl.PublishingHouseServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.PublishingHouseValidator;
import com.epam.validator.Validator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCities;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes 2 requests:
 * GET - return page for adding publishing house
 * POST - validate & save new publishing house in db
 */
@WebServlet("/publishingHouses_add")
public class PublishingHouseAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        setCities(req);
        req.getRequestDispatcher("templates/publishingHouses/addPublishingHouse.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        setCharacterEncoding(req, resp);

        String publishingHouseName = req.getParameter("name");
        int cityId = StringToNumberConverter.convertStringToInt(req.getParameter("cityId"));

        City city = new City(cityId);
        PublishingHouse publishingHouse = new PublishingHouse(publishingHouseName, city);

        Validator<PublishingHouse> validator = new PublishingHouseValidator();
        List<String> invalidFields = validator.validate(publishingHouse, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
            doGet(req, resp);
            return;
        }

        PublishingHouseService publishingHouseService = new PublishingHouseServiceImpl();
        publishingHouse = publishingHouseService.insertPublishingHouse(publishingHouse);
        resp.sendRedirect("/Library/publishingHouses?id=" + publishingHouse.getId());
    }
}
