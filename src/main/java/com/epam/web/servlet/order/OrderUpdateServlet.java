package com.epam.web.servlet.order;

import com.epam.service.OrderService;
import com.epam.service.UserService;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.OrderDtoValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.epam.util.StringToDateConverter.convertStringToDate;
import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setReaders;

/**
 * Servlet that processes POST request for updating readers books
 */
@WebServlet("/orders_update")
public class OrderUpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        int id = StringToNumberConverter.convertStringToInt(req.getParameter("id"));
        int bookId = StringToNumberConverter.convertStringToInt(req.getParameter("bookId"));
        int readerId = StringToNumberConverter.convertStringToInt(req.getParameter("readerId"));

        String dateOfIssueStr = req.getParameter("dateOfIssue");
        java.sql.Date dateOfIssue = convertStringToDate(dateOfIssueStr);

        String expectedReturnDateStr = req.getParameter("expectedReturnDate");
        java.sql.Date expectedReturnDate = convertStringToDate(expectedReturnDateStr);

        OrderDto orderDto = new OrderDto();
        orderDto.setId(id);
        orderDto.setDateOfIssue(dateOfIssue);
        orderDto.setExpectedReturnDate(expectedReturnDate);

        orderDto.setBook(new BookDto(bookId));
        orderDto.setReader(new UserDto(readerId));

        Validator<OrderDto> validator = new OrderDtoValidator();
        List<String> invalidFields = validator.validate(orderDto, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            setBooks(req);

            UserService userService = new UserServiceImpl();
            orderDto.setReader(userService.findReaderById(readerId));
            req.setAttribute("order", orderDto);
            req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
            req.getRequestDispatcher("templates/orders/editOrder.jsp").forward(req, resp);
            return;
        }

        OrderService orderService = new OrderServiceImpl();
        OrderDto updateOrder = orderService.updateOrder(orderDto);

        resp.sendRedirect("/Library/orders?id=" + updateOrder.getId());
    }

}
