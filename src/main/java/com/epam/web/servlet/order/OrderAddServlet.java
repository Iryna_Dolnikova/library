package com.epam.web.servlet.order;

import com.epam.service.OrderService;
import com.epam.service.UserService;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.OrderDtoValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.LoginDto;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.epam.config.SecurityConfig.LOGINED_USER;
import static com.epam.util.StringToDateConverter.convertStringToDate;
import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes 2 requests:
 * GET - return page for creating order
 * POST - validate & save new order in db
 */
@WebServlet("/orders_add")
public class OrderAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        setBooks(req);

        HttpSession httpSession = req.getSession();
        LoginDto loginDto = (LoginDto) httpSession.getAttribute(LOGINED_USER);

        UserService userService = new UserServiceImpl();
        req.setAttribute("reader", userService.findReaderById(loginDto.getId()));
        req.getRequestDispatcher("templates/orders/addOrder.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        int bookId = StringToNumberConverter.convertStringToInt(req.getParameter("bookId"));
        int readerId = StringToNumberConverter.convertStringToInt(req.getParameter("readerId"));

        String dateOfIssueStr = req.getParameter("dateOfIssue");
        java.sql.Date dateOfIssue = convertStringToDate(dateOfIssueStr);
        String expectedReturnDateStr = req.getParameter("expectedReturnDate");
        java.sql.Date expectedReturnDate = convertStringToDate(expectedReturnDateStr);

        OrderDto orderDto = new OrderDto();
        orderDto.setDateOfIssue(dateOfIssue);
        orderDto.setExpectedReturnDate(expectedReturnDate);

        orderDto.setBook(new BookDto(bookId));
        orderDto.setReader(new UserDto(readerId));

        Validator<OrderDto> validator = new OrderDtoValidator();
        List<String> invalidFields = validator.validate(orderDto, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
            doGet(req, resp);
            return;
        }

        OrderService orderService = new OrderServiceImpl();
        OrderDto createdOrder = orderService.insertOrder(orderDto);
        resp.sendRedirect("/Library/orders?id=" + createdOrder.getId());
    }

}
