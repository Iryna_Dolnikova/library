package com.epam.web.servlet.order;

import com.epam.model.enums.RolesEnum;
import com.epam.service.OrderService;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.LoginDto;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.OrderSearchDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.epam.config.SecurityConfig.LOGINED_USER;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for getting all orders & order by id
 */
@WebServlet("/orders")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String readerBookId = req.getParameter("id");
        String currentPageStr = req.getParameter("currentPage");
        int id = StringToNumberConverter.convertStringToInt(readerBookId);
        int currentPage = StringToNumberConverter.convertStringToInt(currentPageStr);
        currentPage = currentPage <= -1 ? 0 : currentPage;

        String readerName = req.getParameter("readerName");
        String bookName = req.getParameter("bookName");
        OrderService orderService = new OrderServiceImpl();

        if (id >= 1) {
            OrderDto order = orderService.findOrderById(id);
            req.setAttribute("order", order);
            req.getRequestDispatcher("templates/orders/order.jsp").forward(req, resp);
        } else {
            HttpSession session = req.getSession();
            LoginDto loginDto = (LoginDto) session.getAttribute(LOGINED_USER);
            OrderSearchDto orderSearchDto;
            if (loginDto.getRole().equals(RolesEnum.READER)) {
                orderSearchDto = orderService.findOrdersByReaderID(loginDto.getId(), bookName, readerName, currentPage);
            } else {
                orderSearchDto = orderService.findAllOrders(bookName, readerName, currentPage);
            }

            int pagesCount = orderSearchDto.getPagesCount();
            List<Integer> pages = IntStream.range(0, pagesCount).boxed().collect(Collectors.toList());

            req.setAttribute("orders", orderSearchDto.getOrders());
            req.setAttribute("readerName", readerName);
            req.setAttribute("bookName", bookName);
            req.setAttribute("pages", pages);
            req.setAttribute("currentPage", orderSearchDto.getCurrentPage());
            req.getRequestDispatcher("templates/orders/orders.jsp").forward(req, resp);
        }
    }
}
