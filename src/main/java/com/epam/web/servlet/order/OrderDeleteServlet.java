package com.epam.web.servlet.order;

import com.epam.service.OrderService;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for deleting order
 */
@WebServlet("/orders_delete")
public class OrderDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idSt = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(idSt);

        if (id >= 1) {
            OrderService orderService = new OrderServiceImpl();
            orderService.deleteOrder(id);
        }
        resp.sendRedirect("/Library/orders");
    }
}
