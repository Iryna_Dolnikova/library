package com.epam.web.servlet.order;

import com.epam.service.OrderService;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.LoginDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.config.SecurityConfig.LOGINED_USER;

/**
 * Servlet for deleting order
 */
@WebServlet("/orders_approve")
public class OrderApproveServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idSt = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(idSt);

        if (id >= 1) {
            HttpSession httpSession = req.getSession();
            LoginDto loginDto = (LoginDto) httpSession.getAttribute(LOGINED_USER);

            OrderService orderService = new OrderServiceImpl();
            orderService.approveOrder(id, loginDto.getId());
            resp.sendRedirect("/Library/readersCards?id=" + id);
        } else {
            resp.sendRedirect("/Library/orders");
        }
    }
}
