package com.epam.web.servlet.order;

import com.epam.service.OrderService;
import com.epam.service.ReadersCardService;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.service.impl.ReadersCardServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.ReadersCardDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;
import static com.epam.web.util.ServletRequestResponseConfig.setLibrarians;
import static com.epam.web.util.ServletRequestResponseConfig.setReaders;

/**
 * Servlet that processes GET request that return page for updating orders
 */
@WebServlet("/orders_edit")
public class OrderUpdatePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String rdId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(rdId);

        if (id >= 1) {
            OrderService orderService = new OrderServiceImpl();
            OrderDto orderById = orderService.findOrderById(id);
            req.setAttribute("order", orderById);
            req.setAttribute("dateOfIssue", orderById.getDateOfIssue());
            req.setAttribute("expectedReturnDate", orderById.getExpectedReturnDate());

            setBooks(req);
            req.getRequestDispatcher("templates/orders/editOrder.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("templates/orders/orders.jsp").include(req, resp);
        }
    }
}
