package com.epam.web.servlet.city;

import com.epam.model.City;
import com.epam.service.CityService;
import com.epam.service.impl.CityServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.CityValidator;
import com.epam.validator.Validator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes POST request for updating city
 */
@WebServlet("/cities_update")
public class CityUpdateServlet extends HttpServlet {

    private CityService cityService = new CityServiceImpl();
    private Validator<City> validator = new CityValidator();

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String cityId = req.getParameter("id");
        String cityName = req.getParameter("name");

        int id = StringToNumberConverter.convertStringToInt(cityId);
        City city = new City(id, cityName);

        List<String> invalidFields = validator.validate(city, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("city", city);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("templates/cities/editCity.jsp");
            requestDispatcher.forward(req, resp);
            return;
        }

        if (id > 0) {
            city = cityService.updateCity(cityName, id);
            req.setAttribute("city", city);
            req.getRequestDispatcher("templates/cities/city.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("templates/cities/cities.jsp").forward(req, resp);
        }
    }

    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }

    public void setValidator(Validator<City> validator) {
        this.validator = validator;
    }
}
