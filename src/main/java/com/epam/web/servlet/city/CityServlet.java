package com.epam.web.servlet.city;

import com.epam.model.City;
import com.epam.service.CityService;
import com.epam.service.impl.CityServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for getting all cities & city by id
 */
@WebServlet("/cities")
public class CityServlet extends HttpServlet {

    private CityService cityService = new CityServiceImpl();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String cityId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(cityId);

        if (id >= 1) {
            City city = cityService.findCityById(id);
            req.setAttribute("city", city);
            req.getRequestDispatcher("templates/cities/city.jsp").forward(req, resp);
        } else {
            List<City> cities = cityService.findAllCities();
            req.setAttribute("cities", cities);
            req.getRequestDispatcher("templates/cities/cities.jsp").forward(req, resp);
        }
    }

    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }
}
