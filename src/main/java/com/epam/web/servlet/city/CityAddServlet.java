package com.epam.web.servlet.city;

import com.epam.model.City;
import com.epam.service.CityService;
import com.epam.service.impl.CityServiceImpl;
import com.epam.validator.CityValidator;
import com.epam.validator.Validator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes 2 requests:
 * GET - return page for adding city
 * POST - validate & save new city in db
 */
@WebServlet("/cities_add")
public class CityAddServlet extends HttpServlet {

    private Validator<City> validator = new CityValidator();
    private CityService cityService = new CityServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        req.getRequestDispatcher("templates/cities/addCity.jsp").forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        setCharacterEncoding(req, resp);

        String cityName = req.getParameter("name");
        List<String> invalidFields = validator.validate(new City(cityName), req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            doGet(req, resp);
            return;
        }

        City city = cityService.insertCity(cityName);
        resp.sendRedirect("/Library/cities?id=" + city.getId());
    }

    public void setValidator(Validator<City> validator) {
        this.validator = validator;
    }

    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }
}
