package com.epam.web.servlet.city;

import com.epam.service.CityService;
import com.epam.service.impl.CityServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for deleting city by id
 */
@WebServlet("/cities_delete")
public class CityDeleteServlet extends HttpServlet {

    private CityService cityService = new CityServiceImpl();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String cityId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(cityId);

        if (id >= 1) {
            cityService.deleteCity(id);
        }
        resp.sendRedirect("/Library/cities");
    }

    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }
}
