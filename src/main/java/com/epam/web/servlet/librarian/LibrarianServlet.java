package com.epam.web.servlet.librarian;

import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setLibrarians;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for getting all librarians & librarian by id
 */
@WebServlet("/librarians")
public class LibrarianServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        UserService userService = new UserServiceImpl();
        String librarianId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(librarianId);

        if (id >= 1) {
            UserDto librarian = userService.findLibrarianById(id);
            req.setAttribute("librarian", librarian);
            req.getRequestDispatcher("templates/librarians/librarian.jsp").forward(req, resp);
        } else {
            setLibrarians(req);
            req.getRequestDispatcher("templates/librarians/librarians.jsp").forward(req, resp);
        }

    }
}
