package com.epam.web.servlet.librarian;

import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request that return page for updating librarian
 */
@WebServlet("/librarians_edit")
public class LibrarianUpdatePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String userId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(userId);

        if (id >= 1) {
            UserService userService = new UserServiceImpl();
            UserDto librarian = userService.findLibrarianById(id);
            req.setAttribute("librarian", librarian);
            req.getRequestDispatcher("templates/librarians/editLibrarian.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("templates/librarians/librarians.jsp").include(req, resp);
        }
    }

}
