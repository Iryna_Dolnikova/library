package com.epam.web.servlet.librarian;

import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.validator.UserValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes 2 requests:
 * GET - return page for adding librarian
 * POST - validate & save new librarian in db
 */
@WebServlet("/librarians_add")
public class LibrarianAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        req.getRequestDispatcher("templates/librarians/addLibrarian.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        setCharacterEncoding(req, resp);

        User librarian = new User();
        librarian.setSurname(req.getParameter("surname"));
        librarian.setFirstName(req.getParameter("firstName"));
        librarian.setPatronymic(req.getParameter("patronymic"));
        librarian.setEmail(req.getParameter("email"));
        librarian.setPhone(req.getParameter("phone"));
        librarian.setPassword(req.getParameter("password"));
        librarian.setRole(RolesEnum.LIBRARIAN);

        Validator<User> validator = new UserValidator();
        List<String> invalidFields = validator.validate(librarian, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            doGet(req, resp);
            return;
        }

        UserService userService = new UserServiceImpl();
        UserDto userDto = userService.insertUser(librarian);

        resp.sendRedirect("/Library/librarians?id=" + userDto.getId());
    }
}
