package com.epam.web.servlet.librarian;

import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for deleting librarian
 */
@WebServlet("/librarians_delete")
public class LibrarianDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(userId);

        if (id >= 0) {
            UserService userService = new UserServiceImpl();
            int idDeleteUser = userService.deleteLibrarian(id);
        }
        resp.sendRedirect("/Library/librarians");
    }
}