package com.epam.web.servlet.author;

import com.epam.model.Author;
import com.epam.service.AuthorService;
import com.epam.service.impl.AuthorServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.AuthorValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.AuthorDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes POST request for updating author
 */
@WebServlet("/authors_update")
public class AuthorUpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String authorIdStr = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(authorIdStr);

        if (id >= 1) {
            Author author = new Author();
            author.setId(id);
            author.setFirstName(req.getParameter("firstName"));
            author.setSurname(req.getParameter("surname"));
            author.setPatronymic(req.getParameter("patronymic"));

            String[] booksIds = req.getParameterValues("booksIds");
            List<Integer> booksIdList = booksIds == null ? new ArrayList<>() : Arrays.stream(booksIds)
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());

            Validator<Author> validator = new AuthorValidator();
            List<String> invalidFields = validator.validate(author, req);
            if (!invalidFields.isEmpty()) {
                String result = String.join(", ", invalidFields);
                setBooks(req);
                req.setAttribute("author", author);
                req.setAttribute("authorBooks", booksIdList);
                req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
                req.getRequestDispatcher("templates/authors/editAuthor.jsp").forward(req, resp);
                return;
            }

            AuthorService authorService = new AuthorServiceImpl();
            AuthorDto updateAuthor = authorService.updateAuthor(author, booksIdList);

            resp.sendRedirect("/Library/authors?id=" + updateAuthor.getId());
        }
    }
}
