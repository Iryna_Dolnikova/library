package com.epam.web.servlet.author;

import com.epam.service.AuthorService;
import com.epam.service.impl.AuthorServiceImpl;
import com.epam.util.StringToNumberConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for deleting author
 */
@WebServlet("/authors_delete")
public class AuthorDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String authorIdStr = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(authorIdStr);

        if (id >= 1) {
            AuthorService authorService = new AuthorServiceImpl();
            authorService.deleteAuthor(id);
        }
        resp.sendRedirect("/Library/authors");
    }
}
