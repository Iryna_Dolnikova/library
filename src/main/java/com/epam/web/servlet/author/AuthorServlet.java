package com.epam.web.servlet.author;

import com.epam.service.AuthorService;
import com.epam.service.impl.AuthorServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.AuthorDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.web.util.ServletRequestResponseConfig.setAuthors;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request for getting all authors & author by id
 */
@WebServlet("/authors")
public class AuthorServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String authorId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(authorId);
        AuthorService authorService = new AuthorServiceImpl();

        if (id >= 1) {
            AuthorDto author = authorService.findAuthorById(id);
            req.setAttribute("author", author);
            req.getRequestDispatcher("templates/authors/author.jsp").forward(req, resp);
        } else {
            setAuthors(req);
            req.getRequestDispatcher("templates/authors/authors.jsp").forward(req, resp);
        }
    }

}
