package com.epam.web.servlet.author;

import com.epam.service.AuthorService;
import com.epam.service.impl.AuthorServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.web.dto.AuthorDto;
import com.epam.web.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes GET request that return page for updating author
 */
@WebServlet("/authors_edit")
public class AuthorUpdatePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String authorId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(authorId);

        if (id >= 1) {
            AuthorService authorService = new AuthorServiceImpl();
            AuthorDto author = authorService.findAuthorById(id);
            setBooks(req);
            req.setAttribute("author", author);
            req.setAttribute("authorBooks", author.getBooks().stream().map(BookDto::getId).collect(Collectors.toList()));
            req.getRequestDispatcher("templates/authors/editAuthor.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("templates/authors/authors.jsp").include(req, resp);
        }
    }
}

