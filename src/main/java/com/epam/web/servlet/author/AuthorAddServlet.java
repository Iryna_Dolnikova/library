package com.epam.web.servlet.author;

import com.epam.model.Author;
import com.epam.service.AuthorService;
import com.epam.service.impl.AuthorServiceImpl;
import com.epam.validator.AuthorValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.AuthorDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.web.util.ServletRequestResponseConfig.setBooks;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes 2 requests:
 * GET - return page for adding author
 * POST - validate & save new author in db
 */
@WebServlet("/authors_add")
public class AuthorAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        setBooks(req);
        req.getRequestDispatcher("templates/authors/addAuthor.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        Author author = new Author();
        author.setFirstName(req.getParameter("firstName"));
        author.setSurname(req.getParameter("surname"));
        author.setPatronymic(req.getParameter("patronymic"));

        String[] booksIds = req.getParameterValues("booksIds");
        List<Integer> booksIdList = booksIds == null ? new ArrayList<>() : Arrays.stream(booksIds)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        Validator<Author> validator = new AuthorValidator();
        List<String> invalidFields = validator.validate(author, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. These fields are invalid: " + result);
            req.setAttribute("booksIds", booksIdList);
            doGet(req, resp);
            return;
        }

        AuthorService authorService = new AuthorServiceImpl();
        AuthorDto authorDto = authorService.insertAuthor(author, booksIdList);

        resp.sendRedirect("/Library/authors?id=" + authorDto.getId());
    }

}
