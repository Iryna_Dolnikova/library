package com.epam.web.servlet;

import com.epam.exception.BookAbsentException;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.DataBaseException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.exception.ReaderBlockedException;
import com.epam.exception.UserDoesNotExistException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.config.SecurityConfig.LOGINED_USER;

/**
 * Servlet that handle all exceptions and shows messages for users
 */
public class ErrorHandlingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Throwable throwable = (Throwable) req.getAttribute("javax.servlet.error.exception");
        String requestUri = (String) req.getAttribute("javax.servlet.error.request_uri");

        // when user's email or password on the login page were invalid
        if (throwable.getClass() == UserDoesNotExistException.class) {
            req.setAttribute("error", throwable.getMessage());
            req.getRequestDispatcher("templates/users/login.jsp").forward(req, resp);
            return;
        }

        // redirect on the page with all such elements
        if (throwable.getClass() == ObjectNotFoundByIDException.class) {
            sendRedirectWithError(req, resp, throwable, requestUri);
            return;
        }

        // we can get this exception while inserting new object => redirect on the page with all such objects
        // example: get this exception on the /authors_add request and redirect on the /authors page
        if (throwable.getClass() == ObjectAlreadyExistException.class
                || throwable.getClass() == BookAbsentException.class) {
            if (requestUri.contains("_add")) {
                requestUri = requestUri.substring(0, requestUri.length() - "_add".length());
            } else if (requestUri.contains("_approve")) {
                requestUri = requestUri.substring(0, requestUri.length() - "_approve".length());
            }
            sendRedirectWithError(req, resp, throwable, requestUri);
            return;
        }

        // we can get this exception while deleting any object => redirect on the page with all such objects
        // example: get this exception on the /authors_delete request and redirect on the /authors page
        if (throwable.getClass() == CannotDeleteException.class) {
            requestUri = requestUri.substring(0, requestUri.length() - "_delete".length());
            requestUri = requestUri.equals("/Library/books") ? "/Library/search" : requestUri;
            sendRedirectWithError(req, resp, throwable, requestUri);
            return;
        }

        // we can get this exception while try to insert/update readerCard for blocked reader =>
        // redirect on the page with all such objects
        if (throwable.getClass() == ReaderBlockedException.class) {
            if (requestUri.contains("_add")) {
                requestUri = requestUri.substring(0, requestUri.length() - "_add".length());
            } else if (requestUri.contains("_update")) {
                requestUri = requestUri.substring(0, requestUri.length() - "_update".length());
            } else if (requestUri.contains("_approve")) {
                requestUri = requestUri.substring(0, requestUri.length() - "_approve".length());
            }
            sendRedirectWithError(req, resp, throwable, requestUri);
            return;
        }

        // if user logged in - redirect on the main page, if not - on the login page
        if (throwable.getClass() == DataBaseException.class) {
            if (req.getSession() != null && req.getSession().getAttribute(LOGINED_USER) != null) {
                sendRedirectWithError(req, resp, throwable, "/Library/search");
                return;
            }
            sendRedirectWithError(req, resp, throwable, "/Library/");
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    private void sendRedirectWithError(HttpServletRequest req, HttpServletResponse resp,
                                       Throwable throwable, String requestUri) throws IOException {
        HttpSession session = req.getSession(true);
        session.setAttribute("error", throwable.getMessage());
        session.setAttribute("error_times", 0);
        resp.sendRedirect(requestUri.equalsIgnoreCase("/Library/books") ? "/Library/search" : requestUri);
    }

}
