package com.epam.web.servlet.user;

import com.epam.model.User;
import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.validator.LoginValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.LoginDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.epam.config.SecurityConfig.LOGINED_USER;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet for user's login
 */
@WebServlet("/")
public class UserLoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        req.getRequestDispatcher("templates/users/login.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        String email = req.getParameter("email");
        String password = req.getParameter("password");

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        Validator<User> validator = new LoginValidator();
        List<String> invalidFields = validator.validate(user, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            req.getRequestDispatcher("templates/users/login.jsp").forward(req, resp);
            return;
        }

        UserService userService = new UserServiceImpl();
        userService.loginUser(email, password);
        LoginDto loginDto = userService.loginUser(email, password);
        HttpSession session = req.getSession(true);
        session.setAttribute(LOGINED_USER, loginDto);

        resp.sendRedirect("/Library/search");
    }

}
