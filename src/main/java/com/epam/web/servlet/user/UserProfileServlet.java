package com.epam.web.servlet.user;

import com.epam.model.enums.RolesEnum;
import com.epam.service.ReadersCardService;
import com.epam.service.UserService;
import com.epam.service.impl.ReadersCardServiceImpl;
import com.epam.service.impl.UserServiceImpl;
import com.epam.web.dto.LoginDto;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.config.SecurityConfig.LOGINED_USER;
import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet returns user's profile
 */
@WebServlet("/profile")
public class UserProfileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        HttpSession session = req.getSession();

        LoginDto loginDto = (LoginDto) session.getAttribute(LOGINED_USER);
        int id = loginDto.getId();
        RolesEnum role = loginDto.getRole();

        ReadersCardService readersCardService = new ReadersCardServiceImpl();
        UserService userService = new UserServiceImpl();
        List<ReadersCardDto> readersCardDtoList = new ArrayList<>();
        UserDto currentUser;

        if (role.equals(RolesEnum.READER)) {
            currentUser = userService.findReaderById(id);
            readersCardDtoList = readersCardService.findReaderBookByReaderID(id);
        } else if (role.equals(RolesEnum.LIBRARIAN)) {
            currentUser = userService.findLibrarianById(id);
            readersCardDtoList = readersCardService.findReaderBookByLibrarianId(id);
        } else {
            currentUser = userService.findAdminById(id);
        }

        req.setAttribute("currentUser", currentUser);
        req.setAttribute("readersCardDtoList", readersCardDtoList);

        req.getRequestDispatcher("templates/users/profile.jsp").forward(req, resp);
    }
}
