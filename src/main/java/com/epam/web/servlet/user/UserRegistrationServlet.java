package com.epam.web.servlet.user;

import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.validator.UserValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet for user's registration
 */
@WebServlet("/registration")
public class UserRegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);
        req.getRequestDispatcher("templates/users/registration.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        User user = new User();
        user.setSurname(req.getParameter("surname"));
        user.setFirstName(req.getParameter("firstName"));
        user.setPatronymic(req.getParameter("patronymic"));
        user.setEmail(req.getParameter("email"));
        user.setPhone(req.getParameter("phone"));
        user.setPassword(req.getParameter("password"));

        Validator<User> validator = new UserValidator();
        List<String> invalidFields = validator.validate(user, req);
        if (req.getParameter("role") == null) {
            invalidFields.add("role");
        }
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            req.getRequestDispatcher("templates/users/registration.jsp").forward(req, resp);
            return;
        }

        user.setRole(RolesEnum.valueOf(req.getParameter("role")));
        UserService userService = new UserServiceImpl();
        UserDto userDto = userService.insertUser(user);

        resp.sendRedirect("/Library/search");
    }

}
