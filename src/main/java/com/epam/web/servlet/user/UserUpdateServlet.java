package com.epam.web.servlet.user;

import com.epam.model.User;
import com.epam.model.enums.RolesEnum;
import com.epam.service.UserService;
import com.epam.service.impl.UserServiceImpl;
import com.epam.util.StringToNumberConverter;
import com.epam.validator.UserForUpdateValidator;
import com.epam.validator.Validator;
import com.epam.web.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.web.util.ServletRequestResponseConfig.setCharacterEncoding;

/**
 * Servlet that processes POST request for updating users
 */
@WebServlet("/users_update")
public class UserUpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCharacterEncoding(req, resp);

        User user = new User();
        String userId = req.getParameter("id");
        int id = StringToNumberConverter.convertStringToInt(userId);
        String userRole = req.getParameter("role");

        user.setId(id);
        user.setFirstName(req.getParameter("firstName"));
        user.setSurname(req.getParameter("surname"));
        user.setPatronymic(req.getParameter("patronymic"));
        user.setEmail(req.getParameter("email"));
        user.setPhone(req.getParameter("phone"));
        user.setRole(RolesEnum.valueOf(userRole));

        String templateName = getTemplateName(userRole);
        String templateStartName = String.format("templates/%s", templateName) + "/%s.jsp";
        String userAttributeName = templateName.substring(0, templateName.length() - 1);

        Validator<User> validator = new UserForUpdateValidator();
        List<String> invalidFields = validator.validate(user, req);
        if (!invalidFields.isEmpty()) {
            String result = String.join(", ", invalidFields);
            req.setAttribute(userAttributeName, user);
            req.setAttribute("error", "Verify your input. This field is invalid: " + result);
            req.getRequestDispatcher(String.format(templateStartName, getEditTemplateName(userRole))).forward(req, resp);
            return;
        }

        UserService userService = new UserServiceImpl();
        UserDto userDto = userService.updateUser(user, id);

        req.setAttribute(userAttributeName, userDto);
        req.getRequestDispatcher(String.format(templateStartName, userAttributeName)).forward(req, resp);
    }

    private String getTemplateName(String role) {
        return role.equals(RolesEnum.READER.name()) ? "readers" : "librarians";
    }

    private String getEditTemplateName(String role) {
        return role.equals(RolesEnum.READER.name()) ? "editReader" : "editLibrarian";
    }

}
