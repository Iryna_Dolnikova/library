package com.epam.web.converter;

import com.epam.model.User;
import com.epam.web.dto.UserDto;

import java.util.List;
import java.util.stream.Collectors;

import static com.epam.web.converter.NameConverter.getFullName;

public class UserConverter {

    private static UserConverter instance;

    private UserConverter() {
    }

    public static synchronized UserConverter getInstance() {
        if (instance == null) {
            instance = new UserConverter();
        }

        return instance;
    }

    public List<UserDto> toUserDtoList(List<User> users) {
        return users.stream()
                .map(this::toUserDto)
                .collect(Collectors.toList());
    }

    public UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setSurname(user.getSurname());
        userDto.setPatronymic(user.getPatronymic());
        userDto.setEmail(user.getEmail());
        userDto.setPhone(user.getPhone());
        userDto.setRole(user.getRole());
        userDto.setFullName(getFullName(userDto.getSurname(), userDto.getFirstName(), userDto.getPatronymic()));
        userDto.setBlocked(user.isBlocked());
        userDto.setActive(user.isActive());
        return userDto;
    }

}
