package com.epam.web.converter;

import com.epam.model.ReaderCard;
import com.epam.web.dto.ReadersCardDto;

import java.util.List;
import java.util.stream.Collectors;

public class ReadersBookConverter {

    public ReadersCardDto toReaderBookDto(ReaderCard readerCard) {
        return ReadersCardDto.Builder.builder()
                .id(readerCard.getId())
                .dateOfIssue(readerCard.getDateOfIssue())
                .expectedReturnDate(readerCard.getExpectedReturnDate())
                .actualReturnDate(readerCard.getActualReturnDate())
                .book(BookConverter.getInstance().toBookDto(readerCard.getBook()))
                .reader(UserConverter.getInstance().toUserDto(readerCard.getReader()))
                .librarian(UserConverter.getInstance().toUserDto(readerCard.getLibrarian()))
                .build();
    }

    public List<ReadersCardDto> toListReaderBookDto(List<ReaderCard> readersBooks) {
        return readersBooks.stream()
                .map(readerBook -> toReaderBookDto(readerBook))
                .collect(Collectors.toList());
    }
}
