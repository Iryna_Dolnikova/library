package com.epam.web.converter;

import com.epam.model.Author;
import com.epam.web.dto.AuthorDto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.web.converter.NameConverter.getFullName;

public class AuthorConverter {

    private static AuthorConverter instance;

    public static synchronized AuthorConverter getInstance() {
        if (instance == null) {
            instance = new AuthorConverter();
        }

        return instance;
    }

    public List<AuthorDto> toAuthorDtoList(List<Author> authors) {
        return authors.stream()
                .map(this::toAuthorDto)
                .collect(Collectors.toList());
    }

    public AuthorDto toAuthorDto(Author author) {
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(author.getId());
        authorDto.setFirstName(author.getFirstName());
        authorDto.setSurname(author.getSurname());
        authorDto.setPatronymic(author.getPatronymic());
        authorDto.setFullName(getFullName(authorDto.getSurname(), authorDto.getFirstName(), authorDto.getPatronymic()));
        authorDto.setBooks(new ArrayList<>());
        return authorDto;
    }

}
