package com.epam.web.converter;

public class NameConverter {

    public static String getFullName(String surname, String firstName, String patronymic) {
        String fullName = surname;
        if (firstName != null && !firstName.isEmpty()) {
            fullName += " " + firstName.charAt(0) + ".";
        }
        if (patronymic != null && !patronymic.isEmpty()) {
            fullName += patronymic.charAt(0) + ".";
        }
        return fullName;
    }

}
