package com.epam.web.converter;

import com.epam.model.Book;
import com.epam.web.dto.BookDto;

import java.util.List;
import java.util.stream.Collectors;

public class BookConverter {

    private BookConverter() {
    }

    private static BookConverter instance;

    public static synchronized BookConverter getInstance() {
        if (instance == null) {
            instance = new BookConverter();
        }

        return instance;
    }

    public List<BookDto> toBookDtoList(List<Book> books) {
        return books.stream()
                .map(this::toBookDto)
                .collect(Collectors.toList());
    }

    public BookDto toBookDto(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());
        bookDto.setPublishingHouse(book.getPublishingHouse());
        bookDto.setIssueYear(book.getIssueYear());
        bookDto.setCount(book.getCount());
        return bookDto;
    }

}
