package com.epam.service;

import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.ReadersCardSearchDto;

import java.util.List;

/**
 * Class contains CRUD logic for readers cards
 */
public interface ReadersCardService {

    ReadersCardDto findReadersBooksById(int id);

    ReadersCardSearchDto findAllReadersBooks(String bookName, String readerName, String librarianName, int currentPage);

    ReadersCardDto insertReaderBook(ReadersCardDto readerBook);

    ReadersCardDto updateReaderBook(ReadersCardDto readerBook);

    int deleteReaderBook(int id);

    int penaltyForLateReturn(ReadersCardDto readerBook);

    List<ReadersCardDto> findReaderBookByLibrarianId(int id);

    List<ReadersCardDto> findReaderBookByReaderID(int id);

}
