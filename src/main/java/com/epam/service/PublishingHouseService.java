package com.epam.service;

import com.epam.model.PublishingHouse;

import java.util.List;

/**
 * Class contains CRUD logic for publishing houses
 */
public interface PublishingHouseService {

    PublishingHouse findPubHouseById(int id);

    List<PublishingHouse> findAll();

    PublishingHouse insertPublishingHouse(PublishingHouse publishingHouse);

    PublishingHouse updatePublishingHouse(PublishingHouse publishingHouse);

    int deletePublishingHouse(int id);
}
