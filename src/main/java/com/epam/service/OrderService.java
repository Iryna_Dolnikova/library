package com.epam.service;

import com.epam.web.dto.OrderDto;
import com.epam.web.dto.OrderSearchDto;

/**
 * Class contains CRUD logic for orders
 */
public interface OrderService {

    OrderDto findOrderById(int id);

    OrderSearchDto findOrdersByReaderID(int readerId, String bookName, String readerName, int currentPage);

    OrderSearchDto findAllOrders(String bookName, String readerName, int currentPage);

    OrderDto insertOrder(OrderDto orderDto);

    OrderDto updateOrder(OrderDto orderDto);

    void approveOrder(int orderId, int librarianId);

    int deleteOrder(int id);

}
