package com.epam.service;

import com.epam.web.dto.BookDto;
import com.epam.web.dto.BookSearchDto;
import com.epam.web.dto.SortDto;

import java.util.List;

/**
 * Class contains CRUD logic for books
 */
public interface BookService {

    BookDto findBookById(int id);

    List<BookDto> findAll();

    BookSearchDto findBookBySearchParameters(String bookName, String phName, String authorName,
                                             SortDto sortDto, int currentPage);

    BookDto insertBook(BookDto bookDto, List<Integer> authorIdList);

    BookDto updateBook(BookDto bookDto, int id);

    BookDto updateBook(BookDto bookDto, int id, List<Integer> authorsId);

    int deleteBook(int id);
}
