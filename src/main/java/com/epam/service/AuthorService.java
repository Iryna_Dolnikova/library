package com.epam.service;

import com.epam.model.Author;
import com.epam.web.dto.AuthorDto;

import java.util.List;

/**
 * Class contains CRUD logic for authors
 */
public interface AuthorService {

    AuthorDto findAuthorById(int id);

    List<AuthorDto> findAuthorByName(String name);

    List<AuthorDto> findAll();

    AuthorDto insertAuthor(Author author, List<Integer> booksIdList);

    AuthorDto updateAuthor(Author author, List<Integer> booksIdList);

    int deleteAuthor(int id);

}
