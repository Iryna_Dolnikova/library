package com.epam.service.impl;

import com.epam.dao.ReadersCardDao;
import com.epam.dao.UsersDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.exception.UserDoesNotExistException;
import com.epam.model.ReaderCard;
import com.epam.model.User;
import com.epam.service.UserService;
import com.epam.web.converter.UserConverter;
import com.epam.web.dto.LoginDto;
import com.epam.web.dto.UserDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
    private final UsersDao usersDao;
    private final UserConverter userConverter;
    private final ReadersCardDao readersCardDao;

    public UserServiceImpl() {
        this.usersDao = UsersDao.getInstance();
        this.userConverter = UserConverter.getInstance();
        this.readersCardDao = ReadersCardDao.getInstance();
    }

    public UserServiceImpl(UsersDao usersDao, UserConverter userConverter, ReadersCardDao readersCardDao) {
        this.usersDao = usersDao;
        this.userConverter = userConverter;
        this.readersCardDao = readersCardDao;
    }

    @Override
    public UserDto findReaderById(int id) {
        User user = usersDao.findActiveReaderById(id)
                .orElseThrow(() -> {
                    logger.warn("Reader with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Reader does not exist");
                });
        return userConverter.toUserDto(user);
    }

    @Override
    public UserDto findLibrarianById(int id) {
        User user = usersDao.findActiveLibrarianById(id)
                .orElseThrow(() -> {
                    logger.warn("Librarian with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Librarian does not exist");
                });
        return userConverter.toUserDto(user);
    }

    @Override
    public UserDto findAdminById(int id) {
        User user = usersDao.findAdminById(id)
                .orElseThrow(() -> {
                    logger.warn("Admin with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Admin does not exist");
                });
        return userConverter.toUserDto(user);
    }

    @Override
    public List<UserDto> findAllReaders() {
        List<User> allReaders = usersDao.findAllReaders();
        return userConverter.toUserDtoList(allReaders);
    }

    @Override
    public List<UserDto> findAllLibrarians() {
        List<User> allLibrarians = usersDao.findAllLibrarian();
        return userConverter.toUserDtoList(allLibrarians);
    }

    @Override
    public UserDto insertUser(User user) {
        if (usersDao.existUserByPhoneOrEmail(user)) {
            logger.warn("User {} already exists", user.getSurname());
            throw new ObjectAlreadyExistException("Sorry, user already exists");
        }
        return userConverter.toUserDto(usersDao.insertUser(user));
    }

    @Override
    public UserDto updateUser(User user, int id) {
        return userConverter.toUserDto(usersDao.updateUser(user, id));
    }

    @Override
    public LoginDto loginUser(String email, String password) {
        return usersDao.getLoginDtoByEmailAndPassword(email, password).orElseThrow(() -> {
            logger.warn("User with email {} does not exist", email);
            return new UserDoesNotExistException("User with such email or password does not exist");
        });
    }

    @Override
    public UserDto blockUnblockReader(int id) {
        UserDto userDto = findReaderById(id);
        boolean isReaderBlock = userDto.isBlocked();
        usersDao.updateReaderBlock(userDto.getId(), !isReaderBlock);
        userDto.setBlocked(!isReaderBlock);
        return userDto;
    }

    @Override
    public int deleteReader(int id) {
        findReaderById(id);
        List<ReaderCard> readerBooks = readersCardDao.findReaderBookByReaderId(id);
        if (readerBooks.stream().anyMatch(readerCard -> readerCard.getActualReturnDate() == null)) {
            logger.warn("Reader {} cannot be deleted because he does not return books", id);
            throw new CannotDeleteException("Reader cannot be deleted because he does not return books");
        }
        return usersDao.deleteUser(id);
    }

    @Override
    public int deleteLibrarian(int id) {
        findLibrarianById(id);
        return usersDao.deleteUser(id);
    }

}
