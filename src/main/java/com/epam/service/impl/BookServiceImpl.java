package com.epam.service.impl;

import com.epam.dao.AuthorsBooksDao;
import com.epam.dao.BooksDao;
import com.epam.dao.ReadersCardDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.Book;
import com.epam.model.PublishingHouse;
import com.epam.model.enums.SortingEnum;
import com.epam.service.AuthorService;
import com.epam.service.BookService;
import com.epam.service.PublishingHouseService;
import com.epam.web.converter.AuthorConverter;
import com.epam.web.converter.BookConverter;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.BookSearchDto;
import com.epam.web.dto.SortDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BookServiceImpl implements BookService {
    public static final int MAX_ELEMENT_PER_PAGE = 5;
    private static final Logger logger = LogManager.getLogger(BookServiceImpl.class);
    private AuthorService authorService;

    private final BooksDao booksDao;
    private final ReadersCardDao readersCardDao;
    private final AuthorsBooksDao authorsBooksDao;
    private final BookConverter bookConverter;
    private final AuthorConverter authorConverter;
    private final PublishingHouseService publishingHouseService;

    private static BookServiceImpl instance;

    public static synchronized BookServiceImpl getInstance() {
        if (instance == null) {
            instance = new BookServiceImpl();
        }

        return instance;
    }

    private BookServiceImpl() {
        this.booksDao = BooksDao.getInstance();
        this.readersCardDao = ReadersCardDao.getInstance();
        this.bookConverter = BookConverter.getInstance();
        this.authorConverter = AuthorConverter.getInstance();
        this.publishingHouseService = new PublishingHouseServiceImpl();
        this.authorsBooksDao = AuthorsBooksDao.getInstance();
        this.authorService = new AuthorServiceImpl();
    }

    public BookServiceImpl(BooksDao booksDao, BookConverter bookConverter, AuthorConverter authorConverter,
                           PublishingHouseService publishingHouseService,
                           AuthorsBooksDao authorsBooksDao, ReadersCardDao readersCardDao) {
        this.booksDao = booksDao;
        this.bookConverter = bookConverter;
        this.authorConverter = authorConverter;
        this.publishingHouseService = publishingHouseService;
        this.authorsBooksDao = authorsBooksDao;
        this.readersCardDao = readersCardDao;
    }

    public BookServiceImpl(BooksDao booksDao, BookConverter bookConverter, AuthorConverter authorConverter,
                           PublishingHouseService publishingHouseService,
                           AuthorService authorService, AuthorsBooksDao authorsBooksDao, ReadersCardDao readersCardDao) {
        this.booksDao = booksDao;
        this.bookConverter = bookConverter;
        this.authorConverter = authorConverter;
        this.publishingHouseService = publishingHouseService;
        this.authorService = authorService;
        this.authorsBooksDao = authorsBooksDao;
        this.readersCardDao = readersCardDao;
    }

    @Override
    public BookDto findBookById(int id) {
        Book book = getBookById(id);
        BookDto bookDto = bookConverter.toBookDto(book);
        bookDto.setAuthors(authorConverter.toAuthorDtoList(book.getAuthors()));
        return bookDto;
    }

    @Override
    public List<BookDto> findAll() {
        List<Book> books = booksDao.findAllBooks();
        List<BookDto> bookDtos = bookConverter.toBookDtoList(books);
        for (int i = 0; i < bookDtos.size(); i++) {
            BookDto currentBookDto = bookDtos.get(i);
            currentBookDto.setAuthors(authorConverter.toAuthorDtoList(books.get(i).getAuthors()));
        }
        return bookDtos;
    }


    @Override
    public BookSearchDto findBookBySearchParameters(String bookName, String phName, String authorName, SortDto sortDto,
                                                    int currentPage) {
        List<Book> books = booksDao.findBookBySearchParameters(bookName, phName, authorName);
        if (books.isEmpty()) {
            return new BookSearchDto(new ArrayList<>(), 0);
        }

        List<BookDto> bookDtoList = bookConverter.toBookDtoList(books);
        for (int i = 0; i < bookDtoList.size(); i++) {
            BookDto currentBookDto = bookDtoList.get(i);
            currentBookDto.setAuthors(authorConverter.toAuthorDtoList(books.get(i).getAuthors()));
        }

        if (sortDto != null && sortDto.getFieldName().equals("count")) {
            bookDtoList = sortBooksByComparator(bookDtoList, sortDto, Comparator.comparing(BookDto::getCount));
        } else if (sortDto != null && sortDto.getFieldName().equals("issuerYear")) {
            bookDtoList = sortBooksByComparator(bookDtoList, sortDto, Comparator.comparing(BookDto::getIssueYear));
        }

        int pagesCount = (int) Math.ceil((double) books.size() / MAX_ELEMENT_PER_PAGE);

        currentPage = currentPage >= pagesCount ? pagesCount - 1 : currentPage;
        int indexFrom = currentPage * MAX_ELEMENT_PER_PAGE;
        int indexTo = indexFrom + MAX_ELEMENT_PER_PAGE;
        bookDtoList = bookDtoList.subList(indexFrom, Math.min(bookDtoList.size(), indexTo));

        return new BookSearchDto(bookDtoList, pagesCount);
    }

    @Override
    public BookDto insertBook(BookDto bookDto, List<Integer> authorIdList) {
        PublishingHouse publishingHouse = bookDto.getPublishingHouse();
        publishingHouseService.findPubHouseById(publishingHouse.getId());

        BookDto insertBook = booksDao.insertBook(bookDto);
        authorIdList.forEach(authorId -> {
            authorService.findAuthorById(authorId);
            authorsBooksDao.authorAndBookAddById(authorId, insertBook.getId());
        });
        return insertBook;
    }

    @Override
    public BookDto updateBook(BookDto bookDto, int id) {
        getBookById(id);
        publishingHouseService.findPubHouseById(bookDto.getPublishingHouse().getId());
        return booksDao.updateBook(bookDto, id);
    }

    @Override
    public BookDto updateBook(BookDto bookDto, int id, List<Integer> authorsId) {
        getBookById(id);
        publishingHouseService.findPubHouseById(bookDto.getPublishingHouse().getId());

        authorsBooksDao.deleteBookFromAuthorsBook(id);
        BookDto updatedBook = booksDao.updateBook(bookDto, id);
        authorsId.forEach(authorId -> {
            authorService.findAuthorById(authorId);
            authorsBooksDao.authorAndBookAddById(authorId, updatedBook.getId());
        });
        return updatedBook;
    }

    @Override
    public int deleteBook(int id) {
        findBookById(id);
        if (!readersCardDao.findReaderBookByBookId(id).isEmpty()) {
            logger.warn("Book {} cannot be deleted because it was taken by some readers", id);
            throw new CannotDeleteException("Book cannot be deleted because was taken by some readers");
        }
        authorsBooksDao.deleteBookFromAuthorsBook(id);
        return booksDao.deleteBook(id);
    }

    private Book getBookById(int id) {
        return booksDao.findBookById(id)
                .orElseThrow(() -> {
                    logger.warn("Book with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Book does not exist");
                });
    }

    private List<BookDto> sortBooksByComparator(List<BookDto> books, SortDto sortDto, Comparator<BookDto> comparing) {
        if (sortDto.getSortingEnum().equals(SortingEnum.ASC)) {
            books = books.stream()
                    .sorted(comparing)
                    .collect(Collectors.toList());
        } else if (sortDto.getSortingEnum().equals(SortingEnum.DESC)) {
            books = books.stream()
                    .sorted(comparing.reversed())
                    .collect(Collectors.toList());
        }
        return books;
    }

}

