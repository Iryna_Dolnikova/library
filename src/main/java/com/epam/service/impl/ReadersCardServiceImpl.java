package com.epam.service.impl;

import com.epam.dao.ReadersCardDao;
import com.epam.exception.BookAbsentException;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.exception.ReaderBlockedException;
import com.epam.model.ReaderCard;
import com.epam.service.BookService;
import com.epam.service.ReadersCardService;
import com.epam.service.UserService;
import com.epam.web.converter.ReadersBookConverter;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.ReadersCardDto;
import com.epam.web.dto.ReadersCardSearchDto;
import com.epam.web.dto.UserDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

public class ReadersCardServiceImpl implements ReadersCardService {

    private static final Logger logger = LogManager.getLogger(ReadersCardServiceImpl.class);
    private static final int AMOUNT_OF_THE_FINE_FOR_DAY = 5;

    public static final int MAX_ELEMENT_PER_PAGE = 5;
    private final ReadersCardDao readersCardDao;

    private final ReadersBookConverter readersBookConverter;

    private final UserService userService;
    private final BookService bookService;

    public ReadersCardServiceImpl() {
        this.readersCardDao = ReadersCardDao.getInstance();
        this.readersBookConverter = new ReadersBookConverter();
        this.userService = new UserServiceImpl();
        this.bookService = BookServiceImpl.getInstance();
    }

    public ReadersCardServiceImpl(ReadersCardDao readersCardDao, ReadersBookConverter readersBookConverter, UserService userService, BookService bookService) {
        this.readersCardDao = readersCardDao;
        this.readersBookConverter = readersBookConverter;
        this.userService = userService;
        this.bookService = bookService;
    }

    @Override
    public ReadersCardDto findReadersBooksById(int id) {
        ReaderCard readerCard = getReaderCardById(id);
        ReadersCardDto readersCardDto = readersBookConverter.toReaderBookDto(readerCard);
        readersCardDto.setPenalty(penaltyForLateReturn(readersCardDto));
        return readersCardDto;
    }

    @Override
    public ReadersCardSearchDto findAllReadersBooks(String bookName, String readerName, String librarianName,
                                                    int currentPage) {
        List<ReaderCard> readerCards = readersCardDao.findReadersBooksBySearchParameter(bookName);

        readerCards = readerCards.stream()
                .filter(readerBook -> readerBook.getReader().getSurname().toLowerCase()
                        .contains(readerName != null ? readerName.toLowerCase() : ""))
                .filter(readerBook -> readerBook.getLibrarian().getSurname().toLowerCase()
                        .contains(librarianName != null ? librarianName.toLowerCase() : ""))
                .collect(Collectors.toList());

        if (readerCards.isEmpty()) {
            return new ReadersCardSearchDto(new ArrayList<>(), 0);
        }

        int pagesCount = (int) Math.ceil((double) readerCards.size() / MAX_ELEMENT_PER_PAGE);
        currentPage = currentPage >= pagesCount ? pagesCount - 1 : currentPage;
        int indexFrom = currentPage * MAX_ELEMENT_PER_PAGE;
        int indexTo = indexFrom + MAX_ELEMENT_PER_PAGE;
        readerCards = readerCards.subList(indexFrom, Math.min(readerCards.size(), indexTo));

        List<ReadersCardDto> readersCardDto = readersBookConverter.toListReaderBookDto(readerCards);
        readersCardDto.forEach(rbDto -> rbDto.setPenalty(penaltyForLateReturn(rbDto)));

        return new ReadersCardSearchDto(readersCardDto, pagesCount);
    }

    @Override
    public ReadersCardDto insertReaderBook(ReadersCardDto readerBook) {
        BookDto bookById = validateReaderBook(readerBook);
        int count = bookById.getCount();
        if (count <= 0) {
            logger.warn("Book {} is absent", bookById.getId());
            throw new BookAbsentException("Sorry, book is absent");
        }

        bookById.setCount(count - 1);
        bookService.updateBook(bookById, bookById.getId());
        return readersCardDao.insertReaderBook(readerBook);
    }

    @Override
    public ReadersCardDto updateReaderBook(ReadersCardDto updatedReaderCard) {
        BookDto updatedBook = validateReaderBook(updatedReaderCard);
        ReadersCardDto readersCardDtoFromDb = findReadersBooksById(updatedReaderCard.getId());
        BookDto bookFromDataBase = readersCardDtoFromDb.getBook();

        if (updatedBook.getId() != bookFromDataBase.getId()) {
            if (updatedBook.getCount() <= 0) {
                logger.warn("Book {} is absent", updatedBook.getId());
                throw new BookAbsentException("Sorry, book is absent");
            }
            updatedBook.setCount(updatedBook.getCount() - 1);
            bookService.updateBook(updatedBook, updatedBook.getId());

            bookFromDataBase.setCount(bookFromDataBase.getCount() + 1);
            bookService.updateBook(bookFromDataBase, bookFromDataBase.getId());
        }

        if (updatedReaderCard.getActualReturnDate() != null && readersCardDtoFromDb.getActualReturnDate() == null) {
            bookFromDataBase.setCount(bookFromDataBase.getCount() + 1);
            bookService.updateBook(bookFromDataBase, bookFromDataBase.getId());
        }

        return readersCardDao.updateReaderBook(updatedReaderCard);
    }

    @Override
    public int deleteReaderBook(int id) {
        ReaderCard readersBooksById = getReaderCardById(id);
        if (readersBooksById.getActualReturnDate() == null) {
            logger.warn("Readers card {} cannot be deleted because reader has not return it yet", id);
            throw new CannotDeleteException("Readers card cannot be deleted because reader has not return it yet");
        }
        return readersCardDao.deleteReaderBook(id);
    }


    @Override
    public int penaltyForLateReturn(ReadersCardDto readerBook) {
        LocalDate expectedDate = readerBook.getExpectedReturnDate().toLocalDate();
        Date actualReturnDateSql = readerBook.getActualReturnDate();

        LocalDate actualDate = LocalDate.now();
        if (actualReturnDateSql != null) {
            actualDate = actualReturnDateSql.toLocalDate();
        }

        if (actualDate.isAfter(expectedDate)) {
            int daysBetween = (int) DAYS.between(expectedDate, actualDate);
            return AMOUNT_OF_THE_FINE_FOR_DAY * daysBetween;
        }
        return 0;
    }

    @Override
    public List<ReadersCardDto> findReaderBookByLibrarianId(int id) {
        List<ReaderCard> readerCardList = readersCardDao.findReaderBookByLibrarianId(id);
        List<ReadersCardDto> readersCardDto = readersBookConverter.toListReaderBookDto(readerCardList);
        readersCardDto.forEach(readerBooks -> readerBooks.setPenalty(penaltyForLateReturn(readerBooks)));
        return readersCardDto;
    }

    @Override
    public List<ReadersCardDto> findReaderBookByReaderID(int id) {
        List<ReaderCard> readerCardList = readersCardDao.findReaderBookByReaderId(id);
        List<ReadersCardDto> readersCardDto = readersBookConverter.toListReaderBookDto(readerCardList);
        readersCardDto.forEach(readerBook -> readerBook.setPenalty(penaltyForLateReturn(readerBook)));
        return readersCardDto;
    }

    private BookDto validateReaderBook(ReadersCardDto readerBook) {
        UserDto reader = userService.findReaderById(readerBook.getReader().getId());
        if (reader.isBlocked()) {
            logger.warn("Reader {} is blocked", reader.getSurname());
            throw new ReaderBlockedException("Sorry, reader is blocked");
        }
        UserDto librarian = readerBook.getLibrarian();
        userService.findLibrarianById(librarian.getId());

        BookDto book = readerBook.getBook();
        return bookService.findBookById(book.getId());
    }

    private ReaderCard getReaderCardById(int id) {
        return readersCardDao.findReaderBookById(id)
                .orElseThrow(() -> {
                    logger.warn("Readers Book with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Readers Book does not exist");
                });
    }
}