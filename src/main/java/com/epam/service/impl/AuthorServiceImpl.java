package com.epam.service.impl;

import com.epam.dao.AuthorsBooksDao;
import com.epam.dao.AuthorsDao;
import com.epam.dao.BooksDao;
import com.epam.dao.ReadersCardDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.Author;
import com.epam.service.AuthorService;
import com.epam.service.BookService;
import com.epam.web.converter.AuthorConverter;
import com.epam.web.converter.BookConverter;
import com.epam.web.dto.AuthorDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class AuthorServiceImpl implements AuthorService {

    private static final Logger logger = LogManager.getLogger(AuthorServiceImpl.class);
    private final AuthorsDao authorsDao;
    private final BookService bookService;
    private final BookConverter bookConverter;
    private final AuthorConverter authorConverter;

    private final AuthorsBooksDao authorsBooksDao;

    public AuthorServiceImpl() {
        this.authorsDao = AuthorsDao.getInstance();
        this.authorsBooksDao = AuthorsBooksDao.getInstance();
        this.bookConverter = BookConverter.getInstance();
        this.authorConverter = AuthorConverter.getInstance();
        this.bookService = new BookServiceImpl(BooksDao.getInstance(), BookConverter.getInstance(),
                AuthorConverter.getInstance(), new PublishingHouseServiceImpl(), this.authorsBooksDao, ReadersCardDao.getInstance());
    }

    public AuthorServiceImpl(AuthorsDao authorsDao, AuthorConverter authorConverter, BookConverter bookConverter,
                             BookService bookService, AuthorsBooksDao authorsBooksDao) {
        this.authorsDao = authorsDao;
        this.authorConverter = authorConverter;
        this.bookConverter = bookConverter;
        this.bookService = bookService;
        this.authorsBooksDao = authorsBooksDao;
    }

    @Override
    public AuthorDto findAuthorById(int id) {
        Author author = authorsDao.findAuthorById(id)
                .orElseThrow(() -> {
                    logger.warn("Author with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Author does not exist");
                });
        AuthorDto authorDto = authorConverter.toAuthorDto(author);
        authorDto.setBooks(bookConverter.toBookDtoList(author.getBooks()));
        return authorDto;
    }

    @Override
    public List<AuthorDto> findAuthorByName(String name) {
        List<Author> authors = authorsDao.findAuthorsByName(name);
        List<AuthorDto> authorDtos = authorConverter.toAuthorDtoList(authors);
        for (int i = 0; i < authorDtos.size(); i++) {
            AuthorDto currentAuthorDto = authorDtos.get(i);
            currentAuthorDto.setBooks(bookConverter.toBookDtoList(authors.get(i).getBooks()));
        }
        return authorDtos;
    }

    @Override
    public List<AuthorDto> findAll() {
        List<Author> authors = authorsDao.findAllAuthors();
        List<AuthorDto> authorDtos = authorConverter.toAuthorDtoList(authors);
        for (int i = 0; i < authorDtos.size(); i++) {
            AuthorDto currentAuthorDto = authorDtos.get(i);
            currentAuthorDto.setBooks(bookConverter.toBookDtoList(authors.get(i).getBooks()));
        }
        return authorDtos;
    }

    @Override
    public AuthorDto insertAuthor(Author author, List<Integer> booksIdList) {
        if (authorsDao.existAuthorByName(author.getFirstName().trim(), author.getSurname().trim(), author.getPatronymic().trim())) {
            logger.warn("Author {} already exists", author.getSurname());
            throw new ObjectAlreadyExistException("Sorry, author already exists");
        }

        Author createdAuthor = authorsDao.insertAuthor(author);
        booksIdList.forEach(bookId -> {
            bookService.findBookById(bookId);
            authorsBooksDao.authorAndBookAddById(createdAuthor.getId(), bookId);
        });

        return authorConverter.toAuthorDto(createdAuthor);
    }

    @Override
    public AuthorDto updateAuthor(Author author, List<Integer> booksIdList) {
        authorsBooksDao.deleteAuthorFromAuthorsBook(author.getId());
        Author updatedAuthor = authorsDao.updateAuthor(author);
        booksIdList.forEach(bookId -> {
            bookService.findBookById(bookId);
            authorsBooksDao.authorAndBookAddById(updatedAuthor.getId(), bookId);
        });
        return authorConverter.toAuthorDto(updatedAuthor);
    }

    @Override
    public int deleteAuthor(int id) {
        AuthorDto authorDto = findAuthorById(id);
        if (authorDto.getBooks() != null && !authorDto.getBooks().isEmpty()) {
            logger.warn("Author {} cannot be deleted because it related with books", id);
            throw new CannotDeleteException("Author cannot be deleted because it related with books");
        }
        return authorsDao.deleteAuthor(id);
    }

}
