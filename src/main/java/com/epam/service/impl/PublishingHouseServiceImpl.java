package com.epam.service.impl;

import com.epam.dao.BooksDao;
import com.epam.dao.PublishingHousesDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.PublishingHouse;
import com.epam.service.CityService;
import com.epam.service.PublishingHouseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class PublishingHouseServiceImpl implements PublishingHouseService {

    private static final Logger logger = LogManager.getLogger(PublishingHouseServiceImpl.class);
    private final PublishingHousesDao publishingHousesDao;
    private final CityService cityService;
    private final BooksDao booksDao;

    public PublishingHouseServiceImpl() {
        this.publishingHousesDao = PublishingHousesDao.getInstance();
        this.cityService = new CityServiceImpl();
        this.booksDao = BooksDao.getInstance();
    }

    public PublishingHouseServiceImpl(PublishingHousesDao publishingHousesDao, CityService cityService, BooksDao booksDao) {
        this.publishingHousesDao = publishingHousesDao;
        this.cityService = cityService;
        this.booksDao = booksDao;
    }

    @Override
    public PublishingHouse findPubHouseById(int id) {
        return publishingHousesDao.findPubHouseById(id)
                .orElseThrow(() -> {
                    logger.warn("Publishing house with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Publishing house does not exist");
                });
    }

    @Override
    public List<PublishingHouse> findAll() {
        return publishingHousesDao.findAllPublishingHouses();
    }

    @Override
    public PublishingHouse insertPublishingHouse(PublishingHouse publishingHouse) {
        cityService.findCityById(publishingHouse.getCity().getId());
        if (publishingHousesDao.existPubHouse(publishingHouse.getName().trim(), publishingHouse.getCity().getId())) {
            logger.warn("Publishing house with name {} already exists", publishingHouse.getName().trim());
            throw new ObjectAlreadyExistException("Sorry, publishing house already exists");
        }
        return publishingHousesDao.insertPublishingHouse(publishingHouse);
    }

    @Override
    public PublishingHouse updatePublishingHouse(PublishingHouse publishingHouse) {
        return publishingHousesDao
                .updatePublishingHouse(publishingHouse);
    }

    @Override
    public int deletePublishingHouse(int id) {
        PublishingHouse publishingHouse = findPubHouseById(id);
        if (!booksDao.findBookBySearchParameters(null, publishingHouse.getName(), null).isEmpty()) {
            logger.warn("Publishing house {} cannot be deleted because it related with books", id);
            throw new CannotDeleteException("Publishing house  cannot be deleted because it related with books");
        }
        return publishingHousesDao.deletePublishingHouse(id);
    }

}
