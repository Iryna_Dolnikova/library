package com.epam.service.impl;

import com.epam.dao.CitiesDao;
import com.epam.dao.PublishingHousesDao;
import com.epam.exception.CannotDeleteException;
import com.epam.exception.ObjectAlreadyExistException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.model.City;
import com.epam.service.CityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class CityServiceImpl implements CityService {

    private static final Logger logger = LogManager.getLogger(CityServiceImpl.class);

    private final CitiesDao citiesDao;
    private final PublishingHousesDao publishingHousesDao;

    public CityServiceImpl() {
        this.citiesDao = CitiesDao.getInstance();
        this.publishingHousesDao = PublishingHousesDao.getInstance();
    }

    public CityServiceImpl(CitiesDao citiesDao, PublishingHousesDao publishingHousesDao) {
        this.citiesDao = citiesDao;
        this.publishingHousesDao = publishingHousesDao;
    }

    @Override
    public City findCityById(int id) {
        return citiesDao.findCityById(id)
                .orElseThrow(() -> {
                    logger.warn("City with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("City does not exist");
                });
    }

    @Override
    public List<City> findAllCities() {
        return citiesDao.findAllCities();
    }

    @Override
    public City insertCity(String name) {
        if (citiesDao.existCityByName(name.trim())) {
            logger.warn("City {} already exists", name);
            throw new ObjectAlreadyExistException("Sorry, city already exists");
        }
        return citiesDao.insertCity(name);
    }

    @Override
    public City updateCity(String name, int id) {
        return citiesDao.updateCity(name, id);
    }

    @Override
    public int deleteCity(int id) {
        if (publishingHousesDao.existPubHouseByCityId(id)) {
            logger.warn("City {} cannot be deleted because it related with publishing houses", id);
            throw new CannotDeleteException("City cannot be deleted because it related with publishing houses");
        }
        return citiesDao.deleteCity(id);
    }

}
