package com.epam.service.impl;

import com.epam.dao.OrderDao;
import com.epam.exception.BookAbsentException;
import com.epam.exception.ObjectNotFoundByIDException;
import com.epam.exception.ReaderBlockedException;
import com.epam.service.BookService;
import com.epam.service.OrderService;
import com.epam.service.UserService;
import com.epam.web.dto.BookDto;
import com.epam.web.dto.OrderDto;
import com.epam.web.dto.OrderSearchDto;
import com.epam.web.dto.UserDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);
    public static final int MAX_ELEMENT_PER_PAGE = 5;
    private final OrderDao orderDao;
    private final UserService userService;
    private final BookService bookService;

    public OrderServiceImpl() {
        this.orderDao = OrderDao.getInstance();
        this.userService = new UserServiceImpl();
        this.bookService = BookServiceImpl.getInstance();
    }

    public OrderServiceImpl(OrderDao orderDao, UserService userService, BookService bookService) {
        this.orderDao = orderDao;
        this.userService = userService;
        this.bookService = bookService;
    }

    @Override
    public OrderDto findOrderById(int id) {
        return getOrderById(id);
    }

    @Override
    public OrderSearchDto findAllOrders(String bookName, String readerName, int currentPage) {
        List<OrderDto> orders = orderDao.findOrdersBySearchParameter(bookName);
        return getOrderSearchDto(readerName, currentPage, orders);
    }

    @Override
    public OrderSearchDto findOrdersByReaderID(int readerId, String bookName, String readerName, int currentPage) {
        List<OrderDto> orders = orderDao.findOrderByReaderId(readerId, bookName);
        return getOrderSearchDto(readerName, currentPage, orders);
    }

    private OrderSearchDto getOrderSearchDto(String readerName, int currentPage, List<OrderDto> orders) {
        orders = orders.stream()
                .filter(readerBook -> readerBook.getReader().getSurname().toLowerCase()
                        .contains(readerName != null ? readerName.toLowerCase() : ""))
                .collect(Collectors.toList());

        if (orders.isEmpty()) {
            return new OrderSearchDto(new ArrayList<>(), 0, 0);
        }

        int pagesCount = (int) Math.ceil((double) orders.size() / MAX_ELEMENT_PER_PAGE);
        currentPage = currentPage >= pagesCount ? pagesCount - 1 : currentPage;
        int indexFrom = currentPage * MAX_ELEMENT_PER_PAGE;
        int indexTo = indexFrom + MAX_ELEMENT_PER_PAGE;
        orders = orders.subList(indexFrom, Math.min(orders.size(), indexTo));

        return new OrderSearchDto(orders, pagesCount, currentPage);
    }

    @Override
    public OrderDto insertOrder(OrderDto orderDto) {
        validateOrder(orderDto);
        return orderDao.insertOrder(orderDto);
    }

    @Override
    public OrderDto updateOrder(OrderDto orderDto) {
        getOrderById(orderDto.getId());
        validateOrder(orderDto);
        return orderDao.updateOrder(orderDto);
    }

    @Override
    public void approveOrder(int orderId, int librarianId) {
        OrderDto orderDto = getOrderById(orderId);
        BookDto bookDto = validateOrder(orderDto);
        int count = bookDto.getCount();
        if (count <= 0) {
            logger.warn("Book {} is absent", bookDto.getId());
            throw new BookAbsentException("Sorry, book is absent");
        }
        bookDto.setCount(count - 1);
        bookService.updateBook(bookDto, bookDto.getId());
        orderDao.approveOrder(orderId, librarianId);
    }

    @Override
    public int deleteOrder(int id) {
        getOrderById(id);
        return orderDao.deleteOrder(id);
    }

    private BookDto validateOrder(OrderDto orderDto) {
        UserDto reader = userService.findReaderById(orderDto.getReader().getId());
        if (reader.isBlocked()) {
            logger.warn("Reader {} is blocked", reader.getSurname());
            throw new ReaderBlockedException("Sorry, reader is blocked");
        }

        BookDto book = orderDto.getBook();
        return bookService.findBookById(book.getId());
    }

    private OrderDto getOrderById(int id) {
        return orderDao.findOrderById(id)
                .orElseThrow(() -> {
                    logger.warn("Order with id {} does not exist", id);
                    return new ObjectNotFoundByIDException("Order does not exist");
                });
    }
}