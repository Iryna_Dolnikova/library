package com.epam.service;

import com.epam.model.User;
import com.epam.web.dto.LoginDto;
import com.epam.web.dto.UserDto;

import java.util.List;

/**
 * Class contains CRUD logic for users
 */
public interface UserService {

    UserDto findReaderById(int id);

    UserDto findLibrarianById(int id);

    UserDto findAdminById(int id);

    List<UserDto> findAllReaders();

    List<UserDto> findAllLibrarians();

    UserDto insertUser(User user);

    UserDto updateUser(User user, int id);

    LoginDto loginUser(String email, String password);

    UserDto blockUnblockReader(int id);

    int deleteReader(int id);

    int deleteLibrarian(int id);

}
