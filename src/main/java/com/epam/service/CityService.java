package com.epam.service;

import com.epam.model.City;

import java.util.List;

/**
 * Class contains CRUD logic for cities
 */
public interface CityService {

    City findCityById(int id);

    List<City> findAllCities();

    City insertCity(String name);

    City updateCity(String name, int id);

    int deleteCity(int id);

}
