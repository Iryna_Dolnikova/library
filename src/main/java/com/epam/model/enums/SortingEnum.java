package com.epam.model.enums;

public enum SortingEnum {
    ASC, DESC;
}
