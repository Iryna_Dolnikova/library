package com.epam.model.enums;

public enum RolesEnum {
    READER, LIBRARIAN, ADMIN;
}