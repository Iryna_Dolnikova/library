package com.epam.model;

import liquibase.pro.packaged.B;

import java.util.List;

public class Book {

    private int id;
    private String name;
    private List<Author> authors;
    private PublishingHouse publishingHouse;
    private Integer issueYear;
    private int count;

    public Book() {

    }

    public Book(int id) {
        this.id = id;
    }

    public Book(int id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public Book(int id, String name, List<Author> authors, PublishingHouse publishingHouse, Integer issueYear, int count) {
        this.id = id;
        this.name = name;
        this.authors = authors;
        this.publishingHouse = publishingHouse;
        this.issueYear = issueYear;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public PublishingHouse getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(PublishingHouse publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Integer getIssueYear() {
        return issueYear;
    }

    public void setIssueYear(Integer issueYear) {
        this.issueYear = issueYear;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", publishingHouse=" + publishingHouse +
                ", publishDate=" + issueYear +
                ", count=" + count +
                '}';
    }
}
