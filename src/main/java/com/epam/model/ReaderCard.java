package com.epam.model;

import java.sql.Date;

public class ReaderCard {
    private int id;
    private Date dateOfIssue;
    private Date expectedReturnDate;
    private Date actualReturnDate;
    private User reader;
    private User librarian;
    private Book book;

    public ReaderCard() {

    }

    public ReaderCard(int id, Date dateOfIssue, Date expectedReturnDate, Date actualReturnDate, User reader, User librarian, Book book) {
        this.id = id;
        this.dateOfIssue = dateOfIssue;
        this.expectedReturnDate = expectedReturnDate;
        this.actualReturnDate = actualReturnDate;
        this.reader = reader;
        this.librarian = librarian;
        this.book = book;
    }

    public ReaderCard(int id, Date expectedReturnDate, Date actualReturnDate) {
        this.id = id;
        this.expectedReturnDate = expectedReturnDate;
        this.actualReturnDate = actualReturnDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(Date dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public Date getExpectedReturnDate() {
        return expectedReturnDate;
    }

    public void setExpectedReturnDate(Date expectedReturnDate) {
        this.expectedReturnDate = expectedReturnDate;
    }

    public Date getActualReturnDate() {
        return actualReturnDate;
    }

    public void setActualReturnDate(Date actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }

    public User getReader() {
        return reader;
    }

    public void setReader(User user) {
        this.reader = user;
    }

    public User getLibrarian() {
        return librarian;
    }

    public void setLibrarian(User librarian) {
        this.librarian = librarian;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public static class Builder {
        private int id;
        private Date dateOfIssue;
        private Date expectedReturnDate;
        private Date actualReturnDate;
        private User reader;
        private User librarian;
        private Book book;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder dateOfIssue(Date dateOfIssue) {
            this.dateOfIssue = dateOfIssue;
            return this;
        }

        public Builder expectedReturnDate(Date expectedReturnDate) {
            this.expectedReturnDate = expectedReturnDate;
            return this;
        }

        public Builder actualReturnDate(Date actualReturnDate) {
            this.actualReturnDate = actualReturnDate;
            return this;
        }

        public Builder reader(User reader) {
            this.reader = reader;
            return this;
        }

        public Builder librarian(User librarian) {
            this.librarian = librarian;
            return this;
        }

        public Builder book(Book book) {
            this.book = book;
            return this;
        }

        public ReaderCard build() {
            return new ReaderCard(id, dateOfIssue, expectedReturnDate, actualReturnDate, reader, librarian, book);
        }
    }

    @Override
    public String toString() {
        return "ReadersBooks{" +
                "id=" + id +
                ", dateOfIssue=" + dateOfIssue +
                ", expectedReturnDate=" + expectedReturnDate +
                ", actualReturnDate=" + actualReturnDate +
                ", reader=" + reader +
                ", librarian=" + librarian +
                ", book=" + book +
                '}';
    }
}


