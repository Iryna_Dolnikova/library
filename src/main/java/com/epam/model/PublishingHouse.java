package com.epam.model;

public class PublishingHouse {

    private int id;
    private String name;
    private City city;

    public PublishingHouse() {

    }

    public PublishingHouse(int id, String name, City city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public PublishingHouse(int id) {
        this.id = id;
    }

    public PublishingHouse(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public PublishingHouse( String name, City city) {
        this.name = name;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "PublishingHouse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city=" + city +
                '}';
    }
}
