package com.epam.model;


import com.epam.model.enums.RolesEnum;

public class User {
    private int id;
    private String surname;
    private String firstName;
    private String patronymic;
    private String email;
    private String phone;
    private String password;
    private RolesEnum role;
    private boolean isBlocked;
    private boolean isActive;

    public User() {

    }

    public User(int id, RolesEnum role) {
        this.id = id;
        this.role = role;
    }

    public User(int id) {
        this.id = id;
    }

    public User(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    public User(int id, String surname, String firstName, String patronymic, String email, String phone, String password, RolesEnum role, boolean is_blocked) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role = role;
        this.isBlocked = is_blocked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RolesEnum getRole() {
        return role;
    }

    public void setRole(RolesEnum roll) {
        this.role = roll;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        this.isBlocked = blocked;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        this.isActive = active;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", role=" + role +
                ", isBlocked=" + isBlocked +
                ", isActive=" + isActive +
                '}';
    }
}
