package com.epam.validator;

import com.epam.web.dto.BookDto;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates Book by next criteria:
 * <p>
 * 1. bookName must be not null, not empty, and matches regexp for names
 * 2. publishingHouseId must be bigger or equal to 0
 * 3. count must be bigger or equal to 0
 * 4. issueYear must be bigger or equal to 0 and not bigger than current year
 */
public class BookValidator implements Validator<BookDto> {

    @Override
    public List<String> validate(BookDto bookDto, HttpServletRequest req) {
        List<String> invalidField = new ArrayList<>();
        setRequestAttributes(bookDto, req);
        if (!isNameValid(bookDto.getName())) {
            invalidField.add("name");
        }
        if (bookDto.getPublishingHouse().getId() < 0) {
            invalidField.add("publishingHouseId");
        }
        if (bookDto.getCount() < 0) {
            invalidField.add("count");
        }
        if (bookDto.getIssueYear() < 0 || bookDto.getIssueYear() > LocalDate.now().getYear()) {
            invalidField.add("issueYear");
        }
        return invalidField;
    }

    private void setRequestAttributes(BookDto bookDto, HttpServletRequest req) {
        req.setAttribute("name", bookDto.getName());
        req.setAttribute("publishingHouseId", bookDto.getPublishingHouse().getId());
        req.setAttribute("count", bookDto.getCount());
        req.setAttribute("issueYear", bookDto.getIssueYear());
    }
}


