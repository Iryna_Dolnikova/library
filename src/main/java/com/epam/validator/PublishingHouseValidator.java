package com.epam.validator;

import com.epam.model.PublishingHouse;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates PublishingHouse by next criteria:
 * <p>
 * 1. name must be not null, not empty, and matches regexp for names
 * 2. cityId must be bigger or equal to 0
 */
public class PublishingHouseValidator implements Validator<PublishingHouse> {

    @Override
    public List<String> validate(PublishingHouse publishingHouse, HttpServletRequest req) {
        List<String> invalidFields = new ArrayList<>();
        int cityId = publishingHouse.getCity().getId();
        setRequestAttributes(publishingHouse, req);
        if (!isNameValid(publishingHouse.getName())) {
            invalidFields.add("name");
        }
        if (cityId == -1) {
            invalidFields.add("cityId");
        }
        return invalidFields;
    }

    private void setRequestAttributes(PublishingHouse publishingHouse, HttpServletRequest req) {
        req.setAttribute("name", publishingHouse.getName());
        req.setAttribute("cityId", publishingHouse.getCity().getId());
    }
}
