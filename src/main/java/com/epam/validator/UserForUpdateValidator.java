package com.epam.validator;

import com.epam.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates User by next criteria:
 * <p>
 * 1. firstName, surname, patronymic must be not null, not empty, and matches regexp for names
 * 2. email must be not null, not empty, and matches regexp for email
 * 3. phone must be not null, not empty, and matches regexp for phone
 */
public class UserForUpdateValidator implements Validator<User> {

    private static final String EMAIL_REGEXP = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
    private static final String PHONE_REGEXP = "^(?:\\+?38[-.\\s]?)?0\\d{2}[-.\\s]?\\d{3}[-.\\s]?(\\d{2}[-.\\s]?){2}$";

    @Override
    public List<String> validate(User user, HttpServletRequest req) {
        List<String> invalidField = new ArrayList<>();
        setRequestAttributes(user, req);
        if (!isPersonNameValid(user.getFirstName())) {
            invalidField.add("firstName");
        }
        if (!isPersonNameValid(user.getSurname())) {
            invalidField.add("surname");
        }
        if (!isPersonNameValidOrNull(user.getPatronymic())) {
            invalidField.add("patronymic");
        }
        if (!isEmailValid(user.getEmail())) {
            invalidField.add("email");
        }
        if (!isPhoneValid(user.getPhone())) {
            invalidField.add("phone");
        }
        return invalidField;
    }

    private boolean isEmailValid(String email) {
        return email != null && !email.isEmpty() && email.matches(EMAIL_REGEXP);
    }

    private boolean isPhoneValid(String phone) {
        return phone != null && !phone.isEmpty() && phone.matches(PHONE_REGEXP);
    }

    private void setRequestAttributes(User user, HttpServletRequest req) {
        req.setAttribute("firstName", user.getFirstName());
        req.setAttribute("surname", user.getSurname());
        req.setAttribute("patronymic", user.getPatronymic());
        req.setAttribute("email", user.getEmail());
        req.setAttribute("phone", user.getPhone());
    }
}

