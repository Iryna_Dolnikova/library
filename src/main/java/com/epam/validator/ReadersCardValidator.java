package com.epam.validator;

import com.epam.web.dto.ReadersCardDto;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates ReadersCardDto by next criteria:
 * <p>
 * 1. bookId, readerId, librarianId must be bigger or equal to 0
 * 2. dateOfIssue cannot be after than current date
 * 3. expectedReturnDate cannot be before current date or after dateOfIssue
 * 4. actualReturnDate cannot be after than current date
 */
public class ReadersCardValidator implements Validator<ReadersCardDto> {

    @Override
    public List<String> validate(ReadersCardDto readersCardDto, HttpServletRequest req) {
        List<String> invalidFields = new ArrayList<>();
        LocalDate dateOfIssue = readersCardDto.getDateOfIssue().toLocalDate();
        LocalDate expectedReturnDate = readersCardDto.getExpectedReturnDate().toLocalDate();
        setRequestAttributes(readersCardDto, req);
        if (readersCardDto.getBook().getId() < 0) {
            invalidFields.add("bookId");
        }
        if (readersCardDto.getReader().getId() < 0) {
            invalidFields.add("readerId");
        }
        if (readersCardDto.getLibrarian().getId() < 0) {
            invalidFields.add("librarianId");
        }

        if (dateOfIssue.isAfter(LocalDate.now())) {
            invalidFields.add("dateOfIssue");
        }
        if (expectedReturnDate.isBefore(dateOfIssue)) {
            invalidFields.add("expectedReturnDate");
        }
        if (readersCardDto.getActualReturnDate() != null && (
                readersCardDto.getActualReturnDate().toLocalDate().isBefore(dateOfIssue) ||
                        readersCardDto.getActualReturnDate().toLocalDate().isAfter(LocalDate.now()))) {
            invalidFields.add("actualReturnDate");
        }
        return invalidFields;
    }

    private void setRequestAttributes(ReadersCardDto readersCardDto, HttpServletRequest req) {
        req.setAttribute("bookId", readersCardDto.getBook().getId());
        req.setAttribute("readerId", readersCardDto.getReader().getId());
        req.setAttribute("librarianId", readersCardDto.getLibrarian().getId());
        req.setAttribute("dateOfIssue", readersCardDto.getDateOfIssue());
        req.setAttribute("expectedReturnDate", readersCardDto.getExpectedReturnDate());
        req.setAttribute("actualReturnDate", readersCardDto.getActualReturnDate());
    }
}
