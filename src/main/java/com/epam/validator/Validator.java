package com.epam.validator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface Validator<T> {
    String NAME_REGEXP = "(\\p{L}*[-\\.\\s\\d]*){2,255}";
    String PERSON_NAME_REGEXP = "(\\p{L}+[-'\\.\\s]*){2,255}";

    List<String> validate(T objectToValidate, HttpServletRequest req);

    default boolean isNameValid(String name) {
        return name != null && !name.trim().isEmpty() && name.matches(NAME_REGEXP);
    }

    default boolean isPersonNameValid(String name) {
        return name != null && !name.isEmpty() && name.matches(PERSON_NAME_REGEXP);
    }

    default boolean isPersonNameValidOrNull(String name) {
        return name == null || name.trim().isEmpty() || (name.matches(PERSON_NAME_REGEXP));
    }

}
