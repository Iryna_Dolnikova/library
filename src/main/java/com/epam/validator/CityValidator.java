package com.epam.validator;

import com.epam.model.City;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates city by next criteria:
 * <p>
 * 1. name must be not null, not empty, and matches regexp for names
 */
public class CityValidator implements Validator<City> {

    @Override
    public List<String> validate(City city, HttpServletRequest req) {
        List<String> invalidFields = new ArrayList<>();
        req.setAttribute("name", city.getName());
        if (!isNameValid(city.getName())) {
            invalidFields.add("name");
        }
        return invalidFields;
    }

}
