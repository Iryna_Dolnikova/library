package com.epam.validator;

import com.epam.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates User by next criteria:
 * <p>
 * 1. email must be not null, not empty, and matches regexp for email
 * 2. password must be not null, not empty, and matches regexp for passwords
 */
public class LoginValidator implements Validator<User> {

    private static final String EMAIL_REGEXP = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";

    private static final String PASSWORD_REGEXP = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,16}$";

    @Override
    public List<String> validate(User user, HttpServletRequest req) {
        List<String> invalidField = new ArrayList<>();
        setRequestAttributes(user, req);
        if (!isEmailValid(user.getEmail())) {
            invalidField.add("email");
        }
        if (!isPasswordValid(user.getPassword())) {
            invalidField.add("password");
        }
        return invalidField;
    }

    private boolean isEmailValid(String email) {
        return email != null && !email.isEmpty() && email.matches(EMAIL_REGEXP);
    }

    private boolean isPasswordValid(String password) {
        return password != null && !password.isEmpty() && password.matches(PASSWORD_REGEXP);
    }

    private void setRequestAttributes(User user, HttpServletRequest req) {
        req.setAttribute("email", user.getEmail());
        req.setAttribute("password", user.getPassword());
    }
}

