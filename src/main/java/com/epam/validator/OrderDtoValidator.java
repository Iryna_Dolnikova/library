package com.epam.validator;

import com.epam.web.dto.OrderDto;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates OrderDto by next criteria:
 * <p>
 * 1. bookId, readerId must be bigger or equal to 0
 * 2. dateOfIssue cannot be after than current date
 * 3. expectedReturnDate cannot be before current date or after dateOfIssue
 */
public class OrderDtoValidator implements Validator<OrderDto> {

    @Override
    public List<String> validate(OrderDto readersCardDto, HttpServletRequest req) {
        List<String> invalidFields = new ArrayList<>();
        LocalDate dateOfIssue = readersCardDto.getDateOfIssue().toLocalDate();
        LocalDate expectedReturnDate = readersCardDto.getExpectedReturnDate().toLocalDate();
        setRequestAttributes(readersCardDto, req);
        if (readersCardDto.getBook().getId() < 0) {
            invalidFields.add("bookId");
        }
        if (readersCardDto.getReader().getId() < 0) {
            invalidFields.add("readerId");
        }

        if (dateOfIssue.isBefore(LocalDate.now())) {
            invalidFields.add("dateOfIssue");
        }
        if (expectedReturnDate.isBefore(LocalDate.now()) || expectedReturnDate.isBefore(dateOfIssue)) {
            invalidFields.add("expectedReturnDate");
        }
        return invalidFields;
    }

    private void setRequestAttributes(OrderDto orderDto, HttpServletRequest req) {
        req.setAttribute("bookId", orderDto.getBook().getId());
        req.setAttribute("readerId", orderDto.getReader().getId());
        req.setAttribute("dateOfIssue", orderDto.getDateOfIssue());
        req.setAttribute("expectedReturnDate", orderDto.getExpectedReturnDate());
    }
}
