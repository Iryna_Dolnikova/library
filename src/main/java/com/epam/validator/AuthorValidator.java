package com.epam.validator;

import com.epam.model.Author;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Class validates Author by next criteria:
 * <p>
 * 1. firstName, surname, patronymic must be not null, not empty, and matches regexp for names
 */
public class AuthorValidator implements Validator<Author> {

    @Override
    public List<String> validate(Author author, HttpServletRequest req) {
        List<String> invalidField = new ArrayList<>();
        setRequestAttributes(author, req);
        if (!isPersonNameValid(author.getFirstName())) {
            invalidField.add("firstName");
        }
        if (!isPersonNameValid(author.getSurname())) {
            invalidField.add("surname");
        }
        if (!isPersonNameValidOrNull(author.getPatronymic())) {
            invalidField.add("patronymic");
        }
        return invalidField;
    }

    private void setRequestAttributes(Author author, HttpServletRequest req) {
        req.setAttribute("firstName", author.getFirstName());
        req.setAttribute("surname", author.getSurname());
        req.setAttribute("patronymic", author.getPatronymic());
    }

}
