<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<html lang="${language}">

<head>
    <title><fmt:message key="a.profile" bundle="${msg}"/></title>


    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
        <%@include file="/static/table.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="user.profile.title" bundle="${msg}"/></h1>

    <div>
        <ul>
            <li>
                <label for="surname"><fmt:message key="user.surname.title" bundle="${msg}"/>:</label>
                <span id="surname">${currentUser.surname}</span>
            </li>
            <li>
                <label for="firstName"><fmt:message key="user.firstname.title" bundle="${msg}"/>:</label>
                <span id="firstName">${currentUser.firstName}</span>
            </li>
            <li>
                <label for="patronymic"><fmt:message key="user.patronymic.title" bundle="${msg}"/>:</label>
                <span id="patronymic">${currentUser.patronymic}</span>
            </li>
            <li>
                <label for="email"><fmt:message key="user.email.title" bundle="${msg}"/>:</label>
                <span id="email">${currentUser.email}</span>
            </li>
            <li>
                <label for="phone"><fmt:message key="user.phone.title" bundle="${msg}"/>:</label>
                <span id="phone">${currentUser.phone}</span>
            </li>
        </ul>
    </div>

</div>

<% if (session != null && session.getAttribute("loginedUser") != null
        && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.ADMIN)) { %>

    <div>
        <h1><fmt:message key="cards.title" bundle="${msg}"/></h1>

        <table>
            <tr>
                <th><fmt:message key="card.title" bundle="${msg}"/></th>
                <th><fmt:message key="reader.name.title" bundle="${msg}"/></th>
                <th><fmt:message key="label.book.name" bundle="${msg}"/></th>
                <th><fmt:message key="librarian.name.title" bundle="${msg}"/></th>
                <th><fmt:message key="card.taking.date.title" bundle="${msg}"/></th>
                <th><fmt:message key="card.waiting.date.title" bundle="${msg}"/></th>
                <th><fmt:message key="card.return.date.title" bundle="${msg}"/></th>
                <th><fmt:message key="card.penalty.title" bundle="${msg}"/></th>
            </tr>
            <c:forEach items="${readersCardDtoList}" var="readerCard">
                <tr>
                    <td>${readerCard.id}</td>
                    <td>${readerCard.reader.fullName}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/books?id=${(readerCard.book.id)}">${readerCard.book.name}</a>
                    </td>
                    <td>${readerCard.librarian.fullName}</td>
                    <td>${readerCard.dateOfIssue}</td>
                    <td>${readerCard.expectedReturnDate}</td>
                    <td>${readerCard.actualReturnDate}</td>
                    <td>${readerCard.penalty}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
<% } %>

</body>
</html>
