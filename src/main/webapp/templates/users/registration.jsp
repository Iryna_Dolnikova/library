<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="a.registration" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/main_header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="a.registration" bundle="${msg}"/></h1>
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="registration" method="post">
        <ul>
            <li>
                <label for="surname"><fmt:message key="user.surname.title" bundle="${msg}"/>:</label>
                <input type="text" id="surname" name="surname" class="input" placeholder="Potter" value="${surname}" required/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="firstName"><fmt:message key="user.firstname.title" bundle="${msg}"/>:</label>
                <input type="text" id="firstName" name="firstName" class="input" placeholder="Harry" value="${firstName}"  required/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="patronymic"><fmt:message key="user.patronymic.title" bundle="${msg}"/>:</label>
                <input type="text" id="patronymic" name="patronymic" class="input" value="${patronymic}" placeholder="Jamesovich"/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="email"><fmt:message key="user.email.title" bundle="${msg}"/>:</label>
                <input type="email" id="email" name="email" class="input" placeholder="potter@gmail.com" value="${email}" required/>
            </li>
            <li>
                <label for="phone"><fmt:message key="user.phone.title" bundle="${msg}"/>:</label>
                <input type="tel" id="phone" name="phone" class="input" value="${phone}"
                       pattern="^(?:\+?38[-.\s]?)?0\d{2}[-.\s]?\d{3}[-.\s]?(\d{2}[-.\s]?){2}$"
                       placeholder="050-123-45-67" required/>
            </li>
            <li>
                <label for="roleLabel"><fmt:message key="user.role.title" bundle="${msg}"/>:</label>
                &nbsp;
                <label class="rad" id="roleLabel">
                    <input type="radio" name="role" value="READER" checked="checked" />
                    <i></i> <fmt:message key="reader.title" bundle="${msg}"/>
                </label>
                &nbsp;
                <label class="rad">
                    <input type="radio" name="role" value="LIBRARIAN" />
                    <i></i> <fmt:message key="librarian.title" bundle="${msg}"/>
                </label>
            </li>
            <li>
                <label for="password"><fmt:message key="user.password.title" bundle="${msg}"/>:</label>
                <input type="text" id="password" name="password" class="input" value="${password}"
                       pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,16}$"
                       placeholder="Jamesovich@123" required/>
                <p class="hint"><fmt:message key="hint.password" bundle="${msg}"/></p>
            </li>
        </ul>

        <button type="submit" class="edit"><fmt:message key="button.create.title" bundle="${msg}"/></button>
    </form>

</div>
</body>
</html>