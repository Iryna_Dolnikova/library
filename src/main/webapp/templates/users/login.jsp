<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><fmt:message key="a.login" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/main_header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="a.login" bundle="${msg}"/></h1>
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="login" method="post">
        <ul>
            <li>
                <label for="email"><fmt:message key="user.email.title" bundle="${msg}"/>:</label>
                <input type="email" id="email" name="email" class="input" placeholder="potter@gmail.com" required/>
            </li>
            <li>
                <label for="password"><fmt:message key="user.password.title" bundle="${msg}"/>:</label>
                <input type="password" id="password" name="password" class="input" placeholder="Jamesovich@123" required/>
            </li>
        </ul>

        <button type="submit" class="edit"><fmt:message key="a.login" bundle="${msg}"/></button>
    </form>

</div>
</body>
</html>