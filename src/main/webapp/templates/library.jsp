<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="a.library" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/search.css"%>
        <%@include file="/static/table.css"%>
        <%@include file="/static/pagintaion.css"%>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="form" style="padding-left:16px;padding-top:16px">
    <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="search" method="get">
        <div class="form-group">
            <label for="bookName"><fmt:message key="label.book.name" bundle="${msg}"/>:</label>
            <input name="bookName" id="bookName" type="text" class="form-control" value="${bookName}"/>
        </div>
        <div class="form-group">
            <label for="authorName"><fmt:message key="label.author.name" bundle="${msg}"/>:</label>
            <input name="authorName" id="authorName" type="text" class="form-control" value="${authorName}"/>
        </div>
        <div class="form-group">
            <label for="phName"><fmt:message key="label.publishing.house.name" bundle="${msg}"/>:</label>
            <input name="phName" id="phName" type="text" class="form-control" value="${phName}"/>
        </div>
        <div>
            <label for="issuerYear"><fmt:message key="label.sort.by.year" bundle="${msg}"/>:</label>
            <input type="radio" id="issuerYear" name="sort" value="issuerYear:ASC"
            ${sort != null && sort.equals("issuerYear:ASC") ? 'checked' : ''}/>ASC
            <input type="radio" id="issuerYearDesc" name="sort" value="issuerYear:DESC"
            ${sort != null && sort.equals("issuerYear:DESC") ? 'checked' : ''}/>DESC
        </div>
        <div>
            <label for="countAsc"><fmt:message key="label.sort.by.count" bundle="${msg}"/>:</label>
            <input type="radio" id="countAsc" name="sort" value="count:ASC"
            ${sort != null && sort.equals("count:ASC") ? 'checked' : ''}/>ASC
            <input type="radio" id="countDesc" name="sort" value="count:DESC"
            ${sort != null && sort.equals("count:DESC") ? 'checked' : ''}/>DESC
        </div>

        <button type="submit"><fmt:message key="button.search.title" bundle="${msg}"/></button>
        <div class="reset">
            <a class="reset" href="${pageContext.request.contextPath}/search"><fmt:message key="button.clear.title" bundle="${msg}"/></a>
        </div>

        <div class="title">
            <h1><fmt:message key="books.title" bundle="${msg}"/></h1>
            <% if (session != null && session.getAttribute("loginedUser") != null
                    && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
            <a href="${pageContext.request.contextPath}/books_add"><fmt:message key="book.create.title" bundle="${msg}"/></a>
            <% } %>
        </div>
    </form>

    <div class="pagination">
        <c:forEach items="${pages}" var="page">
            <a class="${currentPage == page? 'active': ''}"
               href="${pageContext.request.contextPath}/search?currentPage=${(page)}&bookName=${bookName}&authorName=${authorName}&phName=${phName}&sort=${sort}">
                    ${page+1}</a>
        </c:forEach>
    </div>

    <br>
    <div>
        <table>
            <tr>
                <th><fmt:message key="label.book.name" bundle="${msg}"/></th>
                <th><fmt:message key="authors.title" bundle="${msg}"/></th>
                <th><fmt:message key="publishing.house.title" bundle="${msg}"/></th>
                <th><fmt:message key="book.issuer.year.title" bundle="${msg}"/></th>
                <th><fmt:message key="available.number.title" bundle="${msg}"/></th>
            </tr>
            <c:forEach items="${books}" var="book">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/books?id=${(book.id)}">${book.name}</a></td>
                    <td>
                        <c:forEach items="${book.authors}" var="author">
                            <a href="${pageContext.request.contextPath}/authors?id=${(author.id)}">${author.fullName}
                                    ${book.authors.get(book.authors.size()-1).id != author.id ? ';' : ''}
                            </a>
                        </c:forEach>
                    </td>
                    <td><a href="${pageContext.request.contextPath}/publishingHouses?id=${(book.publishingHouse.id)}">
                            ${book.publishingHouse.name}</a></td>
                    <td>${book.issueYear}</td>
                    <td>${book.count}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<script type="text/javascript" src="../libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../libs/propper-1.11.0/popper.min.js"></script>
<script type="text/javascript" src="../libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>

</body>
</html>
