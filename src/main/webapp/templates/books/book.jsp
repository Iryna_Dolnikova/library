<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" th:href="@{/profile.css}">
    <title><fmt:message key="book.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
        <%@include file="/static/table.css"%>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="book.title" bundle="${msg}"/></h1>
    <% if (session != null && session.getAttribute("loginedUser") != null
            && !((LoginDto)session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
        <form class="col-6" action="books_edit" method="get">
            <input type="hidden" name="id" value="${book.id}">
            <button type="submit" class="edit"><fmt:message key="button.edit.title" bundle="${msg}"/></button>
        </form>
        <form class="col-6" action="books_delete" method="get">
            <input type="hidden" name="id" value="${book.id}">
            <button type="submit" class="delete"><fmt:message key="button.delete.title" bundle="${msg}"/></button>
        </form>
    <% } %>

    <div>
        <ul>
            <li>
                <label for="name"><fmt:message key="name.title" bundle="${msg}"/>:</label>
                <span id="name">${book.name}</span>
            </li>
            <li>
                <label for="publishingHouse"><fmt:message key="publishing.house.title" bundle="${msg}"/>:</label>
                <span><a id="publishingHouse"
                         href="${pageContext.request.contextPath}/publishingHouses?id=${(book.publishingHouse.id)}">
                    ${book.publishingHouse.name}
                </a></span>
            </li>
            <li>
                <label for="issueYear"><fmt:message key="book.issuer.year.title" bundle="${msg}"/>:</label>
                <span id="issueYear">${book.issueYear}</span>
            </li>
            <li>
                <label for="count"><fmt:message key="book.count.title" bundle="${msg}"/>:</label>
                <span id="count">${book.count}</span>
            </li>
        </ul>
    </div>

</div>

<div>
    <h1><fmt:message key="authors.title" bundle="${msg}"/></h1>

    <table id="authors">
        <tr>
            <th><fmt:message key="author.full.name.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.surname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.firstname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.patronymic.title" bundle="${msg}"/></th>
        </tr>
        <c:forEach items="${book.authors}" var="author">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/authors?id=${(author.id)}">
                            ${author.fullName}
                    </a>
                </td>
                <td>${author.surname}</td>
                <td>${author.firstName}</td>
                <td>${author.patronymic}</td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>