<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <title><fmt:message key="book.create.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
        <%@include file="/static/table.css"%>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div>

    <form name="myForm" class="form" action="books_add" method="post">
        <div layout:fragment="content" class="card">
            <h1><fmt:message key="book.create.title" bundle="${msg}"/></h1>

            <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
                <p class="error">${error}</p>
            <% } %>

            <ul>
                <li>
                    <label for="name"><fmt:message key="name.title" bundle="${msg}"/>:</label>
                    <input type="text" id="name" name="name" class="input" placeholder="Harry Potter" value="${name}" required/>
                    <p class="hint"><fmt:message key="hint.book.name" bundle="${msg}"/></p>
                </li>
                <li>
                    <label for="issueYear">Issue Year:</label>
                    <input type="number" min="1500" max="3000" step="1" id="issueYear" name="issueYear" class="input"
                           placeholder="2022" value="${issueYear}" required/>
                    <p class="hint"><fmt:message key="hint.issuer.year" bundle="${msg}"/></p>
                </li>
                <li>
                    <label for="count"><fmt:message key="book.count.title" bundle="${msg}"/>:</label>
                    <input type="number" min="0" step="1" id="count" name="count" class="input" placeholder="100" value="${count}" required/>
                    <p class="hint"><fmt:message key="hint.count" bundle="${msg}"/></p>
                </li>
                <li>
                    <label for="pHouses"><fmt:message key="publishing.house.title" bundle="${msg}"/>:</label>
                    <select id="pHouses" name="phId">
                        <c:forEach items="${pHouses}" var="pHouse">
                            <option id="phId" name="phId" value="${pHouse.id}"
                                ${pHouse.id == publishingHouseId ? 'selected="selected"' : ''}>
                                    ${pHouse.name}</option>
                        </c:forEach>
                    </select>
                </li>
                <p class="hint"><fmt:message key="hint.select" bundle="${msg}"/></p>
            </ul>

            <button type="submit" class="edit"><fmt:message key="button.create.title" bundle="${msg}"/></button>
        </div>

        <div>
            <table id="authors">
                <tr>
                    <th></th>
                    <th><fmt:message key="user.full.name.title" bundle="${msg}"/></th>
                    <th><fmt:message key="user.surname.title" bundle="${msg}"/></th>
                    <th><fmt:message key="user.firstname.title" bundle="${msg}"/></th>
                    <th><fmt:message key="user.patronymic.title" bundle="${msg}"/></th>
                </tr>
                <c:forEach items="${authors}" var="author">
                    <tr>
                        <td><input type="checkbox" id="authorsIds" name="authorsIds" value="${author.id}"
                            ${authorsIds.contains(author.id) ? 'checked' : ''}></td>
                        <td><a href="${pageContext.request.contextPath}/authors?id=${(author.id)}">${author.fullName}</a></td>
                        <td>${author.surname}</td>
                        <td>${author.firstName}</td>
                        <td>${author.patronymic}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </form>

</div>
</body>
</html>