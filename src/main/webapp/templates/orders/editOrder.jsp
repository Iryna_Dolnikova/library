<%@page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="order.edit.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="order.edit.title" bundle="${msg}"/></h1>
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="orders_update?id=${order.id}" method="post">

        <ul>
            <li>
                <label for="readers"><fmt:message key="reader.title" bundle="${msg}"/>:</label>
                <input type="hidden" id="readerId" name="readerId" value="${order.reader.id}"/>
                <span id="readers">${order.reader.fullName}</span>
            </li>
            <li>
                <label for="books"><fmt:message key="book.title" bundle="${msg}"/>:</label>
                <select id="books" name="bookId">
                    <c:forEach items="${books}" var="book">
                        <option id="bookId" name="bookId" value="${book.id}"
                            ${book.id == readerCard.book.id ? 'selected="selected"' : ''}>
                                ${book.name}
                        </option>
                    </c:forEach>
                </select>
                <p class="hint"><fmt:message key="hint.select" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="dateOfIssue"><fmt:message key="card.taking.date.title" bundle="${msg}"/>:</label>
                <input type="date" id="dateOfIssue" name="dateOfIssue" class="input" value="${dateOfIssue}" required/>
            </li>
            <li>
                <label for="expectedReturnDate"><fmt:message key="card.waiting.date.title" bundle="${msg}"/>:</label>
                <input type="date" id="expectedReturnDate" name="expectedReturnDate"
                       value="${expectedReturnDate}" required/>
            </li>
        </ul>

        <button type="submit" class="edit"><fmt:message key="button.save.title" bundle="${msg}"/></button>
    </form>

</div>
</body>
</html>