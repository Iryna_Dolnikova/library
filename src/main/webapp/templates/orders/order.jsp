<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="order.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css" %>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="order.title" bundle="${msg}"/></h1>

    <% if (session != null && session.getAttribute("loginedUser") != null
            && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.LIBRARIAN)) { %>
        <form class="col-6" action="orders_edit" method="get">
            <input type="hidden" name="id" value="${order.id}">
            <button type="submit" class="edit"><fmt:message key="button.edit.title" bundle="${msg}"/></button>
        </form>
        <form class="col-6" action="orders_delete" method="get">
            <input type="hidden" name="id" value="${order.id}">
            <button type="submit" class="delete"><fmt:message key="button.delete.title" bundle="${msg}"/></button>
        </form>
    <% } else if (session != null && session.getAttribute("loginedUser") != null
            && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)){ %>
        <form class="col-6" action="orders_approve" method="get">
            <input type="hidden" name="id" value="${order.id}">
            <button type="submit" class="edit"><fmt:message key="button.approve.title" bundle="${msg}"/></button>
        </form>
    <% } %>

    <div class="form">
        <ul>
            <li>
                <label for="reader"><fmt:message key="reader.name.title" bundle="${msg}"/>:</label>
                <% if (session != null && session.getAttribute("loginedUser") != null
                        && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
                    <span><a id="reader" href="${pageContext.request.contextPath}/readers?id=${(order.reader.id)}">
                        ${order.reader.fullName}
                    </a></span>
                <% } else { %>
                    <span><a id="readerProfile" href="${pageContext.request.contextPath}/profile">
                        ${order.reader.fullName}
                    </a></span>
                <% } %>
            </li>
            <li>
                <label for="book"><fmt:message key="label.book.name" bundle="${msg}"/>:</label>
                <span><a id="book" href="${pageContext.request.contextPath}/books?id=${(order.book.id)}">
                    ${order.book.name}
                </a></span>
            </li>

            <li>
                <label for="takingDate"><fmt:message key="card.taking.date.title" bundle="${msg}"/>:</label>
                <span id="takingDate">${order.dateOfIssue}</span>
            </li>
            <li>
                <label for="waitingDate"><fmt:message key="card.waiting.date.title" bundle="${msg}"/>:</label>
                <span id="waitingDate">${order.expectedReturnDate}</span>
            </li>
        </ul>
    </div>

</div>
</body>
</html>