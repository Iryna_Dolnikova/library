<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="orders.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/search.css"%>
        <%@include file="/static/table.css"%>
        <%@include file="/static/pagintaion.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="form" style="padding-left:16px;padding-top:16px">
    <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="orders" method="get">
        <div class="form-group">
            <label for="bookName"><fmt:message key="label.book.name" bundle="${msg}"/>:</label>
            <input name="bookName" id="bookName" type="text" class="form-control" value="${bookName}"/>
        </div>
        <% if (session != null && session.getAttribute("loginedUser") != null
                && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
            <div class="form-group">
                <label for="readerName"><fmt:message key="reader.name.title" bundle="${msg}"/>:</label>
                <input name="readerName" id="readerName" type="text" class="form-control" value="${readerName}"/>
            </div>
        <% } %>
        <button type="submit"><fmt:message key="button.search.title" bundle="${msg}"/></button>
        <div class="reset">
            <a class="reset" href="${pageContext.request.contextPath}/orders"><fmt:message key="button.clear.title" bundle="${msg}"/></a>
        </div>
    </form>

    <div class="title">
        <h1><fmt:message key="orders.title" bundle="${msg}"/></h1>
        <% if (session != null && session.getAttribute("loginedUser") != null
                && ((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
        <a href="${pageContext.request.contextPath}/orders_add"><fmt:message key="order.create.title"
                                                                             bundle="${msg}"/></a>
        <% } %>
    </div>

    <div class="pagination">
        <c:forEach items="${pages}" var="page">
            <a class="${currentPage == page? 'active': ''}"
               href="${pageContext.request.contextPath}/orders?currentPage=${(page)}&bookName=${bookName}&readerName=${readerName}">
                    ${page+1}</a>
        </c:forEach>
    </div>
    <br>

    <div>
        <table>
            <tr>
                <th><fmt:message key="card.title" bundle="${msg}"/></th>
                <th><fmt:message key="reader.name.title" bundle="${msg}"/></th>
                <th><fmt:message key="label.book.name" bundle="${msg}"/></th>
                <th><fmt:message key="card.taking.date.title" bundle="${msg}"/></th>
                <th><fmt:message key="card.waiting.date.title" bundle="${msg}"/></th>
            </tr>
            <c:forEach items="${orders}" var="order">
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/orders?id=${(order.id)}">${order.id}</a>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${order.reader.isActive}">
                                <% if (session != null && session.getAttribute("loginedUser") != null
                                        && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
                                <span><a id="reader"
                                         href="${pageContext.request.contextPath}/readers?id=${(order.reader.id)}">
                                        ${order.reader.fullName}
                                </a></span>
                                <% } else { %>
                                <span><a id="readerProfile" href="${pageContext.request.contextPath}/profile">
                                        ${order.reader.fullName}
                                </a></span>
                                <% } %>
                            </c:when>
                            <c:otherwise>
                                ${order.reader.fullName}<br>
                                <p style="color:gray"><fmt:message key="user.inactive" bundle="${msg}"/></p>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/books?id=${(order.book.id)}">${order.book.name}</a>
                    </td>
                    <td>${order.dateOfIssue}</td>
                    <td>${order.expectedReturnDate}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>