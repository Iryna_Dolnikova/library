<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="city.edit.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css" %>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="city.edit.title" bundle="${msg}"/></h1>
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="cities_update?id=${city.id}" method="post">
        <ul>
            <li>
                <label for="name"><fmt:message key="name.title" bundle="${msg}"/>:</label>
                <input name="name" id="name" type="text" class="form-control" value="${city.name}" required/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
        </ul>
        <button type="submit" class="edit"><fmt:message key="button.save.title" bundle="${msg}"/></button>
    </form>

</div>
</body>
</html>