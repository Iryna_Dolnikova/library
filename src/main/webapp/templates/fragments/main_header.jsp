<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        <%@include file="/static/header.css" %>
    </style>
</head>

<body>
<block fragment="header">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="topnav">
                <a href="${pageContext.request.contextPath}/" style="color: #04AA6D;"><fmt:message key="a.login" bundle="${msg}"/></a>
                <a href="${pageContext.request.contextPath}/registration" style="color: #04AA6D;"><fmt:message key="a.registration" bundle="${msg}"/></a>

                <div style="float: right;">
                    <a href="${requestScope['javax.servlet.forward.query_string']}?language=uk" style="color: #04AA6D;">UA</a>
                    <a href="${requestScope['javax.servlet.forward.query_string']}?language=eu" style="color: #04AA6D;">EN</a>
                </div>
            </div>
        </div>
    </nav>
</block>
</body>
</html>
