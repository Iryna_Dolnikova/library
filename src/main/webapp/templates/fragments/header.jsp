<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        <%@include file="/static/header.css" %>
    </style>
</head>

<body>
<block fragment="header">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="topnav">
                <a href="${pageContext.request.contextPath}/search" style="color: #04AA6D;"><fmt:message key="a.library" bundle="${msg}"/></a>
                <a href="${pageContext.request.contextPath}/search" style="color: #04AA6D;"><i class="fa fa-home"></i></a>

                <a href="${pageContext.request.contextPath}/cities"><fmt:message key="a.all.cities" bundle="${msg}"/></a>
                <a href="${pageContext.request.contextPath}/publishingHouses"><fmt:message key="a.all.publishing.houses" bundle="${msg}"/></a>

                <a href="${pageContext.request.contextPath}/authors"><fmt:message key="a.all.authors" bundle="${msg}"/></a>

                <% if (session != null && session.getAttribute("loginedUser") != null
                        && ((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.LIBRARIAN)) { %>

                    <a href="${pageContext.request.contextPath}/readers"><fmt:message key="a.all.readers" bundle="${msg}"/></a>
                    <a href="${pageContext.request.contextPath}/readersCards"><fmt:message key="a.all.readers.cards" bundle="${msg}"/></a>
                <% } else if (session != null && session.getAttribute("loginedUser") != null
                        && ((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.ADMIN)) { %>

                    <a href="${pageContext.request.contextPath}/readers"><fmt:message key="a.all.readers" bundle="${msg}"/></a>
                    <a href="${pageContext.request.contextPath}/librarians"><fmt:message key="a.all.librarians" bundle="${msg}"/></a>
                    <a href="${pageContext.request.contextPath}/readersCards"><fmt:message key="a.all.readers.cards" bundle="${msg}"/></a>
                <% } %>

                <div style="float: right;">
                    <a href="${pageContext.request.contextPath}/orders" style="color: #04AA6D;"><fmt:message key="a.all.orders" bundle="${msg}"/></a>

                    <a href="${pageContext.request.contextPath}/profile" style="color: #04AA6D;"><fmt:message key="a.profile" bundle="${msg}"/></a>
                    <a href="${pageContext.request.contextPath}/profile" style="color: #04AA6D;"><i
                            class="fas">&#xf007;</i></a>
                    <a href="${pageContext.request.contextPath}/logout" style="color: #04AA6D;"><fmt:message key="a.logout" bundle="${msg}"/></a>
                    <a href="${pageContext.request.contextPath}/logout" style="color: #04AA6D;"><i class="fa">&#xf08b;</i></a>
                </div>

                <a href="${requestScope['javax.servlet.forward.query_string'].contains("language")
                    ? requestScope['javax.servlet.forward.query_string'].substring(0, requestScope['javax.servlet.forward.query_string'].length()-11)
                    : requestScope['javax.servlet.forward.query_string']}?language=uk" style="color: #04AA6D;">UA</a>

                <a href="${requestScope['javax.servlet.forward.query_string'].contains("language")
                    ? requestScope['javax.servlet.forward.query_string'].substring(0, requestScope['javax.servlet.forward.query_string'].length()-11)
                    : requestScope['javax.servlet.forward.query_string']}?language=eu" style="color: #04AA6D;" >EN</a>
            </div>
        </div>
    </nav>
</block>
</body>
</html>
