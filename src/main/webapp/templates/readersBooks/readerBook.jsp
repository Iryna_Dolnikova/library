<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="reader.cards.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css" %>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="reader.cards.title" bundle="${msg}"/></h1>
    <form class="col-6" action="readersCards_edit" method="get">
        <input type="hidden" name="id" value="${readerCard.id}">
        <button type="submit" class="edit"><fmt:message key="button.edit.title" bundle="${msg}"/></button>
    </form>
    <form class="col-6" action="readersCards_delete" method="get">
        <input type="hidden" name="id" value="${readerCard.id}">
        <button type="submit" class="delete"><fmt:message key="button.delete.title" bundle="${msg}"/></button>
    </form>

    <div class="form">
        <ul>
            <li>
                <label for="reader"><fmt:message key="reader.name.title" bundle="${msg}"/>:</label>
                <span><a id="reader" href="${pageContext.request.contextPath}/readers?id=${(readerCard.reader.id)}">
                    ${readerCard.reader.fullName}
                </a></span>
            </li>
            <li>
                <label for="book"><fmt:message key="label.book.name" bundle="${msg}"/>:</label>
                <span><a id="book" href="${pageContext.request.contextPath}/books?id=${(readerCard.book.id)}">
                    ${readerCard.book.name}
                </a></span>
            </li>
            <li>
                <label for="librarian"><fmt:message key="librarian.name.title" bundle="${msg}"/>:</label>
                <span><a id="librarian" href="${pageContext.request.contextPath}/librarians?id=${(readerCard.librarian.id)}">
                    ${readerCard.librarian.fullName}
                </a></span>
            </li>

            <li>
                <label for="takingDate"><fmt:message key="card.taking.date.title" bundle="${msg}"/>:</label>
                <span id="takingDate">${readerCard.dateOfIssue}</span>
            </li>
            <li>
                <label for="waitingDate"><fmt:message key="card.waiting.date.title" bundle="${msg}"/>:</label>
                <span id="waitingDate">${readerCard.expectedReturnDate}</span>
            </li>
            <li>
                <label for="returnDate"><fmt:message key="card.return.date.title" bundle="${msg}"/>:</label>
                <span id="returnDate">${readerCard.actualReturnDate}</span>
            </li>
            <li>
                <label for="penalty"><fmt:message key="card.penalty.title" bundle="${msg}"/>:</label>
                <span id="penalty">${readerCard.penalty}</span>
            </li>
        </ul>
    </div>

</div>
</body>
</html>