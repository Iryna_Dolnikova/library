<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="readers.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/table.css"%>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content">
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <div class="title">
        <h1><fmt:message key="readers.title" bundle="${msg}"/></h1>
        <a href="${pageContext.request.contextPath}/readers_add"><fmt:message key="reader.create.title" bundle="${msg}"/></a>
    </div>

    <table>
        <tr>
            <th><fmt:message key="user.full.name.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.surname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.firstname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.patronymic.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.email.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.phone.title" bundle="${msg}"/></th>
        </tr>
        <c:forEach items="${readers}" var="reader">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/readers?id=${(reader.id)}">
                            ${reader.fullName}
                    </a>
                </td>
                <td>${reader.surname}</td>
                <td>${reader.firstName}</td>
                <td>${reader.patronymic}</td>
                <td>${reader.email}</td>
                <td>${reader.phone}</td>
            </tr>
        </c:forEach>
    </table>

</div>
</body>
</html>