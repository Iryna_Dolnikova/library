<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <title><fmt:message key="authors.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/table.css"%>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content">
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <div class="title">
        <h1><fmt:message key="authors.title" bundle="${msg}"/></h1>
        <% if (session != null && session.getAttribute("loginedUser") != null
                && !((LoginDto) session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
        <a href="${pageContext.request.contextPath}/authors_add"><fmt:message key="author.create.title" bundle="${msg}"/></a>
        <% } %>
    </div>

    <table>
        <tr>
            <th><fmt:message key="user.full.name.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.surname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.firstname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.patronymic.title" bundle="${msg}"/></th>
            <th><fmt:message key="books.title" bundle="${msg}"/></th>
        </tr>

        <c:forEach items="${authors}" var="author">
            <tr>
                <td><a href="${pageContext.request.contextPath}/authors?id=${(author.id)}">${author.fullName}</a></td>
                <td>${author.surname}</td>
                <td>${author.firstName}</td>
                <td>${author.patronymic}</td>
                <td>
                    <c:forEach items="${author.books}" var="book">
                        <a href="${pageContext.request.contextPath}/books?id=${(book.id)}">${book.name}
                                ${author.books.get(author.books.size()-1).id != book.id ? ';' : ''}
                        </a>
                    </c:forEach>
                </td>
            </tr>
        </c:forEach>
    </table>

</div>
</body>
</html>