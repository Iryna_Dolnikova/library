<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="author.create.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
        <%@include file="/static/table.css"%>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div>
    <form name="myForm" class="form" action="authors_add" method="post">
        <div layout:fragment="content" class="card">
            <h1><fmt:message key="author.create.title" bundle="${msg}"/></h1>

            <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
                <p class="error">${error}</p>
            <% } %>

            <ul>
                <li>
                    <label for="surname"><fmt:message key="user.surname.title" bundle="${msg}"/>:</label>
                    <input type="text" id="surname" name="surname" class="input" placeholder="Potter" value="${surname}" required/>
                    <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
                </li>
                <li>
                    <label for="firstName"><fmt:message key="user.firstname.title" bundle="${msg}"/>:</label>
                    <input type="text" id="firstName" name="firstName" class="input" placeholder="Harry" value="${firstName}" required/>
                    <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
                </li>
                <li>
                    <label for="patronymic"><fmt:message key="user.patronymic.title" bundle="${msg}"/>:</label>
                    <input type="text" id="patronymic" name="patronymic" class="input" placeholder="Jamesovich" value="${patronymic}"/>
                    <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
                </li>
            </ul>
            <button type="submit" class="edit"><fmt:message key="button.create.title" bundle="${msg}"/></button>
        </div>

        <div>
            <table id="books">
                <tr>
                    <th></th>
                    <th><fmt:message key="label.book.name" bundle="${msg}"/></th>
                </tr>
                <c:forEach items="${books}" var="book">
                    <tr>
                        <td><input type="checkbox" id="booksIds" name="booksIds" value="${book.id}"
                            ${booksIds.contains(book.id) ? 'checked' : ''}></td>
                        <td>
                            <a href="${pageContext.request.contextPath}/books?id=${(book.id)}">
                                    ${book.name}
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </form>

</div>
</body>
</html>