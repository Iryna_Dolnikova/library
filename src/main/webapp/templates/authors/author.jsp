<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" th:href="@{/profile.css}">
    <title><fmt:message key="author.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
        <%@include file="/static/table.css"%>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="author.title" bundle="${msg}"/></h1>

    <% if (session != null && session.getAttribute("loginedUser") != null
            && !((LoginDto)session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
        <form class="col-6" action="authors_edit" method="get">
            <input type="hidden" name="id" value="${author.id}">
            <button type="submit" class="edit"><fmt:message key="button.edit.title" bundle="${msg}"/></button>
        </form>
        <form class="col-6" action="authors_delete" method="get">
            <input type="hidden" name="id" value="${author.id}">
            <button type="submit" class="delete"><fmt:message key="button.delete.title" bundle="${msg}"/></button>
        </form>
    <% } %>

    <div>
        <ul>
            <li>
                <label for="surname"><fmt:message key="user.surname.title" bundle="${msg}"/>:</label>
                <span id="surname">${author.surname}</span>
            </li>
            <li>
                <label for="firstName"><fmt:message key="user.firstname.title" bundle="${msg}"/>:</label>
                <span id="firstName">${author.firstName}</span>
            </li>
            <li>
                <label for="patronymic"><fmt:message key="user.patronymic.title" bundle="${msg}"/>:</label>
                <span id="patronymic">${author.patronymic}</span>
            </li>
        </ul>

    </div>

</div>

<div>
    <h1 for="books"><fmt:message key="books.title" bundle="${msg}"/></h1>

    <table id="books">
        <tr>
            <th><fmt:message key="label.book.name" bundle="${msg}"/></th>
        </tr>
        <c:forEach items="${author.books}" var="book">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/books?id=${(book.id)}">
                            ${book.name}
                    </a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>