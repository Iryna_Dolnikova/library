<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="publishing.house.create.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css"%>
        <%@include file="/static/card.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="publishing.house.create.title" bundle="${msg}"/></h1>
    <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="publishingHouses_add" method="post">
        <ul>
            <li>
                <label for="name"><fmt:message key="name.title" bundle="${msg}"/>:</label>
                <input type="text" id="name" name="name" class="input" placeholder="А-ба-ба-га-ла-ма-га" value="${name}"
                       required/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="cities"><fmt:message key="city.title" bundle="${msg}"/>:</label>
                <select id="cities" name="cityId">
                    <c:forEach items="${cities}" var="city">
                        <option id="cityId" name="cityId" value="${city.id}"
                            ${city.id == cityId ? 'selected="selected"' : ''}>
                                ${city.name}</option>
                    </c:forEach>
                </select>
                <p class="hint"><fmt:message key="hint.select" bundle="${msg}"/></p>
            </li>
        </ul>

        <button type="submit" class="edit"><fmt:message key="button.create.title" bundle="${msg}"/></button>
    </form>

</div>
</body>
</html>