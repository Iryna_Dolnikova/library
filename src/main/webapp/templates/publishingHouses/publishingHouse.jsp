<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="publishing.house.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css" %>
    </style>

</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="publishing.house.title" bundle="${msg}"/></h1>

    <% if (session != null && session.getAttribute("loginedUser") != null
            && !((LoginDto)session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
        <form class="col-6" action="publishingHouses_edit" method="get">
            <input type="hidden" name="id" value="${publishingHouse.id}">
            <button type="submit" class="edit"><fmt:message key="button.edit.title" bundle="${msg}"/></button>
        </form>
        <form class="col-6" action="publishingHouses_delete" method="get">
            <input type="hidden" name="id" value="${publishingHouse.id}">
            <button type="submit" class="delete"><fmt:message key="button.delete.title" bundle="${msg}"/></button>
        </form>
    <% } %>

    <div class="form">
        <ul>
            <li>
                <label for="name"><fmt:message key="name.title" bundle="${msg}"/>:</label>
                <span id="name">${publishingHouse.name}</span>
            </li>
            <li>
                <label for="city"><fmt:message key="city.title" bundle="${msg}"/>:</label>
                <span><a id="city" href="${pageContext.request.contextPath}/cities?id=${(publishingHouse.city.id)}">
                    ${publishingHouse.city.name}
                </a></span>
            </li>
        </ul>
    </div>

</div>
</body>
</html>