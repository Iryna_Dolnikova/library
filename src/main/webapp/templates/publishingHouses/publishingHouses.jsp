<%@ page import="com.epam.web.dto.LoginDto" %>
<%@ page import="com.epam.model.enums.RolesEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="publishing.houses.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/table.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content">
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <div class="title">
        <h1><fmt:message key="publishing.houses.title" bundle="${msg}"/></h1>

        <% if (session != null && session.getAttribute("loginedUser") != null
                && !((LoginDto)session.getAttribute("loginedUser")).getRole().equals(RolesEnum.READER)) { %>
        <a href="${pageContext.request.contextPath}/publishingHouses_add"><fmt:message key="publishing.house.create.title" bundle="${msg}"/></a>
        <% } %>
    </div>

    <table>
        <tr>
            <th><fmt:message key="name.title" bundle="${msg}"/></th>
            <th><fmt:message key="city.title" bundle="${msg}"/></th>
        </tr>
        <c:forEach items="${publishingHouses}" var="publishingHouse">
            <tr>
                <td><a href="${pageContext.request.contextPath}/publishingHouses?id=${(publishingHouse.id)}">
                        ${publishingHouse.name}</a></td>
                <td><a href="${pageContext.request.contextPath}/cities?id=${(publishingHouse.city.id)}">
                        ${publishingHouse.city.name}</a></td>
            </tr>
        </c:forEach>
    </table>

</div>
</body>
</html>