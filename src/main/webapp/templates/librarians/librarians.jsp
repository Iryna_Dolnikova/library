<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <title><fmt:message key="librarians.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/table.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content">
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <div class="title">
        <h1><fmt:message key="librarians.title" bundle="${msg}"/></h1>
        <a href="${pageContext.request.contextPath}/librarians_add"><fmt:message key="librarian.create.title" bundle="${msg}"/></a>
    </div>

    <table>
        <tr>
            <th><fmt:message key="user.full.name.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.surname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.firstname.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.patronymic.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.email.title" bundle="${msg}"/></th>
            <th><fmt:message key="user.phone.title" bundle="${msg}"/></th>
        </tr>
        <c:forEach items="${librarians}" var="librarian">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/librarians?id=${(librarian.id)}">
                            ${librarian.fullName}
                    </a>
                </td>
                <td>${librarian.surname}</td>
                <td>${librarian.firstName}</td>
                <td>${librarian.patronymic}</td>
                <td>${librarian.email}</td>
                <td>${librarian.phone}</td>
            </tr>
        </c:forEach>
    </table>

</div>
</body>
</html>