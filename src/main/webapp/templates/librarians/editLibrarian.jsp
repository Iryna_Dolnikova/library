<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
    <title><fmt:message key="librarian.edit.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
        <%@include file="/static/table.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="librarian.edit.title" bundle="${msg}"/></h1>
   <% if (request.getAttribute("error") != null || session.getAttribute("error") != null) { %>
        <p class="error">${error}</p>
    <% } %>

    <form name="myForm" class="form" action="users_update?id=${librarian.id}" method="post">
        <ul>
            <li>
                <label for="surname"><fmt:message key="user.surname.title" bundle="${msg}"/>:</label>
                <input name="surname" id="surname" type="text" class="form-control" value="${librarian.surname}" required/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="firstName"><fmt:message key="user.firstname.title" bundle="${msg}"/>:</label>
                <input type="text" id="firstName" name="firstName" class="input" value="${librarian.firstName}" required/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="patronymic"><fmt:message key="user.patronymic.title" bundle="${msg}"/>:</label>
                <input type="text" id="patronymic" name="patronymic" class="input" value="${librarian.patronymic}"/>
                <p class="hint"><fmt:message key="hint.name" bundle="${msg}"/></p>
            </li>
            <li>
                <label for="email"><fmt:message key="user.email.title" bundle="${msg}"/>:</label>
                <input type="email" id="email" name="email" class="input" value="${librarian.email}" required/>
            </li>
            <li>
                <label for="phone"><fmt:message key="user.phone.title" bundle="${msg}"/>:</label>
                <input type="tel" id="phone" name="phone" class="input"
                       pattern="^(?:\+?38[-.\s]?)?0\d{2}[-.\s]?\d{3}[-.\s]?(\d{2}[-.\s]?){2}$"
                       placeholder="050-123-45-67" value="${librarian.phone}" required/>
            </li>
        </ul>

        <input type="hidden" id="role" name="role" class="input" value="${librarian.role}"/>
        <button type="submit" class="edit"><fmt:message key="button.save.title" bundle="${msg}"/></button>
    </form>

</div>
</body>
</html>