<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.messages" var="msg"/>

<!DOCTYPE HTML>
<html lang="${language}">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title><fmt:message key="librarian.title" bundle="${msg}"/></title>

    <style>
        <%@include file="/static/main.css" %>
        <%@include file="/static/card.css"%>
    </style>
</head>

<body>
<jsp:include page="/templates/fragments/header.jsp"/>

<div layout:fragment="content" class="card">
    <h1><fmt:message key="librarian.title" bundle="${msg}"/></h1>
    <form class="col-6" action="librarians_edit" method="get">
        <input type="hidden" name="id" value="${librarian.id}">
        <button type="submit" class="edit"><fmt:message key="button.edit.title" bundle="${msg}"/></button>
    </form>
    <form class="col-6" action="librarians_delete" method="get">
        <input type="hidden" name="id" value="${librarian.id}">
        <button type="submit" class="delete"><fmt:message key="button.delete.title" bundle="${msg}"/></button>
    </form>

    <div>
        <ul>
            <li>
                <label for="surname"><fmt:message key="user.surname.title" bundle="${msg}"/>:</label>
                <span id="surname">${librarian.surname}</span>
            </li>
            <li>
                <label for="firstName"><fmt:message key="user.firstname.title" bundle="${msg}"/>:</label>
                <span id="firstName">${librarian.firstName}</span>
            </li>
            <li>
                <label for="patronymic"><fmt:message key="user.patronymic.title" bundle="${msg}"/>:</label>
                <span id="patronymic">${librarian.patronymic}</span>
            </li>
            <li>
                <label for="email"><fmt:message key="user.email.title" bundle="${msg}"/>:</label>
                <span id="email">${librarian.email}</span>
            </li>
            <li>
                <label for="phone"><fmt:message key="user.phone.title" bundle="${msg}"/>:</label>
                <span id="phone">${librarian.phone}</span>
            </li>
        </ul>

    </div>

</div>
</body>
</html>