use library;

CREATE TABLE cities(
	id int unsigned primary key auto_increment,
    name varchar(255) not null unique
);

CREATE TABLE publishing_houses(
	id int unsigned primary key auto_increment,
    name varchar(255) not null, 
    city_id int unsigned not null,
    
    foreign key (city_id) references cities (id),
    UNIQUE(name, city_id)
);

CREATE TABLE authors(
	id int unsigned primary key auto_increment,
    first_name varchar(255) not null,
	surname varchar(255) not null,
	patronymic varchar(255) null,
    UNIQUE(first_name, surname, patronymic)
);

CREATE TABLE books(
	id int unsigned primary key auto_increment,
    name varchar(255) not null,
    publishing_house_id int unsigned not null,
    issue_year int not null,
    count int unsigned not null,
    
    foreign key (publishing_house_id) references publishing_houses (id),
    UNIQUE(name, publishing_house_id, issue_year)
);

CREATE TABLE authors_books(
    author_id int unsigned not null,
 	book_id int unsigned not null,
    
    foreign key (author_id) references authors(id),
    foreign key (book_id) references books(id),
    UNIQUE(author_id, book_id)
);

CREATE TABLE users(
	id int unsigned primary key auto_increment,
	surname varchar(255) not null,
    first_name varchar(255) not null,
    patronymic varchar(255) not null,
	email varchar(255) not null unique,
	phone varchar(20) not null unique,
	password varchar(255) not null,
    role varchar(20) not null
);

CREATE TABLE readers_books(
	id int unsigned primary key auto_increment,
	reader_id int unsigned not null,
    librarian_id int unsigned not null,
	book_id int unsigned not null,
	date_of_issue date not null,
	expected_return_date date not null,
	actual_return_date date null,

	foreign key (reader_id) references users(id),
	foreign key (librarian_id) references users(id),
	foreign key (book_id) references books(id)
);

ALTER TABLE users add column is_blocked boolean not null;
ALTER TABLE users add column is_active boolean not null;
ALTER TABLE readers_books modify librarian_id int unsigned null;


