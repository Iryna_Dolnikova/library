use library;

INSERT INTO cities (name) VALUES ("Київ"), ("Дніпро"), ("Харків"), ("Львів"), ("Вінниця"), ("Івано-Франківськ");
INSERT INTO publishing_houses(name, city_id) VALUES ("А-ба-ба-га-ла-ма-га", 1), ("Ранок", 1),
	("Видавництво Старого Лева", 4), ("Клуб сімейного дозвілля", 3), ("Фабул", 2);
    
INSERT INTO authors(surname, first_name, patronymic) VALUES ("Шевченко", "Тарас", "Григорович"), 
	("Квітка-Основ'яненко", "Григорій", "Федорович"), ("Гончар", "Олесь", "Терентійович"), ("Стус", "Василь", "Семенович");
 
INSERT INTO books (name, publishing_house_id, issue_year, count) VALUES 
	("Левада", 1, 2020, 5), ("Берег любові", 2, 2001, 3), ("Людина і зброя", 3, 1993, 4),
    ("Маруся", 4, 1987, 2), ("Конотопська відьма", 2, 2005, 4),
    ("Кобзар", 5, 2007, 10), ("Наймичка", 5, 2007, 9), ("Палімпсести", 3, 2011, 17);
   
INSERT INTO authors_books(book_id, author_id) VALUES (1, 1), (2, 3),
	(3, 3), (4, 2), (5, 2), (6, 1), (7, 4);
  
INSERT INTO users (surname, first_name, patronymic, email, phone, password, role, is_blocked, is_active) VALUES
	("Карпенко", "Тарас", "Сергійович", "karpenko.taras@gmail.com", "050-328-19-42", SHA("Password@123"), "READER", false, true),
	("Лєвда", "Валерія", "Володимирівна", "levda.valeriya@gmail.com", "050-264-21-08", SHA("Password@123"), "READER", false, true),
	("Денисенко", "Ірина", "Миколаївна", "iryna_1962@ukr.net", "066-297-15-33", SHA("Password@123"), "LIBRARIAN", false, true),
	("Панченко", "Тарас", "Григорович", "panchenko.taras@gmail.com", "067-254-31-19", SHA("Password@123"), "READER", false, true),
	("Крамаренко", "Катерина", "Олександрівна", "kramar.katya@ukr.net", "067-315-27-28", SHA("Password@123"), "READER", false, true),
	("Тютюнник", "Тетяна", "Володимирівна", "tetyana_1968.@gmail.com", "063-412-15-02", SHA("Password@123"), "LIBRARIAN", false, true),
	("Кузменко", "Сергій", "Іванович", "kuzmenko.serg.@gmail.com", "050-415-20-02", SHA("Password@123"), "LIBRARIAN", false, true),
	("Маслій", "Андрій", "Ігоревич", "masliy_2002.@gmail.com", "067-318-15-16", SHA("Password@123"), "LIBRARIAN", false, true);
 
INSERT INTO library.users (surname, first_name, patronymic, email, phone, password, role, is_blocked, is_active) VALUES
	("Дольнікова", "Ірина", "Володимирівна", "dolnikova@gmail.com", "050-294-51-42", SHA("Password@123"), "ADMIN", false, true);

 INSERT INTO readers_books (reader_id, librarian_id, book_id, date_of_issue, expected_return_date, actual_return_date) VALUES
	(1, 2, 3, "2020-04-19", "2020-05-30", "2020-06-13"), (1, 6, 3, "2020-05-05", "2020-05-25", NULL), 
    (1, 8, 6, "2022-04-13", "2020-04-19", "2020-04-19"), (2, 2, 6, "2020-06-23", "2020-08-23", "2020-08-05"), 
    (4, 7, 6, "2022-06-08", "2022-08-14", NULL), (4, 5, 7, "2020-11-11", "2022-01-17", "2021-01-14"), 
    (4, 3, 8, "2021-10-02", "2021-12-12", "2021-12-19"), (4, 4, 8, "2022-03-08", "2020-05-27", "2020-05-29");

